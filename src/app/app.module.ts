import {NgModule, ApplicationRef} from '@angular/core';
import {BrowserModule} from '@angular/platform-browser';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {Http} from '@angular/http';

import {FormsModule} from '@angular/forms';
import {StoreModule} from '@ngrx/store';
import {AppReducerConstants} from './app-store/app-reducers.constant';
import {EffectsModule} from '@ngrx/effects';
import {environment} from '../environments/environment';
import {StoreDevtoolsModule} from '@ngrx/store-devtools';
import {ToastContainerModule, ToastNoAnimationModule, ToastrModule, ToastrService} from 'ngx-toastr';

/*
 * Platform and Environment providers/directives/pipes
 */
import {routing} from './app.routing'
// App is our top level component
import {AppComponent} from './app.component';
import {APP_RESOLVER_PROVIDERS} from './app.resolver';
import {AppState, InternalStateType} from './app.service';

// Core providers
import {CoreModule} from './core/core.module';
import {SmartadminLayoutModule} from './shared/layout/layout.module';


import {ModalModule} from 'ngx-bootstrap/modal';
import {AppReadyEvent} from './app-ready.component';

import {DropzoneConfigInterface, DropzoneModule} from 'ngx-dropzone-wrapper';

import {Constants} from './constants/app.constant';

import {HelperService} from './services/helper.service';
import {MyNotifyService} from './services/my-notify.service';
import {AuthenticationService} from './services/authentication.service';
import {HTTP_INTERCEPTORS, HttpClient, HttpClientModule} from '@angular/common/http';
import {AuthInterceptor} from './services/auth.interceptor';
import {RespInterceptor} from 'app/services/resp.interceptor';
import {DownloadService} from './services/download.service';
import {AuthGuard} from './core/guards/auth.guard';

import {PayerService} from './pages/payer/payer.service';
import {CollinsonUserService} from './pages/collinson-user/collinson-user.service';
import {CurrencyService} from './pages/currency/currency.service';
import {PricePolicyService} from './pages/price-policy/price-policy.service';
import {PayeeBankAccountService} from './pages/payee-bank-account/payee-bank-account.service';
import {DepositService} from './pages/deposit/deposit.service';
import {CountryService} from './pages/country/country.service';
import {RateService} from './pages/rate/rate.service';
import {CurrencyCountryConfigService} from './pages/currency-country-config/currency-country-config.service';
import {TransactionService} from './pages/transaction/transaction.service';
import {PayeeService} from './pages/payee/payee.service';
import {SysBankAccountService} from './pages/sys-bank-account/sys-bank-account.service';
import {PriceCategoryService} from './pages/price-category/price-category.service';

import {TranslateLoader, TranslateModule} from '@ngx-translate/core';
import {TranslateHttpLoader} from '@ngx-translate/http-loader';

export function createTranslateLoader(http: HttpClient) {
    return new TranslateHttpLoader(http, './assets/i18n/', '.json');
}

// Application wide providers
const APP_PROVIDERS = [
    ...APP_RESOLVER_PROVIDERS,
    AppState
];

type StoreType = {
    state: InternalStateType,
    restoreInputValues: () => void,
    disposeOldHosts: () => void
};

const DROPZONE_CONFIG: DropzoneConfigInterface = {
    // Change this to your upload POST address:
    url: Constants.API_ENDPOINT + 'api/v1/attachment/upload',
    maxFilesize: 50,
    // acceptedFiles: 'image/*, application/pdf, application/vnd.openxmlformats-officedocument.spreadsheetml.sheet,application/vnd.ms-excel',
    acceptedFiles: '',
    addRemoveLinks: true,
    clickable: true
};

/**
 * `AppModule` is the main entry point into Angular2's bootstraping process
 */
@NgModule({
    bootstrap: [AppComponent],
    declarations: [
        AppComponent,

    ],
    imports: [
        BrowserModule,
        BrowserAnimationsModule,
        FormsModule,

        ModalModule.forRoot(),
        DropzoneModule.forRoot(DROPZONE_CONFIG),

        ToastrModule.forRoot({
            timeOut: 5000,
            positionClass: 'toast-top-right',
            preventDuplicates: true,
        }),
        // ToastContainerModule.forRoot(),
        ToastNoAnimationModule,
        CoreModule,
        SmartadminLayoutModule,

        routing,

        StoreModule.forRoot(AppReducerConstants), // eargly loading not lazy loading.
        EffectsModule.forRoot([]),
        !environment.production ? StoreDevtoolsModule.instrument() : [],
         TranslateModule.forRoot({
                    loader: {
                        provide: TranslateLoader,
                        useFactory: (createTranslateLoader),
                        deps: [HttpClient]
                    }
                }),
    ],
    exports: [],
    providers: [ // expose our Services and Providers into Angular's dependency injection
        // ENV_PROVIDERS,
AuthGuard,
        APP_PROVIDERS,
AuthenticationService,
{provide: HTTP_INTERCEPTORS, useClass: AuthInterceptor, multi: true},
        {provide: HTTP_INTERCEPTORS, useClass: RespInterceptor, multi: true},
        AppReadyEvent,
        HelperService,
DownloadService,
MyNotifyService,
    PayerService,
    CollinsonUserService,
    CurrencyService,
    PricePolicyService,
    PayeeBankAccountService,
    DepositService,
    CountryService,
    RateService,
    CurrencyCountryConfigService,
    TransactionService,
    PayeeService,
    SysBankAccountService,
    PriceCategoryService,
    ]
})
export class AppModule {
    constructor(public appRef: ApplicationRef, public appState: AppState) {
    }


}

