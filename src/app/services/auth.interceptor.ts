import {HttpEvent, HttpHandler, HttpInterceptor, HttpRequest, HttpResponse} from '@angular/common/http';
import {Observable} from 'rxjs/Observable';
import {Injectable, Injector} from '@angular/core';

import {AuthenticationService} from './authentication.service';

@Injectable()
export class AuthInterceptor implements HttpInterceptor {
    private authService: AuthenticationService;

    constructor(private injector: Injector) {
    }


    intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        this.authService = this.injector.get(AuthenticationService); // get it here within intercept
        // const copiedReq = req.clone({headers: req.headers.set('', '')});
        if (this.authService.getToken() && req.headers.get('Authorization') == null) {
            // headers.append('Authorization', 'Bearer ' + this.authenticationService.getToken());
            // copiedReq copiedReq = req.clone({params: req.params.set('Authorization', 'Bearer ' + this.authService.getToken())});
            const copiedReq = req.clone({
                setHeaders: {
                    Authorization: `Bearer ${this.authService.getToken()}`
                }
            });
            return next.handle(copiedReq);
        } else {
            return next.handle(req);
        }
        // return null;
    }
}
