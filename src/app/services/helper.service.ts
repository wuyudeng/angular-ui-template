import {Injectable} from '@angular/core';
import {Router} from '@angular/router';

@Injectable()
export class HelperService {

    constructor() {
    }

    parseToFix(input: number, num: number) {
        if (input) {
            return parseFloat(parseFloat((Math.round(input * 10000) / 10000).toString()).toFixed(num));
        }
    }

    generateQueryString(o: any) {
        let str = '';
        if (o) {
            Object.keys(o).forEach(key => {
                console.log(key);
                if (o[key] !== null && o[key] !== '' && o[key] !== undefined) {
                    str = str + '&f_' + key + '=' + o[key]
                }
            });
        }

        return str;
    }

    copyProperty(objFrom: any, objTo: any) {
        if (objFrom && objTo) {
            // Object.keys()
        }
    }


    getSearchConditionByRouter(router: Router) {

        const urlTree = router.parseUrl(router.url);

        const searchConditionForQuery = this.generateQueryString(urlTree.queryParams);

        console.log(searchConditionForQuery);

        return searchConditionForQuery;
    }

    generateQueryParams(queryParams: { [p: string]: string }, o: any) {
        if (o) {
            Object.keys(o).forEach(key => {
                console.log(key);
                if (o[key] !== null && o[key] !== '' && o[key] !== undefined) {
                    // str = str +  '&f_' + key + '=' +  o[key]
                    queryParams[key] = o[key];
                }
            });
        }
        return queryParams;
    }

    public populateObject<T>(params: URLSearchParams, prefix: string, val: T) {
        debugger;
        const objectKeys = Object.keys(val) as Array<keyof T>;

        if (prefix) {
            prefix = prefix + '.';
        }

        for (let objKey of objectKeys) {

            let value = val[objKey];
            let key = prefix + objKey;

            // this.PopulateSearchParams(params, key, value);
        }
    }
}
