import {Injectable} from '@angular/core';
import 'rxjs/add/operator/map'
import {Constants} from '../constants/app.constant';
import {User} from '../models/user.model';
import {LoginResp} from '../models/login.resp.model';
import {HttpClient, HttpHeaders, HttpParams} from '@angular/common/http';
import {Observable} from 'rxjs/Observable';

@Injectable()
export class AuthenticationService {
    public user: User;
    public token: string;
    private userUrl = Constants.API_ENDPOINT + 'v1/user';

    constructor(private http: HttpClient) {
        // set token if saved in local storage
        const currentUser = JSON.parse(localStorage.getItem(Constants.CurrentUser));
        this.token = currentUser && currentUser.token;
    }

    login(username: string, password: string) {
        const body = new HttpParams()
            .set('username', username)
            .set('password', password);

        const postData = 'username=' + encodeURIComponent(username) + '&password=' + encodeURIComponent(password);
        console.log(postData);
        return this.http.post(Constants.API_ENDPOINT + 'login', body.toString(),
            {
                headers: new HttpHeaders()
                    .set('Content-Type', 'application/x-www-form-urlencoded')
            })
            .map((response) => {
                console.log('=======token=====');
                // login successful if there's a jwt token in the response
                const loginResp: LoginResp | any = response;
                // console.log(token);
                if (loginResp.access_token) {
                    // set token property
                    this.token = loginResp.access_token;
                    loginResp.loginSecond = new Date().getTime() / 1000;

                    // if (loginResp.type !== Constants.USER.backend) {
                    //     return false;
                    // }
                    // store username and jwt token in local storage to keep user logged in between page refreshes
                    localStorage.setItem(Constants.CurrentUser, JSON.stringify(loginResp));
                    // return true to indicate successful login
                    return true;
                } else {
                    return false;
                }
            }).catch(res => Observable.throw(res));
    }

    getToken() {
        const t: LoginResp = JSON.parse(localStorage.getItem(Constants.CurrentUser));
        if (t) {
            return t.access_token;
        }
    }


    logout(): void {
        // clear token remove user from local storage to log user out
        this.token = null;
        localStorage.removeItem(Constants.CurrentUser);
        localStorage.removeItem(Constants.CurrentUserInfo);
        this.http.post(Constants.API_ENDPOINT + 'logout', null);

    }


    storeUserInfo(user: User) {
        localStorage.setItem(Constants.CurrentUserInfo, JSON.stringify(user));
    }

    getUserInfo() {
        let user: User = new User();
        user = JSON.parse(localStorage.getItem(Constants.CurrentUserInfo));
        return user;
    }

}
