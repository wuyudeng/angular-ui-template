import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders, HttpParams} from '@angular/common/http';
import {Observable} from 'rxjs/Observable';
import {saveAs} from 'file-saver';
import {Constants} from '../constants/app.constant';

@Injectable()
export class DownloadService {
    constructor(private http: HttpClient) {

    }

    downloadFile(url: string, fileName: string) {
        console.log(url);
        this.getFileBlob(url).subscribe(blob => {
            saveAs(blob, fileName);
        });
    }

    getFileBlob(url: string): Observable<Blob> {
        const options: {
            headers?: HttpHeaders,
            observe?: 'body',
            params?: HttpParams,
            reportProgress?: boolean,
            responseType: 'blob',
            withCredentials?: boolean
        } = {
            responseType: 'blob'
        };
        return this.http.get(url, options);
    }
}
