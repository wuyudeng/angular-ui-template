import {Injectable} from '@angular/core';
import 'rxjs/add/operator/map';
import {NotificationService} from '../shared/utils/notification.service';
import {ToastrService} from 'ngx-toastr';

@Injectable()
export class MyNotifyService {
    constructor(private notificationService: NotificationService,
                public toastrService: ToastrService) {
    }

    notifySuccess(content: string) {
        // this.notiicationService.smallBox({
        //     title: 'Congratulations',
        //     content: content,
        //     color: '#356635',
        //     timeout: 1000,
        //     icon: 'fa fa-check'
        // });
        this.toastrService.success(content);
    }

    notifyFail(content: string) {
        // this.notificationService.smallBox({
        //     title: 'Sorry',
        //     content: content,
        //     color: '#953b39',
        //     timeout: 2000,
        //     icon: 'fa fa-remove'
        // });
        this.toastrService.error(content);
    }
}
