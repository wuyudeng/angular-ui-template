import {
    HttpErrorResponse, HttpEvent, HttpHandler, HttpInterceptor, HttpRequest,
    HttpResponse
} from '@angular/common/http';
import {Observable} from 'rxjs/Observable';
import {Injectable, Injector} from '@angular/core';

import {AuthenticationService} from './authentication.service';
import {MyNotifyService} from './my-notify.service';

@Injectable()
export class RespInterceptor implements HttpInterceptor {
    private authService: AuthenticationService;

    constructor(private injector: Injector,
                public myNotifyService: MyNotifyService) {
    }


    intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        this.authService = this.injector.get(AuthenticationService); // get it here within intercept
        const started = Date.now();
        // return next
        //     .handle(req)
        //     .do(event => {
        //         console.log(event)
        //         if (event instanceof HttpResponse) {
        //             const elapsed = Date.now() - started;
        //
        //             if (event.status === 404 || event.status >= 500) {
        //                 this.myNotifyService.notifyFail('Our server is experiencing some error, please try again later.');
        //             } else if (event.status === 401) {
        //                 this.myNotifyService.notifyFail('Login Id or password is wrong, please try again later.');
        //             } else if (event.status === 403) {
        //                 this.myNotifyService.notifyFail('Your login is expired, please logout and re-login.');
        //             }
        //         }
        //     });

        return next.handle(req).do((event: HttpEvent<any>) => {
            if (event instanceof HttpResponse) {
                // do stuff with response if you want
            }
        }, (err: any) => {
            if (err instanceof HttpErrorResponse) {
                console.log(err);
                // if (err.status === 404 || err.status >= 500) {
                if (err.status >= 500) {
                    this.myNotifyService.notifyFail('Our server is experiencing some error, please try again later.');
                } else if (err.status === 400) {
                    if (err.error && err.error.error) {
                        this.myNotifyService.notifyFail(err.error.error);
                    } else {
                        this.myNotifyService.notifyFail('Error happens, please try again later.');
                    }
                } else if (err.status === 401) {
                    this.myNotifyService.notifyFail('Login Id or password is wrong, please try again later.');
                } else if (err.status === 403) {
                    this.myNotifyService.notifyFail('Your login is expired, please logout and re-login.');
                }
                // else {
                //     this.myNotifyService.notifyFail('Our server is experiencing some error, please try again later.');
                // }
            }
        });
    }
}
