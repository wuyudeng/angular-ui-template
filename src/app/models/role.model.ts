import {RolePermission} from './role-permission.model';

export class Role {
    id: number;
    version: string;
    name: string;
    rolePermissions: any;
}