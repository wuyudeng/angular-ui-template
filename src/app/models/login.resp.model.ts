export class LoginResp {
    access_token: string;
    expires_in: number;
    type: string;
    loginSecond: number;
}