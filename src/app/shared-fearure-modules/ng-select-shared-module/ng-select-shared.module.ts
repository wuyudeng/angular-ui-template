import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {CommonNgSelectComponent} from './common-ng-select/common-ng-select.component';
import {NgSelectModule} from '@ng-select/ng-select';

// shared module
// do not provide services in Shared Modules! Especially not if you plan to use them in Lazy Loaded Modules!
@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        ReactiveFormsModule,
        NgSelectModule
    ],
    declarations: [CommonNgSelectComponent],
    exports: [CommonNgSelectComponent]
})
export class NgSelectSharedModule {
}
