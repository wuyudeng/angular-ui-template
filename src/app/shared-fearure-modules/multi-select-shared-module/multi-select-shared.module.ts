import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {SelectModule} from 'ng2-select';
import {CommonMultiSelectComponent} from './common-multi-select/common-multi-select.component';

// shared module
// do not provide services in Shared Modules! Especially not if you plan to use them in Lazy Loaded Modules!
@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        ReactiveFormsModule,
        SelectModule
    ],
    declarations: [CommonMultiSelectComponent],
    exports: [CommonMultiSelectComponent]
})
export class MultiSelectSharedModule {
}
