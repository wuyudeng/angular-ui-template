import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {Subject} from 'rxjs/Subject';

@Component({
    selector: 'common-multi-select',
    templateUrl: './common-multi-select.component.html'
})
export class CommonMultiSelectComponent implements OnInit {
    @Input('getFromStore')
    public getFromStore: boolean;

    @Input('service')
    public service: any;

    @Input('multiple')
    public multiple: boolean;

    @Input('datesForLoad')
    public datasForLoad: any;

    @Input('refreshSubject')
    public refreshSubject: any;

    @Input('datasForLoadSubject')
    public datasForLoadSubject = new Subject();

    @Input('dataIdForLoad')
    public dataIdForLoad: number;

    @Input('dataIdForLoadSubject')
    public dataIdForLoadSubject: Subject<number>;

    @Input('datasForSelect')
    public datasForSelect: any;

    @Input('datasForSelectSubject')
    public datasForSelectSubject = new Subject();

    // The parent can bind to this event
    @Output() datasSelected = new EventEmitter();

    public selectedDatas: any;

    constructor() {

    }

    ngOnInit(): void {
        console.log('this.dataForLoad');
        console.log(this.datasForLoad);
        if (this.datasForSelect && this.datasForSelect.length > 0) {

        }
        this.getOptionList();
        this.subscribeParentEvent();
    }

    subscribeParentEvent() {
        if (this.refreshSubject) {
            this.refreshSubject.subscribe(event => {
                this.getOptionList();
            });
        }

        if (this.dataIdForLoadSubject) {
            this.dataIdForLoadSubject.subscribe(event => {
                console.log(event);
                this.selectedDatas = [this.datasForSelect.find(p =>
                    p.id === event
                )];
            });
        }

        if (this.datasForSelectSubject) {
            this.datasForSelectSubject.subscribe(event => {
                console.log(event);
                this.datasForSelect = event;
                if (this.datasForSelect && this.datasForSelect.length > 0) {
                    this.datasForSelect.map(p => {
                        p.text = p.name;
                        p.id = p.id;
                    });
                }
            });
        }

        if (this.datasForLoadSubject) {
            this.datasForLoadSubject.subscribe(event => {
                console.log(event);
                this.datasForLoad = event;
                // 如果传进来的是数组，加上text 和 name 属性
                if (this.datasForLoad && this.datasForLoad.length > 0) {
                    this.datasForLoad.map(p => {
                        p.text = p.name;
                        p.id = p.id;
                    });
                    this.selectedDatas = this.datasForLoad;
                }
                // 如果传进来的是单个object，加上text 和 name 属性
                else if (this.datasForLoad) {
                    this.selectedDatas = [this.datasForLoad];
                    this.datasForLoad.id = this.datasForLoad.id;
                    this.datasForLoad.text = this.datasForLoad.name;
                }
            });
        }
    }

    // 获取下拉菜单的列表
    getOptionList() {
        if (this.service) {
            this.service.getAll().subscribe(resp => {
                console.log(this.service.toString() + 'get multi select list', resp);
                this.datasForSelect = resp.content;
                this.processDataForSelect();
            });
        }
    }

    processDataForSelect() {
        if (this.datasForSelect && this.datasForSelect.length > 0) {
            this.datasForSelect.map(p => {
                p.text = p.name;
                p.id = p.id;
            });
        }
    }

    // getDataList() {
    //     this.brandTagService.getAll().subscribe(resp => {
    //         this.datasForSelect = resp.results;
    //         console.log(resp);
    //         if (this.datasForSelect && this.datasForSelect.length > 0) {
    //             this.datasForSelect.map(p => {
    //                 p.text = p.name;
    //                 p.id = p.id;
    //             });
    //
    //             // if have dataForLoad emit it
    //             // else emit the first one in datasForSelect
    //             if (this.datasForLoad && this.datasForSelect) {
    //                 // this.datasSelected.emit(this.dataForLoad)
    //                 this.selectedDatas = this.datasForLoad;
    //                 this.selectedDatas.map(p => {
    //                     p.text = p.name;
    //                     p.id = p.id;
    //                 });
    //                 this.datasSelected.emit(this.selectedDatas);
    //             }
    //         }
    //     })
    // }

    // ng2 select
    public selected(value: any): void {
        // console.log(value);
    }

    public removed(value: any): void {
        // console.log(value);
    }

    public typed(value: any): void {
        // console.log('New search input: ', value);
    }

    // public refreshValue(value: any): void {
    //     let selected = [];
    //     if (this.datasForSelect && value) {
    //         selected = this.datasForSelect.filter(p =>
    //             value.filter(v => v.id === p.id).length > 0
    //         );
    //     }
    //     this.datasSelected.emit(selected);
    // }

    public refreshValue(value: any): void {
        console.log(value);

        let selected = [];
        if (this.datasForSelect && value) {

            if (this.multiple) {
                selected = this.datasForSelect.filter(p =>
                    value.filter(v => v.id === p.id).length > 0
                );
                this.datasSelected.emit(selected);
            } else {
                selected = this.datasForSelect.filter(p =>
                    value.id === p.id
                );
                this.datasSelected.emit(selected[0]);
            }
        }
    }

}
