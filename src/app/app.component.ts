import {Component, ViewContainerRef} from '@angular/core';

import {PayerService} from './pages/payer/payer.service';
import {CollinsonUserService} from './pages/collinson-user/collinson-user.service';
import {CurrencyService} from './pages/currency/currency.service';
import {PricePolicyService} from './pages/price-policy/price-policy.service';
import {PayeeBankAccountService} from './pages/payee-bank-account/payee-bank-account.service';
import {DepositService} from './pages/deposit/deposit.service';
import {CountryService} from './pages/country/country.service';
import {RateService} from './pages/rate/rate.service';
import {CurrencyCountryConfigService} from './pages/currency-country-config/currency-country-config.service';
import {TransactionService} from './pages/transaction/transaction.service';
import {PayeeService} from './pages/payee/payee.service';
import {SysBankAccountService} from './pages/sys-bank-account/sys-bank-account.service';
import {PriceCategoryService} from './pages/price-category/price-category.service';

@Component({
  selector: 'app-root',
  template: '<router-outlet></router-outlet>'
})
export class AppComponent {
  public title = 'app works!';

  public constructor(private viewContainerRef: ViewContainerRef,
                      public payerService: PayerService,
                      public collinsonUserService: CollinsonUserService,
                      public currencyService: CurrencyService,
                      public pricePolicyService: PricePolicyService,
                      public payeeBankAccountService: PayeeBankAccountService,
                      public depositService: DepositService,
                      public countryService: CountryService,
                      public rateService: RateService,
                      public currencyCountryConfigService: CurrencyCountryConfigService,
                      public transactionService: TransactionService,
                      public payeeService: PayeeService,
                      public sysBankAccountService: SysBankAccountService,
                      public priceCategoryService: PriceCategoryService,
                    ) {
      this.payerService.fetchAll();
      this.collinsonUserService.fetchAll();
      this.currencyService.fetchAll();
      this.pricePolicyService.fetchAll();
      this.payeeBankAccountService.fetchAll();
      this.depositService.fetchAll();
      this.countryService.fetchAll();
      this.rateService.fetchAll();
      this.currencyCountryConfigService.fetchAll();
      this.transactionService.fetchAll();
      this.payeeService.fetchAll();
      this.sysBankAccountService.fetchAll();
      this.priceCategoryService.fetchAll();

  }

}
