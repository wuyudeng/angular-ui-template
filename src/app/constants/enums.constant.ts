
export class EnumsConstant {

    // public static CURRENCIES: Currency[] = [
    //     {id: 1, name: 'NZD', currency: 'NZD'},
    //     {id: 2, name: 'AUD', currency: 'AUD'},
    //     {id: 3, name: 'USD', currency: 'USD'},
    //     {id: 4, name: 'SGD', currency: 'SGD'},
    //     {id: 5, name: 'CNY', currency: 'CNY'}
    // ];
    // // app.module.ts init
    // public static COUNTRIES: Country[] = [
    //     {id: 13, code: 'AU', name: 'Australia'},
    //     {id: 44, code: 'CN', name: 'China'},
    //     {id: 158, code: 'NZ', name: 'New Zealand'}
    // ];
    //
    // public static STATUS: string[] = [
    //     'success',
    //     // 'fxback',
    //     'pending',
    //     'created',
    //     'locked',
    //     'fail',
    //     'sync'
    // ];
    //
    // public static TRANSACTIONORDERTYPES: TransactionOrderType[] = [
    //     new TransactionOrderType({name: 'Online', value: 'Online', canRefund: true, display: true}),
    //     new TransactionOrderType({name: 'SpotPay', value: 'SpotPay', canRefund: true, display: true}),
    //     new TransactionOrderType({name: 'Invoice', value: 'Invoice', canRefund: true, display: true}),
    //     new TransactionOrderType({name: 'StaticPay', value: 'StaticPay', canRefund: true, display: true}),
    //     new TransactionOrderType({name: 'Withdrawals', value: 'Withdrawals', canRefund: false, display: true}),
    //     new TransactionOrderType({name: 'Adjust', value: 'Adjust', canRefund: false, display: true}),
    //     new TransactionOrderType({name: 'Refunds', value: 'Refunds', canRefund: false, display: true}),
    //     new TransactionOrderType({name: 'TransactionFee', value: 'TransactionFee', canRefund: false, display: true}),
    //     new TransactionOrderType({name: 'Rebate', value: 'Rebate', canRefund: false, display: true})
    // ];
    //
    // public static GATEWAYS: Gateway[] = [
    //     {id: 0, name: 'payease'},
    //     {id: 1, name: 'alipay'},
    //     {id: 2, name: 'wechat'},
    //     {id: 3, name: 'jdpay'},
    //     {id: 4, name: 'baidupay'}
    // ];
    //
    // public static COMMON_STATUS: CommonStatus[] = [
    //     {id: 0, name: 'disabled'},
    //     {id: 1, name: 'enabled'}
    // ];
    //
    // public static SETTLE_STATUS: CommonStatus[] = [
    //     {id: 0, name: 'Pending'},
    //     {id: 1, name: 'Completed'},
    //     {id: 2, name: 'Rejected'},
    //     {id: 3, name: 'Failed'}
    // ];
    //
    // public static MerchantRole: CommonStatus[] = [
    //     {id: 0, name: 'ADMIN'},
    //     {id: 1, name: 'MANAGER'},
    //     {id: 2, name: 'DEVELOPER'},
    //     {id: 3, name: 'ACCOUNTANT'},
    //     {id: 4, name: 'SALES'}
    // ];
    //
    // // public static PayementMethodArray: PaymentMethodSub[] = [
    // //     EnumsConstant.PAYMENTMETHOD.OnlineBank,
    // //     EnumsConstant.PAYMENTMETHOD.Alipay,
    // //     EnumsConstant.PAYMENTMETHOD.Wechat
    // // ];
    //
    // public static PAYMENTMETHOD: PaymentMethod = {
    //     OnlineBank: {
    //         id: 0,
    //         name: 'OnlineBank'
    //     },
    //     Alipay: {
    //         id: 1,
    //         name: 'Alipay'
    //     },
    //     Wechat: {
    //         id: 2,
    //         name: 'Wechat'
    //     }
    // };
    //
    // public static SETTLE_TYPE: CommonStatus[] = [
    //     {id: 0, name: 'WITHDRAW'},
    //     {id: 1, name: 'AUTO_WITHDRAW_NORMAL'},
    //     {id: 2, name: 'AUTO_WITHDRAW_COMB'},
    //     {id: 3, name: 'AUTO_WITHDRAW_BACKUP'}
    // ];

}