import {environment} from '../../environments/environment';
import {UserType} from '../models/admin/user-type.model';
import {TrxType} from '../models/admin/trx-type.model';
import {Tab} from '../models/latipay/tab.model';
import {MerchantType} from '../models/latipay/merchant-type.model';
import {SettlementType} from '../models/admin/settlement-type.model';
import {OrderType} from '../models/latipay/OrderType';
import {SettlementsTab} from '../models/latipay/settlements-tab.model';
import {Step} from '../models/latipay/onboard/step.model';

export class Constants {
    public static CurrentUser = 'latipay-admin-current-user';
    public static CurrentUserInfo = 'latipay-admin-current-user-detail';
    public static CurrentUserPermissions = 'latipay-admin-current-user-permission';
    public static API_ENDPOINT = environment.apiUrl;
    public static PERMISSION_CHANGE = 'latipay-admin-current-user-permission-changed';
    // public static Status = {
    //     booked: 'BOOKED',
    //     payeesAdded: 'PAYEES_ADDED',
    //     processing: 'PROCESSING',
    //     received: 'RECEIVED',
    //     canceled: 'CANCELED',
    //     settled: 'SETTLED',
    //     statusForSpot: [
    //         'BOOKED',
    //         'PAYEES_ADDED',
    //         'PROCESSING',
    //         'CANCELED'
    //     ]
    // };
    // public static RecevedTrxType = {
    //     parent: 'parent-trx',
    //     undividableLeaf: 'leaf-trx-can-not-be-divide',
    //     dividableLeaf: 'leaf-trx-can-be-divide'
    // };
    // public static USER: UserType = {
    //     broker: 'BROKER',
    //     individual: 'INDIVIDUAL',
    //     company: 'COMPANY',
    //     backend: 'BACKEND',
    //     nonBackend: 'NONBACKEND'
    // };
    //
    // public static ADMINUSERTYPES: string[] =
    //     [
    //         'BROKER',
    //         'INDIVIDUAL',
    //         'COMPANY',
    //         'BACKEND',
    //     ];
    //
    //
    // public static
    // TrxType: TrxType = {
    //     NonBrokerUnsettled: 'NonBrokerUnsettled',
    //     NonBrokerSettled: 'NonBrokerSettled',
    //     BrokerUnsettled: 'BrokerUnsettled',
    //     BrokerSettled: 'BrokerSettled',
    //     DealingBlotter: 'DealingBlotter',
    //     DealingBlotterChecked: 'DealingBlotterChecked',
    //     TrxPl: 'TrxPl'
    // };
    //
    // public static
    // SettlementsTab: SettlementsTab = {
    //     pending: 'Pending',
    //     completed: 'Completed',
    //     rejected: 'Rejected',
    //     failed: 'Failed'
    // };
    //
    // public static TabName: Tab = {
    //     profile: 'Profile',
    //     document: 'Document',
    //     pricing: 'Pricing',
    //     user: 'User',
    //     wallet: 'Wallet',
    //     bankAccount: 'bankAccount',
    //     onboardAccpountDetail: 'onboardAccpountDetail',
    //     onboardDirector: 'onboardDirector',
    //     onboardShareholder: 'onboardShareholder'
    // };
    //
    // public static
    // MerchantType: MerchantType = {
    //     individual: 'INDIVIDUAL',
    //     company: 'COMPANY'
    // };
    //
    // public static
    // OrderType: OrderType = {
    //     orgOrder: 'orgOrder',
    //     allOrder: 'allOrder'
    // };
    //
    // public static StepConstant: Step[] = [
    //     new Step({name: 'NONE', step: 0}),
    //     new Step({name: 'TRIL', step: 10}),
    //     new Step({name: 'TRIL_PASS', step: 20}),
    //     new Step({name: 'FULL', step: 30}),
    //     new Step({name: 'FULL_PASS', step: 40}),
    // ];
    // public static ExcelUploadUrl = environment.apiUrl + 'v1/excel';

}
