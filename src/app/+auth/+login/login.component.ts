import {Component, OnInit} from '@angular/core';
import {Router} from '@angular/router';
import {AbstractControl, FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {NotificationService} from '../../shared/utils/notification.service';
import {AuthenticationService} from '../../services/authentication.service';
import {MyNotifyService} from '../../services/my-notify.service';

@Component({
    selector: 'app-login',
    templateUrl: './login.component.html'
})
export class LoginComponent {
    public router: Router;
    public form: FormGroup;
    public email: AbstractControl;
    public username: AbstractControl;
    public password: AbstractControl;
    public loading: boolean;

    constructor(router: Router, fb: FormBuilder,
                public myNotifyService: MyNotifyService,
                public authService: AuthenticationService) {
        this.router = router;
        this.form = fb.group({
            // 'email': ['', Validators.compose([Validators.required, emailValidator])],
            'username': ['', Validators.compose([Validators.required])],
            'password': ['', Validators.compose([Validators.required, Validators.minLength(6)])]
        });

        // this.email = this.form.controls['email'];
        this.username = this.form.controls['username'];
        this.password = this.form.controls['password'];
        this.setValues();
    }

    setValues() {
        this.form
            .setValue({
                username: '',
                password: '',
            })
    }

    public onSubmit(values: Object): void {
        this.loading = true;
        this.authService.login(this.username.value, this.password.value).subscribe((resp: any) => {
            console.log(resp);
            this.loading = false;
        }, err => {
            this.myNotifyService.notifyFail('Error happens, Please check and try again');
            this.loading = false;
        });
    }
}
