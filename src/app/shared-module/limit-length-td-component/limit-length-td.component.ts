import {AfterContentChecked, Component, EventEmitter, Input, OnInit, Output, ViewChild} from '@angular/core';

declare var $: any;

@Component({
    selector: 'limit-length-td-component',
    templateUrl: 'limit-length-td.component.html'
})
export class LimitLengthTdComponent implements OnInit, AfterContentChecked {
    @Input('maxLength')
    public maxLength = 20;

    @Input('text')
    public text = '';
    public displayText = '';

    constructor() {
    }

    ngOnInit() {

        if (this.text && typeof this.text === 'string') {
            this.displayText = this.text.substring(0, this.maxLength)
        }
    }

    ngAfterContentChecked(): void {

    }
}
