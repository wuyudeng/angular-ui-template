import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {NgxPopperModule} from 'ngx-popper';
import {NgxPaginationModule} from 'ngx-pagination';
import {DropzoneModule} from 'ngx-dropzone-wrapper';
import {SelectModule} from 'ng2-select';
import {NouisliderModule} from 'ng2-nouislider';
import {NguiDatetimePickerModule} from '@ngui/datetime-picker';
import {QuillModule} from 'ngx-quill';
import {ValidationErrorComponent} from './bases/validation-error/validation-error.component';
import {MyDropZoneComponent} from './my-drop-zone-component/my-drop-zone.component';
import {MultiSelectSharedModule} from '../shared-fearure-modules/multi-select-shared-module/multi-select-shared.module';
import {NgSelectSharedModule} from '../shared-fearure-modules/ng-select-shared-module/ng-select-shared.module';
import {LimitLengthTdComponent} from './limit-length-td-component/limit-length-td.component';
import {PayerSubformComponent} from './subform-components/payer/payer-subform.component';
import {CollinsonUserSubformComponent} from './subform-components/collinson-user/collinson-user-subform.component';
import {CurrencySubformComponent} from './subform-components/currency/currency-subform.component';
import {PricePolicySubformComponent} from './subform-components/price-policy/price-policy-subform.component';
import {PayeeBankAccountSubformComponent} from './subform-components/payee-bank-account/payee-bank-account-subform.component';
import {DepositSubformComponent} from './subform-components/deposit/deposit-subform.component';
import {CountrySubformComponent} from './subform-components/country/country-subform.component';
import {RateSubformComponent} from './subform-components/rate/rate-subform.component';
import {CurrencyCountryConfigSubformComponent} from './subform-components/currency-country-config/currency-country-config-subform.component';
import {TransactionSubformComponent} from './subform-components/transaction/transaction-subform.component';
import {PayeeSubformComponent} from './subform-components/payee/payee-subform.component';
import {SysBankAccountSubformComponent} from './subform-components/sys-bank-account/sys-bank-account-subform.component';
import {PriceCategorySubformComponent} from './subform-components/price-category/price-category-subform.component';
// shared module
// do not provide services in Shared Modules! Especially not if you plan to use them in Lazy Loaded Modules!
@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        ReactiveFormsModule,
        NgxPaginationModule,
        DropzoneModule,
        SelectModule,
        MultiSelectSharedModule,
        NgSelectSharedModule,
        NgxPopperModule,
        QuillModule,
        NguiDatetimePickerModule,
        NouisliderModule
    ],
    declarations: [
        ValidationErrorComponent,
        MyDropZoneComponent,
        LimitLengthTdComponent,
PayerSubformComponent,
CollinsonUserSubformComponent,
CurrencySubformComponent,
PricePolicySubformComponent,
PayeeBankAccountSubformComponent,
DepositSubformComponent,
CountrySubformComponent,
RateSubformComponent,
CurrencyCountryConfigSubformComponent,
TransactionSubformComponent,
PayeeSubformComponent,
SysBankAccountSubformComponent,
PriceCategorySubformComponent,
    ],
    exports: [
        ValidationErrorComponent,
        MyDropZoneComponent,
        LimitLengthTdComponent,
PayerSubformComponent,
CollinsonUserSubformComponent,
CurrencySubformComponent,
PricePolicySubformComponent,
PayeeBankAccountSubformComponent,
DepositSubformComponent,
CountrySubformComponent,
RateSubformComponent,
CurrencyCountryConfigSubformComponent,
TransactionSubformComponent,
PayeeSubformComponent,
SysBankAccountSubformComponent,
PriceCategorySubformComponent,
    ]
})
export class SharedModule {
}
