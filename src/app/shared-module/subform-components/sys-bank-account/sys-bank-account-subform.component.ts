import {AfterViewInit, ChangeDetectorRef, Component, Input, OnInit} from '@angular/core';
import {FormGroup} from '@angular/forms';
import {Subject} from 'rxjs/Subject';
import {SysBankAccountModel} from '../../../pages/sys-bank-account/sys-bank-account.model';
import {ImageModel} from '../../../models/bases/image.model';
import {SysBankAccountService} from '../../../pages/sys-bank-account/sys-bank-account.service';

@Component({
    selector: 'sys-bank-account-subform-component',
    templateUrl: 'sys-bank-account-subform.component.html'
})
export class SysBankAccountSubformComponent implements OnInit, AfterViewInit {

    @Input('group')
    public myForm: FormGroup;

    @Input('sysBankAccount')
    public sysBankAccount: SysBankAccountModel;

    // public sysBankAccounts: CompanyModel[];
    // public imageSubject: Subject<ImageModel[]> = new Subject<ImageModel[]>();


// =====================================================================
// =============================Dropzone Variable=======================
// ============================Dropzone Variable========================
// =====================================================================

// =================================================================================
// =============================multi select subject Variable=======================
// ============================multi select subject Variable========================
// =================================================================================


// =================================================================================
// ===================================EnumList Variable=============================
// ===================================EnumList Variable=============================
// =================================================================================


    constructor(public ref: ChangeDetectorRef,
public sysBankAccountService: SysBankAccountService,
) {
    }

    ngOnInit(): void {
        if (!this.sysBankAccount) {
            this.sysBankAccount = new SysBankAccountModel();
        }
        // this.getCompanies();
this.getEnumList();
    }

    ngAfterViewInit() {
        // if (this.sysBankAccount) {
        //     this.imageSubject.next(this.sysBankAccount.brandImg);
        // }
    }

    equals(r1: any, r2: any) {
        if (r1 && r2) {
            return r1.pk === r2.pk;
        }
    }

    // getCompanies() {
    //     this.companyService.getAllFromStore().subscribe(resp => {
    //         console.log(resp);
    //         this.companies = resp.companys;
    //     });
    // }

    filesChanged($event) {
        console.log($event);
        // this.brandTag.brandImg = $event;
    }

// =====================================================================
// ===========================getEnumList===============================
getEnumList(){
    }
// ===========================getEnumList===============================
// =====================================================================

// =====================================================================
// =======================Multi Select Event============================











// =======================Multi Select Event============================
// =====================================================================

// =====================================================================
// =============================Dropzone================================
emitDropzoneFiles(){


}


// ============================Dropzone=================================
// =====================================================================

}
