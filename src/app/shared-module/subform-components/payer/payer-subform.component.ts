import {AfterViewInit, ChangeDetectorRef, Component, Input, OnInit} from '@angular/core';
import {FormGroup} from '@angular/forms';
import {Subject} from 'rxjs/Subject';
import {PayerModel} from '../../../pages/payer/payer.model';
import {ImageModel} from '../../../models/bases/image.model';
    import {CollinsonUserService} from '../../../pages/collinson-user/collinson-user.service';
import {PayerService} from '../../../pages/payer/payer.service';

@Component({
    selector: 'payer-subform-component',
    templateUrl: 'payer-subform.component.html'
})
export class PayerSubformComponent implements OnInit, AfterViewInit {

    @Input('group')
    public myForm: FormGroup;

    @Input('payer')
    public payer: PayerModel;

    // public payers: CompanyModel[];
    // public imageSubject: Subject<ImageModel[]> = new Subject<ImageModel[]>();


// =====================================================================
// =============================Dropzone Variable=======================
        public attachmentsImageSubject: Subject<ImageModel[] | any> = new Subject<ImageModel[] | any>();
// ============================Dropzone Variable========================
// =====================================================================

// =================================================================================
// =============================multi select subject Variable=======================
        public collinsonUserRefreshSubject = new Subject<any>();
        public collinsonUserForLoadSubject = new Subject<any>();
// ============================multi select subject Variable========================
// =================================================================================


// =================================================================================
// ===================================EnumList Variable=============================
// ===================================EnumList Variable=============================
// =================================================================================


    constructor(public ref: ChangeDetectorRef,
public payerService: PayerService,
    public collinsonUserService: CollinsonUserService,
) {
    }

    ngOnInit(): void {
        if (!this.payer) {
            this.payer = new PayerModel();
        }
        // this.getCompanies();
this.getEnumList();
    }

    ngAfterViewInit() {
        // if (this.payer) {
        //     this.imageSubject.next(this.payer.brandImg);
        // }
    }

    equals(r1: any, r2: any) {
        if (r1 && r2) {
            return r1.pk === r2.pk;
        }
    }

    // getCompanies() {
    //     this.companyService.getAllFromStore().subscribe(resp => {
    //         console.log(resp);
    //         this.companies = resp.companys;
    //     });
    // }

    filesChanged($event) {
        console.log($event);
        // this.brandTag.brandImg = $event;
    }

// =====================================================================
// ===========================getEnumList===============================
getEnumList(){
    }
// ===========================getEnumList===============================
// =====================================================================

// =====================================================================
// =======================Multi Select Event============================









        collinsonUserSelected($event) {
        this.payer.collinsonUser = $event;
    }






// =======================Multi Select Event============================
// =====================================================================

// =====================================================================
// =============================Dropzone================================
emitDropzoneFiles(){

        this.attachmentsImageSubject.next(this.payer.attachments);

}


        attachmentsFileObjectsChanged($event) {
        this.payer.attachments = $event;
    }
// ============================Dropzone=================================
// =====================================================================

}
