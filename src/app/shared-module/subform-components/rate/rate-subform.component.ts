import {AfterViewInit, ChangeDetectorRef, Component, Input, OnInit} from '@angular/core';
import {FormGroup} from '@angular/forms';
import {Subject} from 'rxjs/Subject';
import {RateModel} from '../../../pages/rate/rate.model';
import {ImageModel} from '../../../models/bases/image.model';
import {RateService} from '../../../pages/rate/rate.service';

@Component({
    selector: 'rate-subform-component',
    templateUrl: 'rate-subform.component.html'
})
export class RateSubformComponent implements OnInit, AfterViewInit {

    @Input('group')
    public myForm: FormGroup;

    @Input('rate')
    public rate: RateModel;

    // public rates: CompanyModel[];
    // public imageSubject: Subject<ImageModel[]> = new Subject<ImageModel[]>();


// =====================================================================
// =============================Dropzone Variable=======================
// ============================Dropzone Variable========================
// =====================================================================

// =================================================================================
// =============================multi select subject Variable=======================
// ============================multi select subject Variable========================
// =================================================================================


// =================================================================================
// ===================================EnumList Variable=============================
// ===================================EnumList Variable=============================
// =================================================================================


    constructor(public ref: ChangeDetectorRef,
public rateService: RateService,
) {
    }

    ngOnInit(): void {
        if (!this.rate) {
            this.rate = new RateModel();
        }
        // this.getCompanies();
this.getEnumList();
    }

    ngAfterViewInit() {
        // if (this.rate) {
        //     this.imageSubject.next(this.rate.brandImg);
        // }
    }

    equals(r1: any, r2: any) {
        if (r1 && r2) {
            return r1.pk === r2.pk;
        }
    }

    // getCompanies() {
    //     this.companyService.getAllFromStore().subscribe(resp => {
    //         console.log(resp);
    //         this.companies = resp.companys;
    //     });
    // }

    filesChanged($event) {
        console.log($event);
        // this.brandTag.brandImg = $event;
    }

// =====================================================================
// ===========================getEnumList===============================
getEnumList(){
    }
// ===========================getEnumList===============================
// =====================================================================

// =====================================================================
// =======================Multi Select Event============================









// =======================Multi Select Event============================
// =====================================================================

// =====================================================================
// =============================Dropzone================================
emitDropzoneFiles(){


}


// ============================Dropzone=================================
// =====================================================================

}
