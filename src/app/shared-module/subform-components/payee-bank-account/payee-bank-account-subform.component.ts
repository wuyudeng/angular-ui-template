import {AfterViewInit, ChangeDetectorRef, Component, Input, OnInit} from '@angular/core';
import {FormGroup} from '@angular/forms';
import {Subject} from 'rxjs/Subject';
import {PayeeBankAccountModel} from '../../../pages/payee-bank-account/payee-bank-account.model';
import {ImageModel} from '../../../models/bases/image.model';
import {PayeeBankAccountService} from '../../../pages/payee-bank-account/payee-bank-account.service';

@Component({
    selector: 'payee-bank-account-subform-component',
    templateUrl: 'payee-bank-account-subform.component.html'
})
export class PayeeBankAccountSubformComponent implements OnInit, AfterViewInit {

    @Input('group')
    public myForm: FormGroup;

    @Input('payeeBankAccount')
    public payeeBankAccount: PayeeBankAccountModel;

    // public payeeBankAccounts: CompanyModel[];
    // public imageSubject: Subject<ImageModel[]> = new Subject<ImageModel[]>();


// =====================================================================
// =============================Dropzone Variable=======================
// ============================Dropzone Variable========================
// =====================================================================

// =================================================================================
// =============================multi select subject Variable=======================
// ============================multi select subject Variable========================
// =================================================================================


// =================================================================================
// ===================================EnumList Variable=============================
// ===================================EnumList Variable=============================
// =================================================================================


    constructor(public ref: ChangeDetectorRef,
public payeeBankAccountService: PayeeBankAccountService,
) {
    }

    ngOnInit(): void {
        if (!this.payeeBankAccount) {
            this.payeeBankAccount = new PayeeBankAccountModel();
        }
        // this.getCompanies();
this.getEnumList();
    }

    ngAfterViewInit() {
        // if (this.payeeBankAccount) {
        //     this.imageSubject.next(this.payeeBankAccount.brandImg);
        // }
    }

    equals(r1: any, r2: any) {
        if (r1 && r2) {
            return r1.pk === r2.pk;
        }
    }

    // getCompanies() {
    //     this.companyService.getAllFromStore().subscribe(resp => {
    //         console.log(resp);
    //         this.companies = resp.companys;
    //     });
    // }

    filesChanged($event) {
        console.log($event);
        // this.brandTag.brandImg = $event;
    }

// =====================================================================
// ===========================getEnumList===============================
getEnumList(){
    }
// ===========================getEnumList===============================
// =====================================================================

// =====================================================================
// =======================Multi Select Event============================














// =======================Multi Select Event============================
// =====================================================================

// =====================================================================
// =============================Dropzone================================
emitDropzoneFiles(){


}


// ============================Dropzone=================================
// =====================================================================

}
