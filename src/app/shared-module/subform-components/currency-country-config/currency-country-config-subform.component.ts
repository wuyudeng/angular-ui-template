import {AfterViewInit, ChangeDetectorRef, Component, Input, OnInit} from '@angular/core';
import {FormGroup} from '@angular/forms';
import {Subject} from 'rxjs/Subject';
import {CurrencyCountryConfigModel} from '../../../pages/currency-country-config/currency-country-config.model';
import {ImageModel} from '../../../models/bases/image.model';
import {CurrencyCountryConfigService} from '../../../pages/currency-country-config/currency-country-config.service';

@Component({
    selector: 'currency-country-config-subform-component',
    templateUrl: 'currency-country-config-subform.component.html'
})
export class CurrencyCountryConfigSubformComponent implements OnInit, AfterViewInit {

    @Input('group')
    public myForm: FormGroup;

    @Input('currencyCountryConfig')
    public currencyCountryConfig: CurrencyCountryConfigModel;

    // public currencyCountryConfigs: CompanyModel[];
    // public imageSubject: Subject<ImageModel[]> = new Subject<ImageModel[]>();


// =====================================================================
// =============================Dropzone Variable=======================
// ============================Dropzone Variable========================
// =====================================================================

// =================================================================================
// =============================multi select subject Variable=======================
// ============================multi select subject Variable========================
// =================================================================================


// =================================================================================
// ===================================EnumList Variable=============================
// ===================================EnumList Variable=============================
// =================================================================================


    constructor(public ref: ChangeDetectorRef,
public currencyCountryConfigService: CurrencyCountryConfigService,
) {
    }

    ngOnInit(): void {
        if (!this.currencyCountryConfig) {
            this.currencyCountryConfig = new CurrencyCountryConfigModel();
        }
        // this.getCompanies();
this.getEnumList();
    }

    ngAfterViewInit() {
        // if (this.currencyCountryConfig) {
        //     this.imageSubject.next(this.currencyCountryConfig.brandImg);
        // }
    }

    equals(r1: any, r2: any) {
        if (r1 && r2) {
            return r1.pk === r2.pk;
        }
    }

    // getCompanies() {
    //     this.companyService.getAllFromStore().subscribe(resp => {
    //         console.log(resp);
    //         this.companies = resp.companys;
    //     });
    // }

    filesChanged($event) {
        console.log($event);
        // this.brandTag.brandImg = $event;
    }

// =====================================================================
// ===========================getEnumList===============================
getEnumList(){
    }
// ===========================getEnumList===============================
// =====================================================================

// =====================================================================
// =======================Multi Select Event============================






// =======================Multi Select Event============================
// =====================================================================

// =====================================================================
// =============================Dropzone================================
emitDropzoneFiles(){


}


// ============================Dropzone=================================
// =====================================================================

}
