import {AfterViewInit, ChangeDetectorRef, Component, Input, OnInit} from '@angular/core';
import {FormGroup} from '@angular/forms';
import {Subject} from 'rxjs/Subject';
import {TransactionModel} from '../../../pages/transaction/transaction.model';
import {ImageModel} from '../../../models/bases/image.model';
import {TransactionService} from '../../../pages/transaction/transaction.service';

@Component({
    selector: 'transaction-subform-component',
    templateUrl: 'transaction-subform.component.html'
})
export class TransactionSubformComponent implements OnInit, AfterViewInit {

    @Input('group')
    public myForm: FormGroup;

    @Input('transaction')
    public transaction: TransactionModel;

    // public transactions: CompanyModel[];
    // public imageSubject: Subject<ImageModel[]> = new Subject<ImageModel[]>();


// =====================================================================
// =============================Dropzone Variable=======================
// ============================Dropzone Variable========================
// =====================================================================

// =================================================================================
// =============================multi select subject Variable=======================
// ============================multi select subject Variable========================
// =================================================================================


// =================================================================================
// ===================================EnumList Variable=============================
    public transactionStatusEnumList: any;
    public transactionTypeEnumList: any;
// ===================================EnumList Variable=============================
// =================================================================================


    constructor(public ref: ChangeDetectorRef,
public transactionService: TransactionService,
) {
    }

    ngOnInit(): void {
        if (!this.transaction) {
            this.transaction = new TransactionModel();
        }
        // this.getCompanies();
this.getEnumList();
    }

    ngAfterViewInit() {
        // if (this.transaction) {
        //     this.imageSubject.next(this.transaction.brandImg);
        // }
    }

    equals(r1: any, r2: any) {
        if (r1 && r2) {
            return r1.pk === r2.pk;
        }
    }

    // getCompanies() {
    //     this.companyService.getAllFromStore().subscribe(resp => {
    //         console.log(resp);
    //         this.companies = resp.companys;
    //     });
    // }

    filesChanged($event) {
        console.log($event);
        // this.brandTag.brandImg = $event;
    }

// =====================================================================
// ===========================getEnumList===============================
getEnumList(){
this.transactionService.getEnumList('transactionStatusEnum').subscribe((resp: any) => {
console.log(resp);
this.transactionStatusEnumList = resp;
});
this.transactionService.getEnumList('transactionTypeEnum').subscribe((resp: any) => {
console.log(resp);
this.transactionTypeEnumList = resp;
});
    }
// ===========================getEnumList===============================
// =====================================================================

// =====================================================================
// =======================Multi Select Event============================








































// =======================Multi Select Event============================
// =====================================================================

// =====================================================================
// =============================Dropzone================================
emitDropzoneFiles(){


}


// ============================Dropzone=================================
// =====================================================================

}
