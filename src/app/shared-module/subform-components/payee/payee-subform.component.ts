import {AfterViewInit, ChangeDetectorRef, Component, Input, OnInit} from '@angular/core';
import {FormGroup} from '@angular/forms';
import {Subject} from 'rxjs/Subject';
import {PayeeModel} from '../../../pages/payee/payee.model';
import {ImageModel} from '../../../models/bases/image.model';
    import {PayerService} from '../../../pages/payer/payer.service';
    import {CollinsonUserService} from '../../../pages/collinson-user/collinson-user.service';
import {PayeeService} from '../../../pages/payee/payee.service';

@Component({
    selector: 'payee-subform-component',
    templateUrl: 'payee-subform.component.html'
})
export class PayeeSubformComponent implements OnInit, AfterViewInit {

    @Input('group')
    public myForm: FormGroup;

    @Input('payee')
    public payee: PayeeModel;

    // public payees: CompanyModel[];
    // public imageSubject: Subject<ImageModel[]> = new Subject<ImageModel[]>();


// =====================================================================
// =============================Dropzone Variable=======================
        public attachmentsImageSubject: Subject<ImageModel[] | any> = new Subject<ImageModel[] | any>();
// ============================Dropzone Variable========================
// =====================================================================

// =================================================================================
// =============================multi select subject Variable=======================
        public payerRefreshSubject = new Subject<any>();
        public payerForLoadSubject = new Subject<any>();
        public collinsonUserRefreshSubject = new Subject<any>();
        public collinsonUserForLoadSubject = new Subject<any>();
// ============================multi select subject Variable========================
// =================================================================================


// =================================================================================
// ===================================EnumList Variable=============================
// ===================================EnumList Variable=============================
// =================================================================================


    constructor(public ref: ChangeDetectorRef,
public payeeService: PayeeService,
    public payerService: PayerService,
    public collinsonUserService: CollinsonUserService,
) {
    }

    ngOnInit(): void {
        if (!this.payee) {
            this.payee = new PayeeModel();
        }
        // this.getCompanies();
this.getEnumList();
    }

    ngAfterViewInit() {
        // if (this.payee) {
        //     this.imageSubject.next(this.payee.brandImg);
        // }
    }

    equals(r1: any, r2: any) {
        if (r1 && r2) {
            return r1.pk === r2.pk;
        }
    }

    // getCompanies() {
    //     this.companyService.getAllFromStore().subscribe(resp => {
    //         console.log(resp);
    //         this.companies = resp.companys;
    //     });
    // }

    filesChanged($event) {
        console.log($event);
        // this.brandTag.brandImg = $event;
    }

// =====================================================================
// ===========================getEnumList===============================
getEnumList(){
    }
// ===========================getEnumList===============================
// =====================================================================

// =====================================================================
// =======================Multi Select Event============================









        payerSelected($event) {
        this.payee.payer = $event;
    }




        collinsonUserSelected($event) {
        this.payee.collinsonUser = $event;
    }


// =======================Multi Select Event============================
// =====================================================================

// =====================================================================
// =============================Dropzone================================
emitDropzoneFiles(){

        this.attachmentsImageSubject.next(this.payee.attachments);

}


        attachmentsFileObjectsChanged($event) {
        this.payee.attachments = $event;
    }
// ============================Dropzone=================================
// =====================================================================

}
