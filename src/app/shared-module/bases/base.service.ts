import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs/Observable';
import {Paging} from '../../models/bases/paging.model';
import {Constants} from '../../constants/app.constant';
import {Sorts} from '../../models/bases/sorts.model';
import 'rxjs/add/observable/throw';
import {AbstractControl, FormControl} from '@angular/forms';

export abstract class BaseService {

    public baseUrl = Constants.API_ENDPOINT + 'v1/';
    public url: string;
    public embeddedStr = '';

    constructor(public entity: string, public http: HttpClient) {
        this.url = this.baseUrl + entity;
    }

    getUrl() {
        return this.url;
    }

    getSearchUrl(searchCondition: string, paging: Paging, sortOprions: Sorts) {
        if (!searchCondition) {
            searchCondition = '';
        }
        let sortStr = sortOprions.getSortString();
        if (!sortStr) {
            sortStr = '';
        }
        return this.embeddedStr + searchCondition + sortStr;
    }

    getDownloadTemplateUrl() {
        // http://localhost:9999/v1/excel/template?rule=payer
        return this.baseUrl + 'excel/template?rule=' + this.entity;
    }

    getExcelUploadUrl() {
        // 'https://-----/v1/excel?rule=entity';
        return this.baseUrl + 'excel?rule=' + this.entity;
    }

    getEnumList(e: string) {
        e = e.replace('Enum', '');
        return this.http.get(`${this.baseUrl}enum/${e}`)
            .map((response) => response).catch(res => Observable.throw(res));
    }

    head(id: any, field: string, value: string) {
        field = field ? field : '';
        value = value ? value : '';
        const str = id ? (field + '/' + value + '?id=' + id) : field + '/' + value;
        return this.http.head(`${this.url}/${str}`)
            .map((response) => response).catch(res => Observable.throw(res));
    }

    // public setUrl(entity: string) {
    //     this.url = this.baseUrl + entity;
    // }
    getAll(searchStr ?: string) {
        searchStr = searchStr ? searchStr : '';
        let tempPaging = new Paging({pageNumber: 1, pageSize: 1000000});
        let idSortStr = 'sort=id,desc&';
        let finalStr = `${this.url}?${tempPaging.getPagingStr()}${searchStr}`;
        return this.http.get(finalStr)
            .map((response) => response).catch(res => Observable.throw(res));
    }

    getAllWithoutEmbeded() {
        let tempPaging = new Paging({pageNumber: 1, pageSize: 1000000});
        let idSortStr = 'sort=id,desc&';
        return this.http.get(`${this.url}?${tempPaging.getPagingStr()}`)
            .map((response) => response).catch(res => Observable.throw(res));
    }

    get(id) {
        return this.http.get(`${this.url}/${id}?${this.embeddedStr}`)
            .map((response) => response).catch(res => Observable.throw(res));
    }

    getAllByPaging(searchCondition: string, paging: Paging, sortOprions: Sorts) {
        if (!searchCondition) {
            searchCondition = '';
        }
        let sortStr = sortOprions.getSortString();
        if (!sortStr) {
            sortStr = '';
        }
        return this.http.get(`${this.url}?${paging.getPagingStr()}${this.embeddedStr + searchCondition + sortStr}`)
            .map((response) => response).catch(res => Observable.throw(res));
    }

    add(value: any) {
        return this.http.post(`${this.url}`, value)
            .map((response) => response).catch(res => Observable.throw(res));
    }

    update(id: number | string, value: any) {
        return this.http.put(`${this.url}/${id}`, value)
            .map((response) => response).catch(res => Observable.throw(res));
    }

    patch(id: number, value: any) {
        return this.http.patch(`${this.url}/${id}`, value)
            .map((response) => response).catch(res => Observable.throw(res));
    }

}
