import {Router} from '@angular/router';
import {FormGroup} from '@angular/forms';
import {Sorts} from '../../../models/bases/sorts.model';
import {Paging} from '../../../models/bases/paging.model';
import {PermissionConstant} from '../../../models/bases/permission-constant.model';
import {HelperService} from '../../../services/helper.service';
import {PermissionConstants} from '../../../constants/permission.constant';
import {environment} from '../../../../environments/environment';
import {Sort} from '../../../models/bases/sort.model';

export class ListBaseComponent {
    public sortOprions: Sorts = new Sorts();
    public permission: PermissionConstant;
    public paging: Paging;
    public selectedList: any[] = [];
    public listElements: any[] = [];
    public selectedIds: string;
    public isSelectAll: boolean;
    public baseUrl: string;

    public currentRouterUrl;

    public searchForm: FormGroup;
    public searchCondition: string;
    public loading: boolean;

    constructor(public router: Router,
                public helperService: HelperService) {
        this.permission = PermissionConstants.constants;
        this.paging = new Paging({pageNumber: 1, pageSize: 12});
        this.baseUrl = environment.apiUrl;
    }

    /**
     * ----- paging -----
     */
    pageChanged(event) {
        console.log(event);
        this.paging.pageNumber = event;
        this.refresh();
    }

    onPageSizeChange(event) {
        this.paging.pageNumber = 1;
        this.paging.pageSize = event;
        this.refresh();
    }

    public refresh() {

    };

    /**
     * ----- Sort  -----
     */
    changeSort(c: Sort) {
        console.log('change sort');
        if (c.isSortable) {
            this.sortOprions.sortColumns.map(s => {
                s.isActive = false;
            });
            c.isActive = true;
            c.isAsc = !c.isAsc;
            this.refresh();
        }
    }

    getSortClass(c: Sort) {
        if (c.isSortable && c.isActive) {
            return c.isAsc ? 'sorting_desc' : 'sorting_asc';
        } else if (c.isSortable && !c.isActive) {
            return 'sorting';
        } else {
            return '';
        }
    }

    /**
     *  ----------- select table items ----------
     * */

    select(p: any) {
        console.log('===================');
        console.log('select in base comp');
        this.selectedList = this.listElements.filter(d => d.isSelected === true);
        this.selectedIds = this.selectedList.map(e => e.id).join(',');
        console.log(this.selectedList);
        console.log(this.selectedIds);
        this.checkIfSelectAll();
    }

    selectAll() {

        if (this.isSelectAll) {
            this.listElements.map(d => {
                d.isSelected = true;
            });
            this.selectedList = JSON.parse(JSON.stringify(this.listElements));
            this.selectedIds = this.selectedList.map(e => e.id).join(',');
        } else {
            this.selectedList = [];
            this.listElements.map(d => {
                d.isSelected = false;
            });
            this.selectedIds = '';
        }
    }

    deSelectAll() {
        this.selectedList = [];
        this.listElements.map(d => {
            d.isSelected = false;
        });
        this.isSelectAll = false;
    }

    checkIfSelectAll() {
        if (!(this.selectedList.length === this.listElements.length)) {
            this.isSelectAll = false;
        } else {
            this.isSelectAll = true;
        }
    }


    // url functions

    resetCurrentRouterUrl() {
        this.currentRouterUrl = '';
        if (this.router.url.indexOf('?') > -1) {
            this.currentRouterUrl = this.router.url.substring(0, this.router.url.indexOf('?'));
        } else {
            this.currentRouterUrl = this.router.url
        }
    }


    //form

    debounceSearchForm() {
        this.searchCondition = '';
        this.searchForm.valueChanges.debounceTime(500).subscribe((form: any) => {

            this.resetCurrentRouterUrl();
            const urlTree = this.router.parseUrl(this.currentRouterUrl);
            urlTree.queryParams = this.helperService.generateQueryParams(urlTree.queryParams, form);

            this.router.navigateByUrl(urlTree).then(resp => {
                this.refresh();
            });

        });
    }

    patchFormValueFromRouter() {
        if ((this.router.url.indexOf('?') > -1)) {
            this.searchForm.patchValue(this.router.parseUrl(this.router.url).queryParams);
        }
    }

    // reset Form
    reset() {
        this.resetCurrentRouterUrl();
        this.router.navigateByUrl(this.currentRouterUrl).then(resp => {
            this.searchForm.reset();
        });
    }
}
