import {FormGroup} from '@angular/forms';
import {Subscription} from 'rxjs/Subscription';
import {ActivatedRoute} from '@angular/router';
import {Location} from '@angular/common';

export class FormBaseComponent {
    public loading: boolean;
    public myForm: FormGroup;
    public id: number;
    public isEdit = false;
    public subscription: Subscription;

    constructor(public activatedRoute: ActivatedRoute,
                public location: Location) {

    }

    getRouteParemeter() {
        this.subscription = this.activatedRoute
            .params
            .subscribe(params => {
                console.log(params);
                this.id = params['id'];
                if (this.id && this.id > 0) {
                    this.isEdit = true;
                    this.getItem();
                } else {
                    this.isEdit = false;
                }
                this.initFormControl();
            });
    }

    equals(r1: any, r2: any) {
        if (r1 && r2) {
            return r1.pk === r2.pk;
        }
    }

    initFormControl() {

    };

    goBack() {
        this.location.back();
    }

    public getItem() {
    }

    getQueryParams() {
        this.subscription = this.activatedRoute
            .queryParams
            .subscribe(params => {
            });
    }
}
