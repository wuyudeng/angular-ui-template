import {Injectable} from '@angular/core';
import {Router, CanActivate} from '@angular/router';
import {User} from '../../models/user.model';
import {MyNotifyService} from '../../services/my-notify.service';
import {AuthenticationService} from '../../services/authentication.service';
import {Constants} from '../../constants/app.constant';
import {LoginResp} from '../../models/login.resp.model';

@Injectable()
export class AuthGuard implements CanActivate {
    public user: User;

    constructor(public router: Router,
                public myNotifyService: MyNotifyService,
                public authService: AuthenticationService) {
        // this.router.events.subscribe((resp: any) => {
        //         this.checkExpired();
        //         debugger;
        //     }
        // );
    }

    canActivate() {
        console.log(' ------------ in guard ------------');
        if (localStorage.getItem(Constants.CurrentUser)) {
            this.user = JSON.parse(localStorage.getItem(Constants.CurrentUser));
            // logged in so return true
            this.checkExpired();
            return true;
        }
        // not logged in so redirect to login page
        this.router.navigate(['/auth/login']);
        return false;
    }

    checkExpired() {
        if (localStorage.getItem(Constants.CurrentUser)) {
            const loginResp: LoginResp = JSON.parse(localStorage.getItem(Constants.CurrentUser));
            const now = new Date().getTime() / 1000;
            if (now - loginResp.loginSecond > loginResp.expires_in) {
                this.destroyToken();
            }
        } else {
            this.router.navigate(['/auth/login']);
        }
    }

    destroyToken() {
        this.authService.logout();
        this.authService.logout();
        this.router.navigate(['/auth/login']);
    }
}
