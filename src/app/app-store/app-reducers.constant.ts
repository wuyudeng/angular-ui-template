import {ActionReducerMap} from '@ngrx/store';
import {AppStateInterface} from './app-state.interface';

import {payersReducer} from '../pages/payer/store/reducers';
import {collinsonUsersReducer} from '../pages/collinson-user/store/reducers';
import {currencysReducer} from '../pages/currency/store/reducers';
import {pricePolicysReducer} from '../pages/price-policy/store/reducers';
import {payeeBankAccountsReducer} from '../pages/payee-bank-account/store/reducers';
import {depositsReducer} from '../pages/deposit/store/reducers';
import {countrysReducer} from '../pages/country/store/reducers';
import {ratesReducer} from '../pages/rate/store/reducers';
import {currencyCountryConfigsReducer} from '../pages/currency-country-config/store/reducers';
import {transactionsReducer} from '../pages/transaction/store/reducers';
import {payeesReducer} from '../pages/payee/store/reducers';
import {sysBankAccountsReducer} from '../pages/sys-bank-account/store/reducers';
import {priceCategorysReducer} from '../pages/price-category/store/reducers';

export const AppReducerConstants: ActionReducerMap<AppStateInterface> = {
        payerList: payersReducer,
        collinsonUserList: collinsonUsersReducer,
        currencyList: currencysReducer,
        pricePolicyList: pricePolicysReducer,
        payeeBankAccountList: payeeBankAccountsReducer,
        depositList: depositsReducer,
        countryList: countrysReducer,
        rateList: ratesReducer,
        currencyCountryConfigList: currencyCountryConfigsReducer,
        transactionList: transactionsReducer,
        payeeList: payeesReducer,
        sysBankAccountList: sysBankAccountsReducer,
        priceCategoryList: priceCategorysReducer,
};
