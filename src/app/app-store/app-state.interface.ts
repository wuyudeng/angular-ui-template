    import {PayerStateInterface} from '../pages/payer/store/interfaces/state.interface';
    import {CollinsonUserStateInterface} from '../pages/collinson-user/store/interfaces/state.interface';
    import {CurrencyStateInterface} from '../pages/currency/store/interfaces/state.interface';
    import {PricePolicyStateInterface} from '../pages/price-policy/store/interfaces/state.interface';
    import {PayeeBankAccountStateInterface} from '../pages/payee-bank-account/store/interfaces/state.interface';
    import {DepositStateInterface} from '../pages/deposit/store/interfaces/state.interface';
    import {CountryStateInterface} from '../pages/country/store/interfaces/state.interface';
    import {RateStateInterface} from '../pages/rate/store/interfaces/state.interface';
    import {CurrencyCountryConfigStateInterface} from '../pages/currency-country-config/store/interfaces/state.interface';
    import {TransactionStateInterface} from '../pages/transaction/store/interfaces/state.interface';
    import {PayeeStateInterface} from '../pages/payee/store/interfaces/state.interface';
    import {SysBankAccountStateInterface} from '../pages/sys-bank-account/store/interfaces/state.interface';
    import {PriceCategoryStateInterface} from '../pages/price-category/store/interfaces/state.interface';

export interface AppStateInterface {
        payerList: PayerStateInterface,
        collinsonUserList: CollinsonUserStateInterface,
        currencyList: CurrencyStateInterface,
        pricePolicyList: PricePolicyStateInterface,
        payeeBankAccountList: PayeeBankAccountStateInterface,
        depositList: DepositStateInterface,
        countryList: CountryStateInterface,
        rateList: RateStateInterface,
        currencyCountryConfigList: CurrencyCountryConfigStateInterface,
        transactionList: TransactionStateInterface,
        payeeList: PayeeStateInterface,
        sysBankAccountList: SysBankAccountStateInterface,
        priceCategoryList: PriceCategoryStateInterface,
}
