import {CollinsonUserModel} from '../../collinson-user.model';

export interface CollinsonUserStateInterface {
    collinsonUsers: CollinsonUserModel[];

}