import {CollinsonUserModel} from '../../collinson-user.model';
import {AppStateInterface} from '../../../../app-store/app-state.interface';

export interface CollinsonUserFeatureStateInterface extends AppStateInterface {
    collinsonUsers: CollinsonUserModel[];
}

