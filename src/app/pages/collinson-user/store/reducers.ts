import {Ingredient} from '../../shared/ingredient.model';
import {CollinsonUserActions} from './actions/actions';
import {CollinsonUserActionConstants} from './actions/action.constant';
import {CollinsonUserInitialStateConstant} from './initial-state.constant';

export function collinsonUsersReducer(state = CollinsonUserInitialStateConstant, action: CollinsonUserActions) {
    switch (action.type) {
        case (CollinsonUserActionConstants.SET_CollinsonUserS):
            console.log('CollinsonUserActions.SET_CollinsonUserS');
            return {
                ...state,
                collinsonUsers: [...action.payload]
            };
        // case (CollinsonUserActionConstants.ADD_CollinsonUser):
        //     return {
        //         ...state,
        //         collinsonUsers: [...state.collinsonUsers, action.payload]
        //     };
        case (CollinsonUserActionConstants.UPDATE_CollinsonUser):
            const collinsonUser= state.collinsonUsers[action.payload.index];
            const updatedCollinsonUser= {
                ...collinsonUser,
                ...action.payload.updatedCollinsonUser
            };
            const collinsonUsers = [...state.collinsonUsers];
            collinsonUsers[action.payload.index] = updatedCollinsonUser;
            return {
                ...state,
                collinsonUsers: collinsonUsers
            };
        case (CollinsonUserActionConstants.DELETE_CollinsonUser):
            const oldCollinsonUsers = [...state.collinsonUsers];
            oldCollinsonUsers.splice(action.payload, 1);
            return {
                ...state,
                collinsonUsers: oldCollinsonUsers
            };
        default:
            return state;
    }
}
