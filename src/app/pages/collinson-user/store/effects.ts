import {Injectable} from '@angular/core';
import {Actions, Effect} from '@ngrx/effects';
import 'rxjs/add/operator/switchMap';
import 'rxjs/add/operator/withLatestFrom';
import {HttpClient} from '@angular/common/http';
import {Store} from '@ngrx/store';
import {CollinsonUserActionConstants} from './actions/action.constant';
import {FetchCollinsonUserAction} from './actions/fetch.action';
import {CollinsonUserService} from '../collinson-user.service';
import {CollinsonUserFeatureStateInterface} from './interfaces/feature-state.interface';

@Injectable()
export class CollinsonUserEffects {
    @Effect()
    collinsonUserFetch = this.actions$
        .ofType(CollinsonUserActionConstants.FETCH_CollinsonUserS)
        .switchMap((action: FetchCollinsonUserAction) => {

            console.log('CollinsonUserActions.FETCH_CollinsonUserS');
            return this.collinsonUserService.getAll();
        })
        .map(
            (collinsonUsers) => ({
                // type: CollinsonUserActionConstants.SET_CollinsonUserS,
                type: CollinsonUserActionConstants.SET_CollinsonUserS,
                payload: collinsonUsers.results
            }));
    //
    // @Effect({dispatch: false})
    // collinsonUserStore = this.actions$
    //     .ofType(CollinsonUserActionConstants.STORE_CollinsonUserS)
    //     .withLatestFrom(this.store.select('collinsonUsers'))
    //     .switchMap(([action, state]) => {
    //         const req = new HttpRequest('PUT', 'https://ng-collinsonUser-book-3adbb.firebaseio.com/collinsonUsers.json', state.collinsonUsers, {reportProgress: true});
    //         return this.httpClient.request(req);
    //     });

    constructor(private actions$: Actions,
                private httpClient: HttpClient,
                private collinsonUserService: CollinsonUserService,
                private store: Store<CollinsonUserFeatureStateInterface>) {
    }

}
