import {Action} from '@ngrx/store';
import {CollinsonUserModel} from '../../collinson-user.model';
import {CollinsonUserActionConstants} from './collinson-user-action.constant';

export class AddCollinsonUserAction implements Action {
    readonly type = CollinsonUserActionConstants.SET_CollinsonUserS;

    constructor(public payload: CollinsonUserModel[]) {
    }
}
