import {Action} from '@ngrx/store';
import {CollinsonUserActionConstants} from './action.constant';

export class FetchCollinsonUserAction implements Action {
    // readonly type = CollinsonUserActionConstants.FETCH_ADMINUSERS;
    readonly type = CollinsonUserActionConstants.FETCH_CollinsonUserS;

    constructor(public payload: { }) {
    }
}
