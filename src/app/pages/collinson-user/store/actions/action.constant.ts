export const CollinsonUserActionConstants = {
    SET_CollinsonUserS: 'SET_CollinsonUserS',
    ADD_CollinsonUser: 'ADD_CollinsonUser',
    UPDATE_CollinsonUser: 'UPDATE_CollinsonUser',
    DELETE_CollinsonUser: 'DELETE_CollinsonUser',
    STORE_CollinsonUserS: 'STORE_CollinsonUserS',
    FETCH_CollinsonUserS: 'FETCH_CollinsonUserS',
}