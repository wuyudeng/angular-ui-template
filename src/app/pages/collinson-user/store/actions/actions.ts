import {AddCollinsonUserAction} from './add.action';
import {DeleteCollinsonUserAction} from './delete.action';
import {FetchCollinsonUserAction} from './fetch.action';
import {SetCollinsonUserAction} from './set.action';
import {UpdateCollinsonUserAction} from './update.action';
import {StoreCollinsonUserAction} from './store.action';

export type CollinsonUserActions = SetCollinsonUserAction |
    AddCollinsonUserAction |
    UpdateCollinsonUserAction |
    DeleteCollinsonUserAction |
    StoreCollinsonUserAction |
    FetchCollinsonUserAction;
