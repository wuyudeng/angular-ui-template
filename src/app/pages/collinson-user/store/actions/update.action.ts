import {Action} from '@ngrx/store';
import {CollinsonUserActionConstants} from './action.constants';
import {CollinsonUserModel} from '../../collinson-user.model';

export class UpdateCollinsonUserAction implements Action {
    readonly type = CollinsonUserActionConstants.UPDATE_CollinsonUser;

    constructor(public payload: { index: number, updatedCollinsonUser: CollinsonUserModel }) {
    }
}
