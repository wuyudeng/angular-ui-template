import {Action} from '@ngrx/store';
import {CollinsonUserActionConstants} from './collinson-user-action.constant';

export class DeleteCollinsonUserAction implements Action {
    readonly type = CollinsonUserActionConstants.DELETE_CollinsonUser;

    constructor(public payload: number) {
    }
}
