import {Action} from '@ngrx/store';
import {CollinsonUserActionConstants} from './admin-user-action.constants';
import {CollinsonUserModel} from '../../collinson-user.model';

export class SetCollinsonUserAction implements Action {
    readonly type = CollinsonUserActionConstants.SET_CollinsonUserS;

    constructor(public payload: CollinsonUserModel[]) {
    }
}
