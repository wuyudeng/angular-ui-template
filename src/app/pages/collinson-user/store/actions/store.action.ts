import {Action} from '@ngrx/store';
import {CollinsonUserActionConstants} from './collinson-user-action.constants';

export class StoreCollinsonUserAction implements Action {
    readonly type = CollinsonUserActionConstants.STORE_CollinsonUserS;
    public payload: any;
}
