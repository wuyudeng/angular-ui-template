import {CollinsonUserStateInterface} from './interfaces/state.interface';

export const CollinsonUserInitialStateConstant: CollinsonUserStateInterface = {
    collinsonUsers: []
};