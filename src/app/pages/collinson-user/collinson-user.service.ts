import {Injectable, OnInit} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {BaseService} from '../../shared-module/bases/base.service';

import {Store} from '@ngrx/store';
import {CollinsonUserFeatureStateInterface} from './store/interfaces/feature-state.interface';
import {FetchCollinsonUserAction} from './store/actions/fetch.action';

@Injectable()
export class CollinsonUserService extends BaseService implements OnInit {

    constructor(public http: HttpClient,
public store: Store<CollinsonUserFeatureStateInterface>) {
        super('collinson-user', http);































































        this.embeddedStr = '&embedded=role,branch,introduced-by,granted-authorities,attachments,';
    }

    ngOnInit(): void {
    }

fetchAll() {
        this.store.dispatch(new FetchCollinsonUserAction({}));
    }

    getAllFromStore() {
        return this.store.select(o => o.collinsonUserList);
    }
}