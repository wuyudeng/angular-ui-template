import {Component, OnInit, ViewChild} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {HelperService} from '../../../services/helper.service';
import {ListBaseComponent} from '../../../shared-module/bases/list-base-component/list-base.component';
import {Router} from '@angular/router';
import {Subject} from 'rxjs/Subject';

import {CollinsonUserModel} from '../collinson-user.model';
import {CollinsonUserService} from '../collinson-user.service';

import {SortColumns} from './sort.columns';
import {Sorts} from '../../../models/bases/sorts.model';
import {DownloadService} from '../../../services/download.service';

@Component({
    selector: 'sa-collinson-user-list',
    templateUrl: './collinson-user-list.component.html',
})
export class CollinsonUserListComponent extends ListBaseComponent implements OnInit {
public searchForm: FormGroup;
public searchCondition: string;
public loading: boolean;

@ViewChild('lgImportByExcelModal') lgImportByExcelModal: any;
public excelUploadUrl: string;
public uploadAndImportSubject: Subject<string> = new Subject<string>();
public resetDropzoneSubject: Subject<string> = new Subject();
public exportUrl: string;

constructor(private formBuilder: FormBuilder,
public collinsonUserService: CollinsonUserService,
public router: Router,
public downloadService: DownloadService,
public helperService: HelperService) {
super(router, helperService);
this.sortOprions.sortColumns = SortColumns.Columns;
}

ngOnInit() {
this.excelUploadUrl = this.collinsonUserService.getExcelUploadUrl();
this.refresh();
this.buildSearchFrom();
this.debounceSearchForm();
}

download() {
this.downloadService.downloadFile(this.exportUrl, 'collinson-user-list' + Date.now().toString() + '.xls');
}

/**
* ----- modal functions BEGIN-----
*/
onImportByExcelModalHide() {
    this.lgImportByExcelModal.hide()
}

onFileUploaded(status) {
    this.refresh();
    this.lgImportByExcelModal.hide()
}

importByExceliConfirm() {
    this.uploadAndImportSubject.next('true');
}

importByExcel() {
    this.lgImportByExcelModal.show();
}

downloadExcelTemplate() {
this.downloadService.downloadFile(this.collinsonUserService.getDownloadTemplateUrl(), 'collinson-user-list-template' + Date.now().toString() + '.xls');
}
/**
* ----- modal functions END-----
*/

refresh() {
this.loading = true;
const searchStr = this.helperService.getSearchConditionByRouter(this.router);
this.exportUrl = this.collinsonUserService.getUrl() + '/excel?' + this.collinsonUserService.getSearchUrl(searchStr, this.paging, this.sortOprions);
this.exportUrl = this.exportUrl.replace('?&', '?');
this.collinsonUserService.getAllByPaging(searchStr, this.paging, this.sortOprions).subscribe((resp: any) => {
console.log(resp);
this.listElements = resp.content;
this.paging.totalSize = resp.totalElements;
this.loading = false;
}, err => {
this.loading = false;
});
}

/**
* ----- search form -----
*/
buildSearchFrom() {
this.searchForm = this.formBuilder.group({
        name: ['', [Validators.required]],
        username: ['', [Validators.required]],
        password: ['', [Validators.required]],
        email: ['', [Validators.required]],
        role: ['', [Validators.required]],
        branch: ['', [Validators.required]],
        introducedBy: ['', [Validators.required]],
        verify: ['', [Validators.required]],
        grantedAuthorities: ['', [Validators.required]],
        attachments: ['', [Validators.required]],
        userType: ['', [Validators.required]],
        expiresIn: ['', [Validators.required]],
        address: ['', [Validators.required]],
        city: ['', [Validators.required]],
        country: ['', [Validators.required]],
        maxPerTrx: ['', [Validators.required]],
        minPerTrx: ['', [Validators.required]],
        phone: ['', [Validators.required]],
        age: ['', [Validators.required]],
});
}
}