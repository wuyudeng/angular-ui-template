import {PayerModel} from '../../payer.model';
import {AppStateInterface} from '../../../../app-store/app-state.interface';

export interface PayerFeatureStateInterface extends AppStateInterface {
    payers: PayerModel[];
}

