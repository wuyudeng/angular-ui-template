import {Ingredient} from '../../shared/ingredient.model';
import {PayerActions} from './actions/actions';
import {PayerActionConstants} from './actions/action.constant';
import {PayerInitialStateConstant} from './initial-state.constant';

export function payersReducer(state = PayerInitialStateConstant, action: PayerActions) {
    switch (action.type) {
        case (PayerActionConstants.SET_PayerS):
            console.log('PayerActions.SET_PayerS');
            return {
                ...state,
                payers: [...action.payload]
            };
        // case (PayerActionConstants.ADD_Payer):
        //     return {
        //         ...state,
        //         payers: [...state.payers, action.payload]
        //     };
        case (PayerActionConstants.UPDATE_Payer):
            const payer= state.payers[action.payload.index];
            const updatedPayer= {
                ...payer,
                ...action.payload.updatedPayer
            };
            const payers = [...state.payers];
            payers[action.payload.index] = updatedPayer;
            return {
                ...state,
                payers: payers
            };
        case (PayerActionConstants.DELETE_Payer):
            const oldPayers = [...state.payers];
            oldPayers.splice(action.payload, 1);
            return {
                ...state,
                payers: oldPayers
            };
        default:
            return state;
    }
}
