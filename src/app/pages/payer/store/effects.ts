import {Injectable} from '@angular/core';
import {Actions, Effect} from '@ngrx/effects';
import 'rxjs/add/operator/switchMap';
import 'rxjs/add/operator/withLatestFrom';
import {HttpClient} from '@angular/common/http';
import {Store} from '@ngrx/store';
import {PayerActionConstants} from './actions/action.constant';
import {FetchPayerAction} from './actions/fetch.action';
import {PayerService} from '../payer.service';
import {PayerFeatureStateInterface} from './interfaces/feature-state.interface';

@Injectable()
export class PayerEffects {
    @Effect()
    payerFetch = this.actions$
        .ofType(PayerActionConstants.FETCH_PayerS)
        .switchMap((action: FetchPayerAction) => {

            console.log('PayerActions.FETCH_PayerS');
            return this.payerService.getAll();
        })
        .map(
            (payers) => ({
                // type: PayerActionConstants.SET_PayerS,
                type: PayerActionConstants.SET_PayerS,
                payload: payers.results
            }));
    //
    // @Effect({dispatch: false})
    // payerStore = this.actions$
    //     .ofType(PayerActionConstants.STORE_PayerS)
    //     .withLatestFrom(this.store.select('payers'))
    //     .switchMap(([action, state]) => {
    //         const req = new HttpRequest('PUT', 'https://ng-payer-book-3adbb.firebaseio.com/payers.json', state.payers, {reportProgress: true});
    //         return this.httpClient.request(req);
    //     });

    constructor(private actions$: Actions,
                private httpClient: HttpClient,
                private payerService: PayerService,
                private store: Store<PayerFeatureStateInterface>) {
    }

}
