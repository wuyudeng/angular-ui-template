import {Action} from '@ngrx/store';
import {PayerModel} from '../../payer.model';
import {PayerActionConstants} from './payer-action.constant';

export class AddPayerAction implements Action {
    readonly type = PayerActionConstants.SET_PayerS;

    constructor(public payload: PayerModel[]) {
    }
}
