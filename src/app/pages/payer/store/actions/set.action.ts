import {Action} from '@ngrx/store';
import {PayerActionConstants} from './admin-user-action.constants';
import {PayerModel} from '../../payer.model';

export class SetPayerAction implements Action {
    readonly type = PayerActionConstants.SET_PayerS;

    constructor(public payload: PayerModel[]) {
    }
}
