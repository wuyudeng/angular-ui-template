import {Action} from '@ngrx/store';
import {PayerActionConstants} from './action.constants';
import {PayerModel} from '../../payer.model';

export class UpdatePayerAction implements Action {
    readonly type = PayerActionConstants.UPDATE_Payer;

    constructor(public payload: { index: number, updatedPayer: PayerModel }) {
    }
}
