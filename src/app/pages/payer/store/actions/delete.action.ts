import {Action} from '@ngrx/store';
import {PayerActionConstants} from './payer-action.constant';

export class DeletePayerAction implements Action {
    readonly type = PayerActionConstants.DELETE_Payer;

    constructor(public payload: number) {
    }
}
