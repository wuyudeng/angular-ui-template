import {Action} from '@ngrx/store';
import {PayerActionConstants} from './action.constant';

export class FetchPayerAction implements Action {
    // readonly type = PayerActionConstants.FETCH_ADMINUSERS;
    readonly type = PayerActionConstants.FETCH_PayerS;

    constructor(public payload: { }) {
    }
}
