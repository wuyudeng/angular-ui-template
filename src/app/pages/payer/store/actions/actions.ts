import {AddPayerAction} from './add.action';
import {DeletePayerAction} from './delete.action';
import {FetchPayerAction} from './fetch.action';
import {SetPayerAction} from './set.action';
import {UpdatePayerAction} from './update.action';
import {StorePayerAction} from './store.action';

export type PayerActions = SetPayerAction |
    AddPayerAction |
    UpdatePayerAction |
    DeletePayerAction |
    StorePayerAction |
    FetchPayerAction;
