import {Action} from '@ngrx/store';
import {PayerActionConstants} from './payer-action.constants';

export class StorePayerAction implements Action {
    readonly type = PayerActionConstants.STORE_PayerS;
    public payload: any;
}
