export const PayerActionConstants = {
    SET_PayerS: 'SET_PayerS',
    ADD_Payer: 'ADD_Payer',
    UPDATE_Payer: 'UPDATE_Payer',
    DELETE_Payer: 'DELETE_Payer',
    STORE_PayerS: 'STORE_PayerS',
    FETCH_PayerS: 'FETCH_PayerS',
}