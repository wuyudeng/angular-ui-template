import {PayerStateInterface} from './interfaces/state.interface';

export const PayerInitialStateConstant: PayerStateInterface = {
    payers: []
};