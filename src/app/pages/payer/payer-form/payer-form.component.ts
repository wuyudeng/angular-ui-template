import {ChangeDetectorRef, Component, OnInit} from '@angular/core';
import {FormArray, FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {Subscription} from 'rxjs/Subscription';
import {ActivatedRoute, Router} from '@angular/router';
import {Location} from '@angular/common';
import {AutoUnsubscribe} from 'ngx-auto-unsubscribe';
import {Subject} from 'rxjs/Subject';

import {FormBaseComponent} from '../../../shared-module/bases/form-base-component/form-base.component';
import {MyNotifyService} from '../../../services/my-notify.service';
import {ImageModel} from '../../../models/bases/image.model';

import {PayerModel} from '../payer.model';
import {PayerService} from '../payer.service';
import {ValidationPattern} from '../../../shared-module/bases/validation-error/validation.pattern';
declare var $: any;

    import {CollinsonUserService} from '../../collinson-user/collinson-user.service';

@Component({
selector: 'sa-payer-form',
templateUrl: './payer-form.component.html',
})
@AutoUnsubscribe()
export class PayerFormComponent extends FormBaseComponent implements OnInit {
public loading: boolean;
public myForm: FormGroup;
public id: number;
public isEdit = false;
public subscription: Subscription;
public payer: PayerModel = new PayerModel();
public currentDate = new Date();
// =====================================================================
// =============================Dropzone Variable=======================
        public attachmentsImageSubject: Subject<ImageModel[] | any> = new Subject<ImageModel[] | any>();
// ============================Dropzone Variable========================
// =====================================================================

// =================================================================================
// =============================multi select subject Variable=======================
        public collinsonUserRefreshSubject = new Subject<any>();
        public collinsonUserForLoadSubject = new Subject<any>();
// ============================multi select subject Variable========================
// =================================================================================


// =================================================================================
// ===================================EnumList Variable=============================
// ===================================EnumList Variable=============================
// =================================================================================


constructor(public formBuiler: FormBuilder,
public ref: ChangeDetectorRef,
public router: Router,
public location: Location,
public myNotifyService: MyNotifyService,
public payerService: PayerService,
        public collinsonUserService: CollinsonUserService,
public activatedRoute: ActivatedRoute) {
super(activatedRoute, location);
}

ngOnInit() {
this.getEnumList();
this.getRouteParemeter();
this.getQueryParams();
//this.initFormControl();
}

initFormControl() {
this.myForm = this.formBuiler.group({
        id: new FormControl('', {
        
        
            }),
        name: new FormControl('', {
        validators:[Validators.required,],
        asyncValidators:[ValidationPattern.duplicateValidate(this.id, 'name', this.payerService)],updateOn: 'blur'
            }),
        age: new FormControl('', {
        validators:[Validators.required,Validators.min(18),Validators.max(65),],
        
            }),
        address: new FormControl('', {
        validators:[Validators.required,ValidationPattern.commonRegexp(/^[a-zA-Z/0-9 ]+$/),Validators.minLength(0),Validators.maxLength(35),],
        
            }),
        email: new FormControl('', {
        validators:[Validators.required,],
        
            }),
        city: new FormControl('', {
        validators:[Validators.required,],
        
            }),
        richText: new FormControl('', {
        
        
            }),
        textArea: new FormControl('', {
        
        
            }),
        country: new FormControl('', {
        validators:[Validators.required,],
        
            }),
        collinsonUser: new FormControl('', {
        
        
            }),
        payees: this.formBuiler.array([]),
        transactions: new FormControl('', {
        
        
            }),
        verify: new FormControl('', {
        validators:[Validators.required,],
        
            }),
        testDate: new FormControl('', {
        
        
            }),
        attachments: new FormControl('', {
        
        
            }),
});
}

getEnumList(){
    }

getItem() {
this.loading = true;
this.payerService.get(this.id).subscribe((resp: any) => {
this.loading = false;
console.log(resp);
this.payer = resp;
this.emitDropzoneFiles();
}, err => {
this.loading = false;
});
}

onSubmit({value, valid}: { value: PayerModel, valid: boolean }) {
if(valid){
if (!this.isEdit) {
this. payerService.add(value).subscribe((resp: any) => {
console.log(resp);
this.goBack();
}, err => {
console.log(err);
this.myNotifyService.notifyFail(err.error.error);
})
} else {
this.payerService.update(this.payer.id, value).subscribe((resp: any) => {
console.log(resp);
this.myNotifyService.notifySuccess('The payer is successfully updated.');
this.goBack();
}, err => {
console.log(err);
this.myNotifyService.notifyFail(err.error.error);
})
}
}else {
console.log(this.myForm);
ValidationPattern.validateAllFormFields(this.myForm);
setTimeout(t => {
                $('html, body').animate({
                    scrollTop: $('.text-danger:visible:first').offset().top - 100
                }, 1000);
            });
this.myNotifyService.notifyFail('Some fields are invalid, please check.');
}
}

// =====================================================================
// =======================Multi Select Event============================









        collinsonUserSelected($event) {
        this.payer.collinsonUser = $event;
    }






// =======================Multi Select Event============================
// =====================================================================

// =====================================================================
// =============================Dropzone================================
emitDropzoneFiles(){

        this.attachmentsImageSubject.next(this.payer.attachments);

}


        attachmentsFileObjectsChanged($event) {
        this.payer.attachments = $event;
    }
// ============================Dropzone=================================
// =====================================================================

// ===========================================================================
// =============================daymically add================================


// ================================put in subform's parent form========================================
// ===============================Payer SubForm============================
loadpayeesForm() {
    const payeesCtrls = (<FormArray>this.myForm.controls['payees']).controls;

    if (payeesCtrls && this.payer.payees) {
        while (payeesCtrls.length < this.payer.payees.length) {
            this.addpayeesFormControl();
        }
    }
    // this.ref.markForCheck();
    // this.ref.detectChanges();
}

addpayeesFormControl() {
    const control = <FormArray>this.myForm.controls['payees'];
    const addrCtrl = this.initpayeesFormControl();
    control.push(addrCtrl);
}

removepayeesFormControl(i: number) {
    const control = <FormArray> this.myForm.controls['payees'];
    control.removeAt(i);
}

initpayeesFormControl() {
   return this.formBuiler.group({
        id: new FormControl('', {
        
        }),
        name: new FormControl('', {
        validators:[Validators.required,],
        }),
        country: new FormControl('', {
        validators:[Validators.required,],
        }),
        addressLine1: new FormControl('', {
        validators:[Validators.required,],
        }),
        addressLine2: new FormControl('', {
        validators:[Validators.required,],
        }),
        city: new FormControl('', {
        validators:[Validators.required,],
        }),
        postCode: new FormControl('', {
        
        }),
        attachments: new FormControl('', {
        
        }),
        enable: new FormControl('', {
        validators:[Validators.required,],
        }),
        payer: new FormControl('', {
        
        }),
        payeeBankAccounts: new FormControl('', {
        
        }),
        payAmount: new FormControl('', {
        
        }),
        transactions: new FormControl('', {
        
        }),
        collinsonUser: new FormControl('', {
        
        }),
        verify: new FormControl('', {
        
        }),
    });
}

getpayees(i: number) {
    if (this.isEdit && this.payer && this.payer.payees) {
        return this.payer.payees[i];
    }
}

getpayeesFormData() {
    return <FormArray>this.myForm.get('payees');
}

// ===============================Payer SubForm============================
// ========================================================================

// ============================daymically add=================================
// ===========================================================================



}
