import {Injectable, OnInit} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {BaseService} from '../../shared-module/bases/base.service';

import {Store} from '@ngrx/store';
import {PayerFeatureStateInterface} from './store/interfaces/feature-state.interface';
import {FetchPayerAction} from './store/actions/fetch.action';

@Injectable()
export class PayerService extends BaseService implements OnInit {

    constructor(public http: HttpClient,
public store: Store<PayerFeatureStateInterface>) {
        super('payer', http);













































        this.embeddedStr = '&embedded=collinson-user,payees,transactions,attachments,';
    }

    ngOnInit(): void {
    }

fetchAll() {
        this.store.dispatch(new FetchPayerAction({}));
    }

    getAllFromStore() {
        return this.store.select(o => o.payerList);
    }
}