import {Injectable, OnInit} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {BaseService} from '../../shared-module/bases/base.service';

import {Store} from '@ngrx/store';
import {PayeeBankAccountFeatureStateInterface} from './store/interfaces/feature-state.interface';
import {FetchPayeeBankAccountAction} from './store/actions/fetch.action';

@Injectable()
export class PayeeBankAccountService extends BaseService implements OnInit {

    constructor(public http: HttpClient,
public store: Store<PayeeBankAccountFeatureStateInterface>) {
        super('payee-bank-account', http);










































        this.embeddedStr = '&embedded=payee,collinson-user,';
    }

    ngOnInit(): void {
    }

fetchAll() {
        this.store.dispatch(new FetchPayeeBankAccountAction({}));
    }

    getAllFromStore() {
        return this.store.select(o => o.payeeBankAccountList);
    }
}