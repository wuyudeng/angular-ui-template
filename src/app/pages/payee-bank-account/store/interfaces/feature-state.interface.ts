import {PayeeBankAccountModel} from '../../payee-bank-account.model';
import {AppStateInterface} from '../../../../app-store/app-state.interface';

export interface PayeeBankAccountFeatureStateInterface extends AppStateInterface {
    payeeBankAccounts: PayeeBankAccountModel[];
}

