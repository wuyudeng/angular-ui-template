import {PayeeBankAccountModel} from '../../payee-bank-account.model';

export interface PayeeBankAccountStateInterface {
    payeeBankAccounts: PayeeBankAccountModel[];

}