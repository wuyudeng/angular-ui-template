import {Injectable} from '@angular/core';
import {Actions, Effect} from '@ngrx/effects';
import 'rxjs/add/operator/switchMap';
import 'rxjs/add/operator/withLatestFrom';
import {HttpClient} from '@angular/common/http';
import {Store} from '@ngrx/store';
import {PayeeBankAccountActionConstants} from './actions/action.constant';
import {FetchPayeeBankAccountAction} from './actions/fetch.action';
import {PayeeBankAccountService} from '../payee-bank-account.service';
import {PayeeBankAccountFeatureStateInterface} from './interfaces/feature-state.interface';

@Injectable()
export class PayeeBankAccountEffects {
    @Effect()
    payeeBankAccountFetch = this.actions$
        .ofType(PayeeBankAccountActionConstants.FETCH_PayeeBankAccountS)
        .switchMap((action: FetchPayeeBankAccountAction) => {

            console.log('PayeeBankAccountActions.FETCH_PayeeBankAccountS');
            return this.payeeBankAccountService.getAll();
        })
        .map(
            (payeeBankAccounts) => ({
                // type: PayeeBankAccountActionConstants.SET_PayeeBankAccountS,
                type: PayeeBankAccountActionConstants.SET_PayeeBankAccountS,
                payload: payeeBankAccounts.results
            }));
    //
    // @Effect({dispatch: false})
    // payeeBankAccountStore = this.actions$
    //     .ofType(PayeeBankAccountActionConstants.STORE_PayeeBankAccountS)
    //     .withLatestFrom(this.store.select('payeeBankAccounts'))
    //     .switchMap(([action, state]) => {
    //         const req = new HttpRequest('PUT', 'https://ng-payeeBankAccount-book-3adbb.firebaseio.com/payeeBankAccounts.json', state.payeeBankAccounts, {reportProgress: true});
    //         return this.httpClient.request(req);
    //     });

    constructor(private actions$: Actions,
                private httpClient: HttpClient,
                private payeeBankAccountService: PayeeBankAccountService,
                private store: Store<PayeeBankAccountFeatureStateInterface>) {
    }

}
