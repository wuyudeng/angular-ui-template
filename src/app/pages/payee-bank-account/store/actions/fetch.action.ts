import {Action} from '@ngrx/store';
import {PayeeBankAccountActionConstants} from './action.constant';

export class FetchPayeeBankAccountAction implements Action {
    // readonly type = PayeeBankAccountActionConstants.FETCH_ADMINUSERS;
    readonly type = PayeeBankAccountActionConstants.FETCH_PayeeBankAccountS;

    constructor(public payload: { }) {
    }
}
