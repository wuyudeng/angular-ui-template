import {Action} from '@ngrx/store';
import {PayeeBankAccountActionConstants} from './admin-user-action.constants';
import {PayeeBankAccountModel} from '../../payee-bank-account.model';

export class SetPayeeBankAccountAction implements Action {
    readonly type = PayeeBankAccountActionConstants.SET_PayeeBankAccountS;

    constructor(public payload: PayeeBankAccountModel[]) {
    }
}
