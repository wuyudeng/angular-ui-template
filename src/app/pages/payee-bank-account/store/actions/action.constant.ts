export const PayeeBankAccountActionConstants = {
    SET_PayeeBankAccountS: 'SET_PayeeBankAccountS',
    ADD_PayeeBankAccount: 'ADD_PayeeBankAccount',
    UPDATE_PayeeBankAccount: 'UPDATE_PayeeBankAccount',
    DELETE_PayeeBankAccount: 'DELETE_PayeeBankAccount',
    STORE_PayeeBankAccountS: 'STORE_PayeeBankAccountS',
    FETCH_PayeeBankAccountS: 'FETCH_PayeeBankAccountS',
}