import {Action} from '@ngrx/store';
import {PayeeBankAccountActionConstants} from './action.constants';
import {PayeeBankAccountModel} from '../../payee-bank-account.model';

export class UpdatePayeeBankAccountAction implements Action {
    readonly type = PayeeBankAccountActionConstants.UPDATE_PayeeBankAccount;

    constructor(public payload: { index: number, updatedPayeeBankAccount: PayeeBankAccountModel }) {
    }
}
