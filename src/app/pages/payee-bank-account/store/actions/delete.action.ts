import {Action} from '@ngrx/store';
import {PayeeBankAccountActionConstants} from './payee-bank-account-action.constant';

export class DeletePayeeBankAccountAction implements Action {
    readonly type = PayeeBankAccountActionConstants.DELETE_PayeeBankAccount;

    constructor(public payload: number) {
    }
}
