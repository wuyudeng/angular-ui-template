import {Action} from '@ngrx/store';
import {PayeeBankAccountModel} from '../../payee-bank-account.model';
import {PayeeBankAccountActionConstants} from './payee-bank-account-action.constant';

export class AddPayeeBankAccountAction implements Action {
    readonly type = PayeeBankAccountActionConstants.SET_PayeeBankAccountS;

    constructor(public payload: PayeeBankAccountModel[]) {
    }
}
