import {Action} from '@ngrx/store';
import {PayeeBankAccountActionConstants} from './payee-bank-account-action.constants';

export class StorePayeeBankAccountAction implements Action {
    readonly type = PayeeBankAccountActionConstants.STORE_PayeeBankAccountS;
    public payload: any;
}
