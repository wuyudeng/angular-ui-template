import {AddPayeeBankAccountAction} from './add.action';
import {DeletePayeeBankAccountAction} from './delete.action';
import {FetchPayeeBankAccountAction} from './fetch.action';
import {SetPayeeBankAccountAction} from './set.action';
import {UpdatePayeeBankAccountAction} from './update.action';
import {StorePayeeBankAccountAction} from './store.action';

export type PayeeBankAccountActions = SetPayeeBankAccountAction |
    AddPayeeBankAccountAction |
    UpdatePayeeBankAccountAction |
    DeletePayeeBankAccountAction |
    StorePayeeBankAccountAction |
    FetchPayeeBankAccountAction;
