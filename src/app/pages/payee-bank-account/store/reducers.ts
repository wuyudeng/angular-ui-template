import {Ingredient} from '../../shared/ingredient.model';
import {PayeeBankAccountActions} from './actions/actions';
import {PayeeBankAccountActionConstants} from './actions/action.constant';
import {PayeeBankAccountInitialStateConstant} from './initial-state.constant';

export function payeeBankAccountsReducer(state = PayeeBankAccountInitialStateConstant, action: PayeeBankAccountActions) {
    switch (action.type) {
        case (PayeeBankAccountActionConstants.SET_PayeeBankAccountS):
            console.log('PayeeBankAccountActions.SET_PayeeBankAccountS');
            return {
                ...state,
                payeeBankAccounts: [...action.payload]
            };
        // case (PayeeBankAccountActionConstants.ADD_PayeeBankAccount):
        //     return {
        //         ...state,
        //         payeeBankAccounts: [...state.payeeBankAccounts, action.payload]
        //     };
        case (PayeeBankAccountActionConstants.UPDATE_PayeeBankAccount):
            const payeeBankAccount= state.payeeBankAccounts[action.payload.index];
            const updatedPayeeBankAccount= {
                ...payeeBankAccount,
                ...action.payload.updatedPayeeBankAccount
            };
            const payeeBankAccounts = [...state.payeeBankAccounts];
            payeeBankAccounts[action.payload.index] = updatedPayeeBankAccount;
            return {
                ...state,
                payeeBankAccounts: payeeBankAccounts
            };
        case (PayeeBankAccountActionConstants.DELETE_PayeeBankAccount):
            const oldPayeeBankAccounts = [...state.payeeBankAccounts];
            oldPayeeBankAccounts.splice(action.payload, 1);
            return {
                ...state,
                payeeBankAccounts: oldPayeeBankAccounts
            };
        default:
            return state;
    }
}
