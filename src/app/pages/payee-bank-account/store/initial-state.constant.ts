import {PayeeBankAccountStateInterface} from './interfaces/state.interface';

export const PayeeBankAccountInitialStateConstant: PayeeBankAccountStateInterface = {
    payeeBankAccounts: []
};