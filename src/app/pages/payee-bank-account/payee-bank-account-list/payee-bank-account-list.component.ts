import {Component, OnInit, ViewChild} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {HelperService} from '../../../services/helper.service';
import {ListBaseComponent} from '../../../shared-module/bases/list-base-component/list-base.component';
import {Router} from '@angular/router';
import {Subject} from 'rxjs/Subject';

import {PayeeBankAccountModel} from '../payee-bank-account.model';
import {PayeeBankAccountService} from '../payee-bank-account.service';

import {SortColumns} from './sort.columns';
import {Sorts} from '../../../models/bases/sorts.model';
import {DownloadService} from '../../../services/download.service';

@Component({
    selector: 'sa-payee-bank-account-list',
    templateUrl: './payee-bank-account-list.component.html',
})
export class PayeeBankAccountListComponent extends ListBaseComponent implements OnInit {
public searchForm: FormGroup;
public searchCondition: string;
public loading: boolean;

@ViewChild('lgImportByExcelModal') lgImportByExcelModal: any;
public excelUploadUrl: string;
public uploadAndImportSubject: Subject<string> = new Subject<string>();
public resetDropzoneSubject: Subject<string> = new Subject();
public exportUrl: string;

constructor(private formBuilder: FormBuilder,
public payeeBankAccountService: PayeeBankAccountService,
public router: Router,
public downloadService: DownloadService,
public helperService: HelperService) {
super(router, helperService);
this.sortOprions.sortColumns = SortColumns.Columns;
}

ngOnInit() {
this.excelUploadUrl = this.payeeBankAccountService.getExcelUploadUrl();
this.refresh();
this.buildSearchFrom();
this.debounceSearchForm();
}

download() {
this.downloadService.downloadFile(this.exportUrl, 'payee-bank-account-list' + Date.now().toString() + '.xls');
}

/**
* ----- modal functions BEGIN-----
*/
onImportByExcelModalHide() {
    this.lgImportByExcelModal.hide()
}

onFileUploaded(status) {
    this.refresh();
    this.lgImportByExcelModal.hide()
}

importByExceliConfirm() {
    this.uploadAndImportSubject.next('true');
}

importByExcel() {
    this.lgImportByExcelModal.show();
}

downloadExcelTemplate() {
this.downloadService.downloadFile(this.payeeBankAccountService.getDownloadTemplateUrl(), 'payee-bank-account-list-template' + Date.now().toString() + '.xls');
}
/**
* ----- modal functions END-----
*/

refresh() {
this.loading = true;
const searchStr = this.helperService.getSearchConditionByRouter(this.router);
this.exportUrl = this.payeeBankAccountService.getUrl() + '/excel?' + this.payeeBankAccountService.getSearchUrl(searchStr, this.paging, this.sortOprions);
this.exportUrl = this.exportUrl.replace('?&', '?');
this.payeeBankAccountService.getAllByPaging(searchStr, this.paging, this.sortOprions).subscribe((resp: any) => {
console.log(resp);
this.listElements = resp.content;
this.paging.totalSize = resp.totalElements;
this.loading = false;
}, err => {
this.loading = false;
});
}

/**
* ----- search form -----
*/
buildSearchFrom() {
this.searchForm = this.formBuilder.group({
        accountName: ['', [Validators.required]],
        accountNumber: ['', [Validators.required]],
        accountCountry: ['', [Validators.required]],
        currency: ['', [Validators.required]],
        bankName: ['', [Validators.required]],
        branchName: ['', [Validators.required]],
        clearingCode: ['', [Validators.required]],
        clearingCodeType: ['', [Validators.required]],
        swiftCode: ['', [Validators.required]],
        branchCity: ['', [Validators.required]],
        branchAddress: ['', [Validators.required]],
        payee: ['', [Validators.required]],
        collinsonUser: ['', [Validators.required]],
});
}
}