import {Injectable, OnInit} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {BaseService} from '../../shared-module/bases/base.service';

import {Store} from '@ngrx/store';
import {PricePolicyFeatureStateInterface} from './store/interfaces/feature-state.interface';
import {FetchPricePolicyAction} from './store/actions/fetch.action';

@Injectable()
export class PricePolicyService extends BaseService implements OnInit {

    constructor(public http: HttpClient,
public store: Store<PricePolicyFeatureStateInterface>) {
        super('price-policy', http);










































        this.embeddedStr = '&embedded=price-category,';
    }

    ngOnInit(): void {
    }

fetchAll() {
        this.store.dispatch(new FetchPricePolicyAction({}));
    }

    getAllFromStore() {
        return this.store.select(o => o.pricePolicyList);
    }
}