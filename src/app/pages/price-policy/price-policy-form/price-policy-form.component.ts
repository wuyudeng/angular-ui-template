import {ChangeDetectorRef, Component, OnInit} from '@angular/core';
import {FormArray, FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {Subscription} from 'rxjs/Subscription';
import {ActivatedRoute, Router} from '@angular/router';
import {Location} from '@angular/common';
import {AutoUnsubscribe} from 'ngx-auto-unsubscribe';
import {Subject} from 'rxjs/Subject';

import {FormBaseComponent} from '../../../shared-module/bases/form-base-component/form-base.component';
import {MyNotifyService} from '../../../services/my-notify.service';
import {ImageModel} from '../../../models/bases/image.model';

import {PricePolicyModel} from '../price-policy.model';
import {PricePolicyService} from '../price-policy.service';
import {ValidationPattern} from '../../../shared-module/bases/validation-error/validation.pattern';
declare var $: any;


@Component({
selector: 'sa-price-policy-form',
templateUrl: './price-policy-form.component.html',
})
@AutoUnsubscribe()
export class PricePolicyFormComponent extends FormBaseComponent implements OnInit {
public loading: boolean;
public myForm: FormGroup;
public id: number;
public isEdit = false;
public subscription: Subscription;
public pricePolicy: PricePolicyModel = new PricePolicyModel();
public currentDate = new Date();
// =====================================================================
// =============================Dropzone Variable=======================
// ============================Dropzone Variable========================
// =====================================================================

// =================================================================================
// =============================multi select subject Variable=======================
// ============================multi select subject Variable========================
// =================================================================================


// =================================================================================
// ===================================EnumList Variable=============================
// ===================================EnumList Variable=============================
// =================================================================================


constructor(public formBuiler: FormBuilder,
public ref: ChangeDetectorRef,
public router: Router,
public location: Location,
public myNotifyService: MyNotifyService,
public pricePolicyService: PricePolicyService,
public activatedRoute: ActivatedRoute) {
super(activatedRoute, location);
}

ngOnInit() {
this.getEnumList();
this.getRouteParemeter();
this.getQueryParams();
//this.initFormControl();
}

initFormControl() {
this.myForm = this.formBuiler.group({
        id: new FormControl('', {
        
        
            }),
        buyCurrency: new FormControl('', {
        
        
            }),
        sellCurrency: new FormControl('', {
        
        
            }),
        sysBuyMargin: new FormControl('', {
        
        
            }),
        sysSellMargin: new FormControl('', {
        
        
            }),
        bankBuyMargin: new FormControl('', {
        
        
            }),
        bankSellMargin: new FormControl('', {
        
        
            }),
        priceCategory: new FormControl('', {
        
        
            }),
        baseBuyPrice: new FormControl('', {
        
        
            }),
        baseSellPrice: new FormControl('', {
        
        
            }),
        sysBuyPrice: new FormControl('', {
        
        
            }),
        sysSellPrice: new FormControl('', {
        
        
            }),
        bankBuyPrice: new FormControl('', {
        
        
            }),
        bankSellPrice: new FormControl('', {
        
        
            }),
});
}

getEnumList(){
    }

getItem() {
this.loading = true;
this.pricePolicyService.get(this.id).subscribe((resp: any) => {
this.loading = false;
console.log(resp);
this.pricePolicy = resp;
this.emitDropzoneFiles();
}, err => {
this.loading = false;
});
}

onSubmit({value, valid}: { value: PricePolicyModel, valid: boolean }) {
if(valid){
if (!this.isEdit) {
this. pricePolicyService.add(value).subscribe((resp: any) => {
console.log(resp);
this.goBack();
}, err => {
console.log(err);
this.myNotifyService.notifyFail(err.error.error);
})
} else {
this.pricePolicyService.update(this.pricePolicy.id, value).subscribe((resp: any) => {
console.log(resp);
this.myNotifyService.notifySuccess('The pricePolicy is successfully updated.');
this.goBack();
}, err => {
console.log(err);
this.myNotifyService.notifyFail(err.error.error);
})
}
}else {
console.log(this.myForm);
ValidationPattern.validateAllFormFields(this.myForm);
setTimeout(t => {
                $('html, body').animate({
                    scrollTop: $('.text-danger:visible:first').offset().top - 100
                }, 1000);
            });
this.myNotifyService.notifyFail('Some fields are invalid, please check.');
}
}

// =====================================================================
// =======================Multi Select Event============================














// =======================Multi Select Event============================
// =====================================================================

// =====================================================================
// =============================Dropzone================================
emitDropzoneFiles(){


}


// ============================Dropzone=================================
// =====================================================================

// ===========================================================================
// =============================daymically add================================

// ============================daymically add=================================
// ===========================================================================



}
