import {Ingredient} from '../../shared/ingredient.model';
import {PricePolicyActions} from './actions/actions';
import {PricePolicyActionConstants} from './actions/action.constant';
import {PricePolicyInitialStateConstant} from './initial-state.constant';

export function pricePolicysReducer(state = PricePolicyInitialStateConstant, action: PricePolicyActions) {
    switch (action.type) {
        case (PricePolicyActionConstants.SET_PricePolicyS):
            console.log('PricePolicyActions.SET_PricePolicyS');
            return {
                ...state,
                pricePolicys: [...action.payload]
            };
        // case (PricePolicyActionConstants.ADD_PricePolicy):
        //     return {
        //         ...state,
        //         pricePolicys: [...state.pricePolicys, action.payload]
        //     };
        case (PricePolicyActionConstants.UPDATE_PricePolicy):
            const pricePolicy= state.pricePolicys[action.payload.index];
            const updatedPricePolicy= {
                ...pricePolicy,
                ...action.payload.updatedPricePolicy
            };
            const pricePolicys = [...state.pricePolicys];
            pricePolicys[action.payload.index] = updatedPricePolicy;
            return {
                ...state,
                pricePolicys: pricePolicys
            };
        case (PricePolicyActionConstants.DELETE_PricePolicy):
            const oldPricePolicys = [...state.pricePolicys];
            oldPricePolicys.splice(action.payload, 1);
            return {
                ...state,
                pricePolicys: oldPricePolicys
            };
        default:
            return state;
    }
}
