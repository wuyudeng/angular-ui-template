import {Action} from '@ngrx/store';
import {PricePolicyActionConstants} from './action.constant';

export class FetchPricePolicyAction implements Action {
    // readonly type = PricePolicyActionConstants.FETCH_ADMINUSERS;
    readonly type = PricePolicyActionConstants.FETCH_PricePolicyS;

    constructor(public payload: { }) {
    }
}
