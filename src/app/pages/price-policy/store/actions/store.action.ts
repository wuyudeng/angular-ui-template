import {Action} from '@ngrx/store';
import {PricePolicyActionConstants} from './price-policy-action.constants';

export class StorePricePolicyAction implements Action {
    readonly type = PricePolicyActionConstants.STORE_PricePolicyS;
    public payload: any;
}
