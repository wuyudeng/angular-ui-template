import {AddPricePolicyAction} from './add.action';
import {DeletePricePolicyAction} from './delete.action';
import {FetchPricePolicyAction} from './fetch.action';
import {SetPricePolicyAction} from './set.action';
import {UpdatePricePolicyAction} from './update.action';
import {StorePricePolicyAction} from './store.action';

export type PricePolicyActions = SetPricePolicyAction |
    AddPricePolicyAction |
    UpdatePricePolicyAction |
    DeletePricePolicyAction |
    StorePricePolicyAction |
    FetchPricePolicyAction;
