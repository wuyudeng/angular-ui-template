import {Action} from '@ngrx/store';
import {PricePolicyActionConstants} from './admin-user-action.constants';
import {PricePolicyModel} from '../../price-policy.model';

export class SetPricePolicyAction implements Action {
    readonly type = PricePolicyActionConstants.SET_PricePolicyS;

    constructor(public payload: PricePolicyModel[]) {
    }
}
