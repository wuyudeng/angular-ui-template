import {Action} from '@ngrx/store';
import {PricePolicyActionConstants} from './action.constants';
import {PricePolicyModel} from '../../price-policy.model';

export class UpdatePricePolicyAction implements Action {
    readonly type = PricePolicyActionConstants.UPDATE_PricePolicy;

    constructor(public payload: { index: number, updatedPricePolicy: PricePolicyModel }) {
    }
}
