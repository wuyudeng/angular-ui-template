import {Action} from '@ngrx/store';
import {PricePolicyActionConstants} from './price-policy-action.constant';

export class DeletePricePolicyAction implements Action {
    readonly type = PricePolicyActionConstants.DELETE_PricePolicy;

    constructor(public payload: number) {
    }
}
