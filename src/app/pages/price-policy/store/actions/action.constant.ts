export const PricePolicyActionConstants = {
    SET_PricePolicyS: 'SET_PricePolicyS',
    ADD_PricePolicy: 'ADD_PricePolicy',
    UPDATE_PricePolicy: 'UPDATE_PricePolicy',
    DELETE_PricePolicy: 'DELETE_PricePolicy',
    STORE_PricePolicyS: 'STORE_PricePolicyS',
    FETCH_PricePolicyS: 'FETCH_PricePolicyS',
}