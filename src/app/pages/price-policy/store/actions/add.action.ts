import {Action} from '@ngrx/store';
import {PricePolicyModel} from '../../price-policy.model';
import {PricePolicyActionConstants} from './price-policy-action.constant';

export class AddPricePolicyAction implements Action {
    readonly type = PricePolicyActionConstants.SET_PricePolicyS;

    constructor(public payload: PricePolicyModel[]) {
    }
}
