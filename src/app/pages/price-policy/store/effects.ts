import {Injectable} from '@angular/core';
import {Actions, Effect} from '@ngrx/effects';
import 'rxjs/add/operator/switchMap';
import 'rxjs/add/operator/withLatestFrom';
import {HttpClient} from '@angular/common/http';
import {Store} from '@ngrx/store';
import {PricePolicyActionConstants} from './actions/action.constant';
import {FetchPricePolicyAction} from './actions/fetch.action';
import {PricePolicyService} from '../price-policy.service';
import {PricePolicyFeatureStateInterface} from './interfaces/feature-state.interface';

@Injectable()
export class PricePolicyEffects {
    @Effect()
    pricePolicyFetch = this.actions$
        .ofType(PricePolicyActionConstants.FETCH_PricePolicyS)
        .switchMap((action: FetchPricePolicyAction) => {

            console.log('PricePolicyActions.FETCH_PricePolicyS');
            return this.pricePolicyService.getAll();
        })
        .map(
            (pricePolicys) => ({
                // type: PricePolicyActionConstants.SET_PricePolicyS,
                type: PricePolicyActionConstants.SET_PricePolicyS,
                payload: pricePolicys.results
            }));
    //
    // @Effect({dispatch: false})
    // pricePolicyStore = this.actions$
    //     .ofType(PricePolicyActionConstants.STORE_PricePolicyS)
    //     .withLatestFrom(this.store.select('pricePolicys'))
    //     .switchMap(([action, state]) => {
    //         const req = new HttpRequest('PUT', 'https://ng-pricePolicy-book-3adbb.firebaseio.com/pricePolicys.json', state.pricePolicys, {reportProgress: true});
    //         return this.httpClient.request(req);
    //     });

    constructor(private actions$: Actions,
                private httpClient: HttpClient,
                private pricePolicyService: PricePolicyService,
                private store: Store<PricePolicyFeatureStateInterface>) {
    }

}
