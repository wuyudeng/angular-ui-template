import {PricePolicyModel} from '../../price-policy.model';
import {AppStateInterface} from '../../../../app-store/app-state.interface';

export interface PricePolicyFeatureStateInterface extends AppStateInterface {
    pricePolicys: PricePolicyModel[];
}

