import {PricePolicyStateInterface} from './interfaces/state.interface';

export const PricePolicyInitialStateConstant: PricePolicyStateInterface = {
    pricePolicys: []
};