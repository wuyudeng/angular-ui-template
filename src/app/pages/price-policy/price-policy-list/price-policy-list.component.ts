import {Component, OnInit, ViewChild} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {HelperService} from '../../../services/helper.service';
import {ListBaseComponent} from '../../../shared-module/bases/list-base-component/list-base.component';
import {Router} from '@angular/router';
import {Subject} from 'rxjs/Subject';

import {PricePolicyModel} from '../price-policy.model';
import {PricePolicyService} from '../price-policy.service';

import {SortColumns} from './sort.columns';
import {Sorts} from '../../../models/bases/sorts.model';
import {DownloadService} from '../../../services/download.service';

@Component({
    selector: 'sa-price-policy-list',
    templateUrl: './price-policy-list.component.html',
})
export class PricePolicyListComponent extends ListBaseComponent implements OnInit {
public searchForm: FormGroup;
public searchCondition: string;
public loading: boolean;

@ViewChild('lgImportByExcelModal') lgImportByExcelModal: any;
public excelUploadUrl: string;
public uploadAndImportSubject: Subject<string> = new Subject<string>();
public resetDropzoneSubject: Subject<string> = new Subject();
public exportUrl: string;

constructor(private formBuilder: FormBuilder,
public pricePolicyService: PricePolicyService,
public router: Router,
public downloadService: DownloadService,
public helperService: HelperService) {
super(router, helperService);
this.sortOprions.sortColumns = SortColumns.Columns;
}

ngOnInit() {
this.excelUploadUrl = this.pricePolicyService.getExcelUploadUrl();
this.refresh();
this.buildSearchFrom();
this.debounceSearchForm();
}

download() {
this.downloadService.downloadFile(this.exportUrl, 'price-policy-list' + Date.now().toString() + '.xls');
}

/**
* ----- modal functions BEGIN-----
*/
onImportByExcelModalHide() {
    this.lgImportByExcelModal.hide()
}

onFileUploaded(status) {
    this.refresh();
    this.lgImportByExcelModal.hide()
}

importByExceliConfirm() {
    this.uploadAndImportSubject.next('true');
}

importByExcel() {
    this.lgImportByExcelModal.show();
}

downloadExcelTemplate() {
this.downloadService.downloadFile(this.pricePolicyService.getDownloadTemplateUrl(), 'price-policy-list-template' + Date.now().toString() + '.xls');
}
/**
* ----- modal functions END-----
*/

refresh() {
this.loading = true;
const searchStr = this.helperService.getSearchConditionByRouter(this.router);
this.exportUrl = this.pricePolicyService.getUrl() + '/excel?' + this.pricePolicyService.getSearchUrl(searchStr, this.paging, this.sortOprions);
this.exportUrl = this.exportUrl.replace('?&', '?');
this.pricePolicyService.getAllByPaging(searchStr, this.paging, this.sortOprions).subscribe((resp: any) => {
console.log(resp);
this.listElements = resp.content;
this.paging.totalSize = resp.totalElements;
this.loading = false;
}, err => {
this.loading = false;
});
}

/**
* ----- search form -----
*/
buildSearchFrom() {
this.searchForm = this.formBuilder.group({
        buyCurrency: ['', [Validators.required]],
        sellCurrency: ['', [Validators.required]],
        sysBuyMargin: ['', [Validators.required]],
        sysSellMargin: ['', [Validators.required]],
        bankBuyMargin: ['', [Validators.required]],
        bankSellMargin: ['', [Validators.required]],
        priceCategory: ['', [Validators.required]],
        baseBuyPrice: ['', [Validators.required]],
        baseSellPrice: ['', [Validators.required]],
        sysBuyPrice: ['', [Validators.required]],
        sysSellPrice: ['', [Validators.required]],
        bankBuyPrice: ['', [Validators.required]],
        bankSellPrice: ['', [Validators.required]],
});
}
}