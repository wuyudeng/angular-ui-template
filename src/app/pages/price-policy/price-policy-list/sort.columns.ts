import {Sort} from '../../../models/bases/sort.model';

export class SortColumns {
    public static Columns = [
        new Sort({
            isAsc: false,
            columnDisplay: 'Buy Currency',
            columnModel: 'buyCurrency',
            isSortable: true,
            isActive: false
        }),
        new Sort({
            isAsc: false,
            columnDisplay: 'Sell Currency',
            columnModel: 'sellCurrency',
            isSortable: true,
            isActive: false
        }),
        new Sort({
            isAsc: false,
            columnDisplay: 'Sys Buy Margin',
            columnModel: 'sysBuyMargin',
            isSortable: true,
            isActive: false
        }),
        new Sort({
            isAsc: false,
            columnDisplay: 'Sys Sell Margin',
            columnModel: 'sysSellMargin',
            isSortable: true,
            isActive: false
        }),
        new Sort({
            isAsc: false,
            columnDisplay: 'Bank Buy Margin',
            columnModel: 'bankBuyMargin',
            isSortable: true,
            isActive: false
        }),
        new Sort({
            isAsc: false,
            columnDisplay: 'Bank Sell Margin',
            columnModel: 'bankSellMargin',
            isSortable: true,
            isActive: false
        }),
        new Sort({
            isAsc: false,
            columnDisplay: 'Price Category',
            columnModel: 'priceCategory',
            isSortable: true,
            isActive: false
        }),
        new Sort({
            isAsc: false,
            columnDisplay: 'Base Buy Price',
            columnModel: 'baseBuyPrice',
            isSortable: true,
            isActive: false
        }),
        new Sort({
            isAsc: false,
            columnDisplay: 'Base Sell Price',
            columnModel: 'baseSellPrice',
            isSortable: true,
            isActive: false
        }),
        new Sort({
            isAsc: false,
            columnDisplay: 'Sys Buy Price',
            columnModel: 'sysBuyPrice',
            isSortable: true,
            isActive: false
        }),
        new Sort({
            isAsc: false,
            columnDisplay: 'Sys Sell Price',
            columnModel: 'sysSellPrice',
            isSortable: true,
            isActive: false
        }),
        new Sort({
            isAsc: false,
            columnDisplay: 'Bank Buy Price',
            columnModel: 'bankBuyPrice',
            isSortable: true,
            isActive: false
        }),
        new Sort({
            isAsc: false,
            columnDisplay: 'Bank Sell Price',
            columnModel: 'bankSellPrice',
            isSortable: true,
            isActive: false
        }),
    ];
}
