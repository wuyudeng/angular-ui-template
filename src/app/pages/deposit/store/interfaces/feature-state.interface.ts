import {DepositModel} from '../../deposit.model';
import {AppStateInterface} from '../../../../app-store/app-state.interface';

export interface DepositFeatureStateInterface extends AppStateInterface {
    deposits: DepositModel[];
}

