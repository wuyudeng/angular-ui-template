import {DepositModel} from '../../deposit.model';

export interface DepositStateInterface {
    deposits: DepositModel[];

}