import {Injectable} from '@angular/core';
import {Actions, Effect} from '@ngrx/effects';
import 'rxjs/add/operator/switchMap';
import 'rxjs/add/operator/withLatestFrom';
import {HttpClient} from '@angular/common/http';
import {Store} from '@ngrx/store';
import {DepositActionConstants} from './actions/action.constant';
import {FetchDepositAction} from './actions/fetch.action';
import {DepositService} from '../deposit.service';
import {DepositFeatureStateInterface} from './interfaces/feature-state.interface';

@Injectable()
export class DepositEffects {
    @Effect()
    depositFetch = this.actions$
        .ofType(DepositActionConstants.FETCH_DepositS)
        .switchMap((action: FetchDepositAction) => {

            console.log('DepositActions.FETCH_DepositS');
            return this.depositService.getAll();
        })
        .map(
            (deposits) => ({
                // type: DepositActionConstants.SET_DepositS,
                type: DepositActionConstants.SET_DepositS,
                payload: deposits.results
            }));
    //
    // @Effect({dispatch: false})
    // depositStore = this.actions$
    //     .ofType(DepositActionConstants.STORE_DepositS)
    //     .withLatestFrom(this.store.select('deposits'))
    //     .switchMap(([action, state]) => {
    //         const req = new HttpRequest('PUT', 'https://ng-deposit-book-3adbb.firebaseio.com/deposits.json', state.deposits, {reportProgress: true});
    //         return this.httpClient.request(req);
    //     });

    constructor(private actions$: Actions,
                private httpClient: HttpClient,
                private depositService: DepositService,
                private store: Store<DepositFeatureStateInterface>) {
    }

}
