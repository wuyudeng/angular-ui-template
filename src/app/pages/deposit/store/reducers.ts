import {Ingredient} from '../../shared/ingredient.model';
import {DepositActions} from './actions/actions';
import {DepositActionConstants} from './actions/action.constant';
import {DepositInitialStateConstant} from './initial-state.constant';

export function depositsReducer(state = DepositInitialStateConstant, action: DepositActions) {
    switch (action.type) {
        case (DepositActionConstants.SET_DepositS):
            console.log('DepositActions.SET_DepositS');
            return {
                ...state,
                deposits: [...action.payload]
            };
        // case (DepositActionConstants.ADD_Deposit):
        //     return {
        //         ...state,
        //         deposits: [...state.deposits, action.payload]
        //     };
        case (DepositActionConstants.UPDATE_Deposit):
            const deposit= state.deposits[action.payload.index];
            const updatedDeposit= {
                ...deposit,
                ...action.payload.updatedDeposit
            };
            const deposits = [...state.deposits];
            deposits[action.payload.index] = updatedDeposit;
            return {
                ...state,
                deposits: deposits
            };
        case (DepositActionConstants.DELETE_Deposit):
            const oldDeposits = [...state.deposits];
            oldDeposits.splice(action.payload, 1);
            return {
                ...state,
                deposits: oldDeposits
            };
        default:
            return state;
    }
}
