import {Action} from '@ngrx/store';
import {DepositActionConstants} from './deposit-action.constants';

export class StoreDepositAction implements Action {
    readonly type = DepositActionConstants.STORE_DepositS;
    public payload: any;
}
