import {Action} from '@ngrx/store';
import {DepositActionConstants} from './action.constants';
import {DepositModel} from '../../deposit.model';

export class UpdateDepositAction implements Action {
    readonly type = DepositActionConstants.UPDATE_Deposit;

    constructor(public payload: { index: number, updatedDeposit: DepositModel }) {
    }
}
