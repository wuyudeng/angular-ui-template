import {Action} from '@ngrx/store';
import {DepositActionConstants} from './action.constant';

export class FetchDepositAction implements Action {
    // readonly type = DepositActionConstants.FETCH_ADMINUSERS;
    readonly type = DepositActionConstants.FETCH_DepositS;

    constructor(public payload: { }) {
    }
}
