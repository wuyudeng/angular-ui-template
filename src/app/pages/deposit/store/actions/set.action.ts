import {Action} from '@ngrx/store';
import {DepositActionConstants} from './admin-user-action.constants';
import {DepositModel} from '../../deposit.model';

export class SetDepositAction implements Action {
    readonly type = DepositActionConstants.SET_DepositS;

    constructor(public payload: DepositModel[]) {
    }
}
