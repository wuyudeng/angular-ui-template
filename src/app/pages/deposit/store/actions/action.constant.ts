export const DepositActionConstants = {
    SET_DepositS: 'SET_DepositS',
    ADD_Deposit: 'ADD_Deposit',
    UPDATE_Deposit: 'UPDATE_Deposit',
    DELETE_Deposit: 'DELETE_Deposit',
    STORE_DepositS: 'STORE_DepositS',
    FETCH_DepositS: 'FETCH_DepositS',
}