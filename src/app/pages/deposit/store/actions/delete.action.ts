import {Action} from '@ngrx/store';
import {DepositActionConstants} from './deposit-action.constant';

export class DeleteDepositAction implements Action {
    readonly type = DepositActionConstants.DELETE_Deposit;

    constructor(public payload: number) {
    }
}
