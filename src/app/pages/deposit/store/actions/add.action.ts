import {Action} from '@ngrx/store';
import {DepositModel} from '../../deposit.model';
import {DepositActionConstants} from './deposit-action.constant';

export class AddDepositAction implements Action {
    readonly type = DepositActionConstants.SET_DepositS;

    constructor(public payload: DepositModel[]) {
    }
}
