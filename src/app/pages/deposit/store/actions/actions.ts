import {AddDepositAction} from './add.action';
import {DeleteDepositAction} from './delete.action';
import {FetchDepositAction} from './fetch.action';
import {SetDepositAction} from './set.action';
import {UpdateDepositAction} from './update.action';
import {StoreDepositAction} from './store.action';

export type DepositActions = SetDepositAction |
    AddDepositAction |
    UpdateDepositAction |
    DeleteDepositAction |
    StoreDepositAction |
    FetchDepositAction;
