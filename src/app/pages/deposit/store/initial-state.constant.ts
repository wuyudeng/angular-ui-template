import {DepositStateInterface} from './interfaces/state.interface';

export const DepositInitialStateConstant: DepositStateInterface = {
    deposits: []
};