import {Injectable, OnInit} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {BaseService} from '../../shared-module/bases/base.service';

import {Store} from '@ngrx/store';
import {DepositFeatureStateInterface} from './store/interfaces/feature-state.interface';
import {FetchDepositAction} from './store/actions/fetch.action';

@Injectable()
export class DepositService extends BaseService implements OnInit {

    constructor(public http: HttpClient,
public store: Store<DepositFeatureStateInterface>) {
        super('deposit', http);
























        this.embeddedStr = '&embedded=collinson-user,';
    }

    ngOnInit(): void {
    }

fetchAll() {
        this.store.dispatch(new FetchDepositAction({}));
    }

    getAllFromStore() {
        return this.store.select(o => o.depositList);
    }
}