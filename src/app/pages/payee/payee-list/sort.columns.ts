import {Sort} from '../../../models/bases/sort.model';

export class SortColumns {
    public static Columns = [
        new Sort({
            isAsc: false,
            columnDisplay: 'Name',
            columnModel: 'name',
            isSortable: true,
            isActive: false
        }),
        new Sort({
            isAsc: false,
            columnDisplay: 'Country',
            columnModel: 'country',
            isSortable: true,
            isActive: false
        }),
        new Sort({
            isAsc: false,
            columnDisplay: 'Address Line1',
            columnModel: 'addressLine1',
            isSortable: true,
            isActive: false
        }),
        new Sort({
            isAsc: false,
            columnDisplay: 'Address Line2',
            columnModel: 'addressLine2',
            isSortable: true,
            isActive: false
        }),
        new Sort({
            isAsc: false,
            columnDisplay: 'City',
            columnModel: 'city',
            isSortable: true,
            isActive: false
        }),
        new Sort({
            isAsc: false,
            columnDisplay: 'Post Code',
            columnModel: 'postCode',
            isSortable: true,
            isActive: false
        }),
        new Sort({
            isAsc: false,
            columnDisplay: 'Enable',
            columnModel: 'enable',
            isSortable: true,
            isActive: false
        }),
        new Sort({
            isAsc: false,
            columnDisplay: 'Payer',
            columnModel: 'payer',
            isSortable: true,
            isActive: false
        }),
        new Sort({
            isAsc: false,
            columnDisplay: 'Pay Amount',
            columnModel: 'payAmount',
            isSortable: true,
            isActive: false
        }),
        new Sort({
            isAsc: false,
            columnDisplay: 'Collinson User',
            columnModel: 'collinsonUser',
            isSortable: true,
            isActive: false
        }),
        new Sort({
            isAsc: false,
            columnDisplay: 'Verify',
            columnModel: 'verify',
            isSortable: true,
            isActive: false
        }),
    ];
}
