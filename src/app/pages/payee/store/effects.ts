import {Injectable} from '@angular/core';
import {Actions, Effect} from '@ngrx/effects';
import 'rxjs/add/operator/switchMap';
import 'rxjs/add/operator/withLatestFrom';
import {HttpClient} from '@angular/common/http';
import {Store} from '@ngrx/store';
import {PayeeActionConstants} from './actions/action.constant';
import {FetchPayeeAction} from './actions/fetch.action';
import {PayeeService} from '../payee.service';
import {PayeeFeatureStateInterface} from './interfaces/feature-state.interface';

@Injectable()
export class PayeeEffects {
    @Effect()
    payeeFetch = this.actions$
        .ofType(PayeeActionConstants.FETCH_PayeeS)
        .switchMap((action: FetchPayeeAction) => {

            console.log('PayeeActions.FETCH_PayeeS');
            return this.payeeService.getAll();
        })
        .map(
            (payees) => ({
                // type: PayeeActionConstants.SET_PayeeS,
                type: PayeeActionConstants.SET_PayeeS,
                payload: payees.results
            }));
    //
    // @Effect({dispatch: false})
    // payeeStore = this.actions$
    //     .ofType(PayeeActionConstants.STORE_PayeeS)
    //     .withLatestFrom(this.store.select('payees'))
    //     .switchMap(([action, state]) => {
    //         const req = new HttpRequest('PUT', 'https://ng-payee-book-3adbb.firebaseio.com/payees.json', state.payees, {reportProgress: true});
    //         return this.httpClient.request(req);
    //     });

    constructor(private actions$: Actions,
                private httpClient: HttpClient,
                private payeeService: PayeeService,
                private store: Store<PayeeFeatureStateInterface>) {
    }

}
