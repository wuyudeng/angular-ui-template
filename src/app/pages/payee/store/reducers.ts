import {Ingredient} from '../../shared/ingredient.model';
import {PayeeActions} from './actions/actions';
import {PayeeActionConstants} from './actions/action.constant';
import {PayeeInitialStateConstant} from './initial-state.constant';

export function payeesReducer(state = PayeeInitialStateConstant, action: PayeeActions) {
    switch (action.type) {
        case (PayeeActionConstants.SET_PayeeS):
            console.log('PayeeActions.SET_PayeeS');
            return {
                ...state,
                payees: [...action.payload]
            };
        // case (PayeeActionConstants.ADD_Payee):
        //     return {
        //         ...state,
        //         payees: [...state.payees, action.payload]
        //     };
        case (PayeeActionConstants.UPDATE_Payee):
            const payee= state.payees[action.payload.index];
            const updatedPayee= {
                ...payee,
                ...action.payload.updatedPayee
            };
            const payees = [...state.payees];
            payees[action.payload.index] = updatedPayee;
            return {
                ...state,
                payees: payees
            };
        case (PayeeActionConstants.DELETE_Payee):
            const oldPayees = [...state.payees];
            oldPayees.splice(action.payload, 1);
            return {
                ...state,
                payees: oldPayees
            };
        default:
            return state;
    }
}
