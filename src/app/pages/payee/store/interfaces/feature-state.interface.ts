import {PayeeModel} from '../../payee.model';
import {AppStateInterface} from '../../../../app-store/app-state.interface';

export interface PayeeFeatureStateInterface extends AppStateInterface {
    payees: PayeeModel[];
}

