import {PayeeStateInterface} from './interfaces/state.interface';

export const PayeeInitialStateConstant: PayeeStateInterface = {
    payees: []
};