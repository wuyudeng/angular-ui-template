import {Action} from '@ngrx/store';
import {PayeeActionConstants} from './action.constant';

export class FetchPayeeAction implements Action {
    // readonly type = PayeeActionConstants.FETCH_ADMINUSERS;
    readonly type = PayeeActionConstants.FETCH_PayeeS;

    constructor(public payload: { }) {
    }
}
