export const PayeeActionConstants = {
    SET_PayeeS: 'SET_PayeeS',
    ADD_Payee: 'ADD_Payee',
    UPDATE_Payee: 'UPDATE_Payee',
    DELETE_Payee: 'DELETE_Payee',
    STORE_PayeeS: 'STORE_PayeeS',
    FETCH_PayeeS: 'FETCH_PayeeS',
}