import {Action} from '@ngrx/store';
import {PayeeActionConstants} from './payee-action.constants';

export class StorePayeeAction implements Action {
    readonly type = PayeeActionConstants.STORE_PayeeS;
    public payload: any;
}
