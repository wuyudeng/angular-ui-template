import {Action} from '@ngrx/store';
import {PayeeActionConstants} from './admin-user-action.constants';
import {PayeeModel} from '../../payee.model';

export class SetPayeeAction implements Action {
    readonly type = PayeeActionConstants.SET_PayeeS;

    constructor(public payload: PayeeModel[]) {
    }
}
