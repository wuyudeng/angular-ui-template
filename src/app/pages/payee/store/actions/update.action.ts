import {Action} from '@ngrx/store';
import {PayeeActionConstants} from './action.constants';
import {PayeeModel} from '../../payee.model';

export class UpdatePayeeAction implements Action {
    readonly type = PayeeActionConstants.UPDATE_Payee;

    constructor(public payload: { index: number, updatedPayee: PayeeModel }) {
    }
}
