import {Action} from '@ngrx/store';
import {PayeeModel} from '../../payee.model';
import {PayeeActionConstants} from './payee-action.constant';

export class AddPayeeAction implements Action {
    readonly type = PayeeActionConstants.SET_PayeeS;

    constructor(public payload: PayeeModel[]) {
    }
}
