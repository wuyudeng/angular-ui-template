import {Action} from '@ngrx/store';
import {PayeeActionConstants} from './payee-action.constant';

export class DeletePayeeAction implements Action {
    readonly type = PayeeActionConstants.DELETE_Payee;

    constructor(public payload: number) {
    }
}
