import {AddPayeeAction} from './add.action';
import {DeletePayeeAction} from './delete.action';
import {FetchPayeeAction} from './fetch.action';
import {SetPayeeAction} from './set.action';
import {UpdatePayeeAction} from './update.action';
import {StorePayeeAction} from './store.action';

export type PayeeActions = SetPayeeAction |
    AddPayeeAction |
    UpdatePayeeAction |
    DeletePayeeAction |
    StorePayeeAction |
    FetchPayeeAction;
