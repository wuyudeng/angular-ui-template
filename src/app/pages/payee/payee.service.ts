import {Injectable, OnInit} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {BaseService} from '../../shared-module/bases/base.service';

import {Store} from '@ngrx/store';
import {PayeeFeatureStateInterface} from './store/interfaces/feature-state.interface';
import {FetchPayeeAction} from './store/actions/fetch.action';

@Injectable()
export class PayeeService extends BaseService implements OnInit {

    constructor(public http: HttpClient,
public store: Store<PayeeFeatureStateInterface>) {
        super('payee', http);













































        this.embeddedStr = '&embedded=attachments,payer,payee-bank-accounts,transactions,collinson-user,';
    }

    ngOnInit(): void {
    }

fetchAll() {
        this.store.dispatch(new FetchPayeeAction({}));
    }

    getAllFromStore() {
        return this.store.select(o => o.payeeList);
    }
}