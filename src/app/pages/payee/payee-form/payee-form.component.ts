import {ChangeDetectorRef, Component, OnInit} from '@angular/core';
import {FormArray, FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {Subscription} from 'rxjs/Subscription';
import {ActivatedRoute, Router} from '@angular/router';
import {Location} from '@angular/common';
import {AutoUnsubscribe} from 'ngx-auto-unsubscribe';
import {Subject} from 'rxjs/Subject';

import {FormBaseComponent} from '../../../shared-module/bases/form-base-component/form-base.component';
import {MyNotifyService} from '../../../services/my-notify.service';
import {ImageModel} from '../../../models/bases/image.model';

import {PayeeModel} from '../payee.model';
import {PayeeService} from '../payee.service';
import {ValidationPattern} from '../../../shared-module/bases/validation-error/validation.pattern';
declare var $: any;

    import {PayerService} from '../../payer/payer.service';
    import {CollinsonUserService} from '../../collinson-user/collinson-user.service';

@Component({
selector: 'sa-payee-form',
templateUrl: './payee-form.component.html',
})
@AutoUnsubscribe()
export class PayeeFormComponent extends FormBaseComponent implements OnInit {
public loading: boolean;
public myForm: FormGroup;
public id: number;
public isEdit = false;
public subscription: Subscription;
public payee: PayeeModel = new PayeeModel();
public currentDate = new Date();
// =====================================================================
// =============================Dropzone Variable=======================
        public attachmentsImageSubject: Subject<ImageModel[] | any> = new Subject<ImageModel[] | any>();
// ============================Dropzone Variable========================
// =====================================================================

// =================================================================================
// =============================multi select subject Variable=======================
        public payerRefreshSubject = new Subject<any>();
        public payerForLoadSubject = new Subject<any>();
        public collinsonUserRefreshSubject = new Subject<any>();
        public collinsonUserForLoadSubject = new Subject<any>();
// ============================multi select subject Variable========================
// =================================================================================


// =================================================================================
// ===================================EnumList Variable=============================
// ===================================EnumList Variable=============================
// =================================================================================


constructor(public formBuiler: FormBuilder,
public ref: ChangeDetectorRef,
public router: Router,
public location: Location,
public myNotifyService: MyNotifyService,
public payeeService: PayeeService,
        public payerService: PayerService,
        public collinsonUserService: CollinsonUserService,
public activatedRoute: ActivatedRoute) {
super(activatedRoute, location);
}

ngOnInit() {
this.getEnumList();
this.getRouteParemeter();
this.getQueryParams();
//this.initFormControl();
}

initFormControl() {
this.myForm = this.formBuiler.group({
        id: new FormControl('', {
        
        
            }),
        name: new FormControl('', {
        validators:[Validators.required,],
        
            }),
        country: new FormControl('', {
        validators:[Validators.required,],
        
            }),
        addressLine1: new FormControl('', {
        validators:[Validators.required,],
        
            }),
        addressLine2: new FormControl('', {
        validators:[Validators.required,],
        
            }),
        city: new FormControl('', {
        validators:[Validators.required,],
        
            }),
        postCode: new FormControl('', {
        
        
            }),
        attachments: new FormControl('', {
        
        
            }),
        enable: new FormControl('', {
        validators:[Validators.required,],
        
            }),
        payer: new FormControl('', {
        
        
            }),
        payeeBankAccounts: new FormControl('', {
        
        
            }),
        payAmount: new FormControl('', {
        
        
            }),
        transactions: new FormControl('', {
        
        
            }),
        collinsonUser: new FormControl('', {
        
        
            }),
        verify: new FormControl('', {
        
        
            }),
});
}

getEnumList(){
    }

getItem() {
this.loading = true;
this.payeeService.get(this.id).subscribe((resp: any) => {
this.loading = false;
console.log(resp);
this.payee = resp;
this.emitDropzoneFiles();
}, err => {
this.loading = false;
});
}

onSubmit({value, valid}: { value: PayeeModel, valid: boolean }) {
if(valid){
if (!this.isEdit) {
this. payeeService.add(value).subscribe((resp: any) => {
console.log(resp);
this.goBack();
}, err => {
console.log(err);
this.myNotifyService.notifyFail(err.error.error);
})
} else {
this.payeeService.update(this.payee.id, value).subscribe((resp: any) => {
console.log(resp);
this.myNotifyService.notifySuccess('The payee is successfully updated.');
this.goBack();
}, err => {
console.log(err);
this.myNotifyService.notifyFail(err.error.error);
})
}
}else {
console.log(this.myForm);
ValidationPattern.validateAllFormFields(this.myForm);
setTimeout(t => {
                $('html, body').animate({
                    scrollTop: $('.text-danger:visible:first').offset().top - 100
                }, 1000);
            });
this.myNotifyService.notifyFail('Some fields are invalid, please check.');
}
}

// =====================================================================
// =======================Multi Select Event============================









        payerSelected($event) {
        this.payee.payer = $event;
    }




        collinsonUserSelected($event) {
        this.payee.collinsonUser = $event;
    }


// =======================Multi Select Event============================
// =====================================================================

// =====================================================================
// =============================Dropzone================================
emitDropzoneFiles(){

        this.attachmentsImageSubject.next(this.payee.attachments);

}


        attachmentsFileObjectsChanged($event) {
        this.payee.attachments = $event;
    }
// ============================Dropzone=================================
// =====================================================================

// ===========================================================================
// =============================daymically add================================

// ============================daymically add=================================
// ===========================================================================



}
