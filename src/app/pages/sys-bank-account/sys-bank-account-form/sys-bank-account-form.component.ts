import {ChangeDetectorRef, Component, OnInit} from '@angular/core';
import {FormArray, FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {Subscription} from 'rxjs/Subscription';
import {ActivatedRoute, Router} from '@angular/router';
import {Location} from '@angular/common';
import {AutoUnsubscribe} from 'ngx-auto-unsubscribe';
import {Subject} from 'rxjs/Subject';

import {FormBaseComponent} from '../../../shared-module/bases/form-base-component/form-base.component';
import {MyNotifyService} from '../../../services/my-notify.service';
import {ImageModel} from '../../../models/bases/image.model';

import {SysBankAccountModel} from '../sys-bank-account.model';
import {SysBankAccountService} from '../sys-bank-account.service';
import {ValidationPattern} from '../../../shared-module/bases/validation-error/validation.pattern';
declare var $: any;


@Component({
selector: 'sa-sys-bank-account-form',
templateUrl: './sys-bank-account-form.component.html',
})
@AutoUnsubscribe()
export class SysBankAccountFormComponent extends FormBaseComponent implements OnInit {
public loading: boolean;
public myForm: FormGroup;
public id: number;
public isEdit = false;
public subscription: Subscription;
public sysBankAccount: SysBankAccountModel = new SysBankAccountModel();
public currentDate = new Date();
// =====================================================================
// =============================Dropzone Variable=======================
// ============================Dropzone Variable========================
// =====================================================================

// =================================================================================
// =============================multi select subject Variable=======================
// ============================multi select subject Variable========================
// =================================================================================


// =================================================================================
// ===================================EnumList Variable=============================
// ===================================EnumList Variable=============================
// =================================================================================


constructor(public formBuiler: FormBuilder,
public ref: ChangeDetectorRef,
public router: Router,
public location: Location,
public myNotifyService: MyNotifyService,
public sysBankAccountService: SysBankAccountService,
public activatedRoute: ActivatedRoute) {
super(activatedRoute, location);
}

ngOnInit() {
this.getEnumList();
this.getRouteParemeter();
this.getQueryParams();
//this.initFormControl();
}

initFormControl() {
this.myForm = this.formBuiler.group({
        id: new FormControl('', {
        
        
            }),
        currency: new FormControl('', {
        
        
            }),
        accountName: new FormControl('', {
        
        
            }),
        accountNumber: new FormControl('', {
        
        
            }),
        bankName: new FormControl('', {
        
        
            }),
        bankAddress: new FormControl('', {
        
        
            }),
        bankCity: new FormControl('', {
        
        
            }),
        bankCountry: new FormControl('', {
        
        
            }),
        bsbNumber: new FormControl('', {
        
        
            }),
        bicCode: new FormControl('', {
        
        
            }),
        correspondentBank: new FormControl('', {
        
        
            }),
});
}

getEnumList(){
    }

getItem() {
this.loading = true;
this.sysBankAccountService.get(this.id).subscribe((resp: any) => {
this.loading = false;
console.log(resp);
this.sysBankAccount = resp;
this.emitDropzoneFiles();
}, err => {
this.loading = false;
});
}

onSubmit({value, valid}: { value: SysBankAccountModel, valid: boolean }) {
if(valid){
if (!this.isEdit) {
this. sysBankAccountService.add(value).subscribe((resp: any) => {
console.log(resp);
this.goBack();
}, err => {
console.log(err);
this.myNotifyService.notifyFail(err.error.error);
})
} else {
this.sysBankAccountService.update(this.sysBankAccount.id, value).subscribe((resp: any) => {
console.log(resp);
this.myNotifyService.notifySuccess('The sysBankAccount is successfully updated.');
this.goBack();
}, err => {
console.log(err);
this.myNotifyService.notifyFail(err.error.error);
})
}
}else {
console.log(this.myForm);
ValidationPattern.validateAllFormFields(this.myForm);
setTimeout(t => {
                $('html, body').animate({
                    scrollTop: $('.text-danger:visible:first').offset().top - 100
                }, 1000);
            });
this.myNotifyService.notifyFail('Some fields are invalid, please check.');
}
}

// =====================================================================
// =======================Multi Select Event============================











// =======================Multi Select Event============================
// =====================================================================

// =====================================================================
// =============================Dropzone================================
emitDropzoneFiles(){


}


// ============================Dropzone=================================
// =====================================================================

// ===========================================================================
// =============================daymically add================================

// ============================daymically add=================================
// ===========================================================================



}
