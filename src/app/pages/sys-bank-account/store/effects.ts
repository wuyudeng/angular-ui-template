import {Injectable} from '@angular/core';
import {Actions, Effect} from '@ngrx/effects';
import 'rxjs/add/operator/switchMap';
import 'rxjs/add/operator/withLatestFrom';
import {HttpClient} from '@angular/common/http';
import {Store} from '@ngrx/store';
import {SysBankAccountActionConstants} from './actions/action.constant';
import {FetchSysBankAccountAction} from './actions/fetch.action';
import {SysBankAccountService} from '../sys-bank-account.service';
import {SysBankAccountFeatureStateInterface} from './interfaces/feature-state.interface';

@Injectable()
export class SysBankAccountEffects {
    @Effect()
    sysBankAccountFetch = this.actions$
        .ofType(SysBankAccountActionConstants.FETCH_SysBankAccountS)
        .switchMap((action: FetchSysBankAccountAction) => {

            console.log('SysBankAccountActions.FETCH_SysBankAccountS');
            return this.sysBankAccountService.getAll();
        })
        .map(
            (sysBankAccounts) => ({
                // type: SysBankAccountActionConstants.SET_SysBankAccountS,
                type: SysBankAccountActionConstants.SET_SysBankAccountS,
                payload: sysBankAccounts.results
            }));
    //
    // @Effect({dispatch: false})
    // sysBankAccountStore = this.actions$
    //     .ofType(SysBankAccountActionConstants.STORE_SysBankAccountS)
    //     .withLatestFrom(this.store.select('sysBankAccounts'))
    //     .switchMap(([action, state]) => {
    //         const req = new HttpRequest('PUT', 'https://ng-sysBankAccount-book-3adbb.firebaseio.com/sysBankAccounts.json', state.sysBankAccounts, {reportProgress: true});
    //         return this.httpClient.request(req);
    //     });

    constructor(private actions$: Actions,
                private httpClient: HttpClient,
                private sysBankAccountService: SysBankAccountService,
                private store: Store<SysBankAccountFeatureStateInterface>) {
    }

}
