import {SysBankAccountModel} from '../../sys-bank-account.model';
import {AppStateInterface} from '../../../../app-store/app-state.interface';

export interface SysBankAccountFeatureStateInterface extends AppStateInterface {
    sysBankAccounts: SysBankAccountModel[];
}

