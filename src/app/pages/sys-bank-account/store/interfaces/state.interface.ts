import {SysBankAccountModel} from '../../sys-bank-account.model';

export interface SysBankAccountStateInterface {
    sysBankAccounts: SysBankAccountModel[];

}