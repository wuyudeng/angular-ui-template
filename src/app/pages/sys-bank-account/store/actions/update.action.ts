import {Action} from '@ngrx/store';
import {SysBankAccountActionConstants} from './action.constants';
import {SysBankAccountModel} from '../../sys-bank-account.model';

export class UpdateSysBankAccountAction implements Action {
    readonly type = SysBankAccountActionConstants.UPDATE_SysBankAccount;

    constructor(public payload: { index: number, updatedSysBankAccount: SysBankAccountModel }) {
    }
}
