import {Action} from '@ngrx/store';
import {SysBankAccountModel} from '../../sys-bank-account.model';
import {SysBankAccountActionConstants} from './sys-bank-account-action.constant';

export class AddSysBankAccountAction implements Action {
    readonly type = SysBankAccountActionConstants.SET_SysBankAccountS;

    constructor(public payload: SysBankAccountModel[]) {
    }
}
