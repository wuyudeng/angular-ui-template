import {Action} from '@ngrx/store';
import {SysBankAccountActionConstants} from './admin-user-action.constants';
import {SysBankAccountModel} from '../../sys-bank-account.model';

export class SetSysBankAccountAction implements Action {
    readonly type = SysBankAccountActionConstants.SET_SysBankAccountS;

    constructor(public payload: SysBankAccountModel[]) {
    }
}
