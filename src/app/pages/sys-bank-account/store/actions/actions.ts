import {AddSysBankAccountAction} from './add.action';
import {DeleteSysBankAccountAction} from './delete.action';
import {FetchSysBankAccountAction} from './fetch.action';
import {SetSysBankAccountAction} from './set.action';
import {UpdateSysBankAccountAction} from './update.action';
import {StoreSysBankAccountAction} from './store.action';

export type SysBankAccountActions = SetSysBankAccountAction |
    AddSysBankAccountAction |
    UpdateSysBankAccountAction |
    DeleteSysBankAccountAction |
    StoreSysBankAccountAction |
    FetchSysBankAccountAction;
