import {Action} from '@ngrx/store';
import {SysBankAccountActionConstants} from './action.constant';

export class FetchSysBankAccountAction implements Action {
    // readonly type = SysBankAccountActionConstants.FETCH_ADMINUSERS;
    readonly type = SysBankAccountActionConstants.FETCH_SysBankAccountS;

    constructor(public payload: { }) {
    }
}
