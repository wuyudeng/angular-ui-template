import {Action} from '@ngrx/store';
import {SysBankAccountActionConstants} from './sys-bank-account-action.constant';

export class DeleteSysBankAccountAction implements Action {
    readonly type = SysBankAccountActionConstants.DELETE_SysBankAccount;

    constructor(public payload: number) {
    }
}
