export const SysBankAccountActionConstants = {
    SET_SysBankAccountS: 'SET_SysBankAccountS',
    ADD_SysBankAccount: 'ADD_SysBankAccount',
    UPDATE_SysBankAccount: 'UPDATE_SysBankAccount',
    DELETE_SysBankAccount: 'DELETE_SysBankAccount',
    STORE_SysBankAccountS: 'STORE_SysBankAccountS',
    FETCH_SysBankAccountS: 'FETCH_SysBankAccountS',
}