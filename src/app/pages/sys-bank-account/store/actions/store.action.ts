import {Action} from '@ngrx/store';
import {SysBankAccountActionConstants} from './sys-bank-account-action.constants';

export class StoreSysBankAccountAction implements Action {
    readonly type = SysBankAccountActionConstants.STORE_SysBankAccountS;
    public payload: any;
}
