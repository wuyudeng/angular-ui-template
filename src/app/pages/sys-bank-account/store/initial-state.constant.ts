import {SysBankAccountStateInterface} from './interfaces/state.interface';

export const SysBankAccountInitialStateConstant: SysBankAccountStateInterface = {
    sysBankAccounts: []
};