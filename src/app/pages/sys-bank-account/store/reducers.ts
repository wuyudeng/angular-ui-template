import {Ingredient} from '../../shared/ingredient.model';
import {SysBankAccountActions} from './actions/actions';
import {SysBankAccountActionConstants} from './actions/action.constant';
import {SysBankAccountInitialStateConstant} from './initial-state.constant';

export function sysBankAccountsReducer(state = SysBankAccountInitialStateConstant, action: SysBankAccountActions) {
    switch (action.type) {
        case (SysBankAccountActionConstants.SET_SysBankAccountS):
            console.log('SysBankAccountActions.SET_SysBankAccountS');
            return {
                ...state,
                sysBankAccounts: [...action.payload]
            };
        // case (SysBankAccountActionConstants.ADD_SysBankAccount):
        //     return {
        //         ...state,
        //         sysBankAccounts: [...state.sysBankAccounts, action.payload]
        //     };
        case (SysBankAccountActionConstants.UPDATE_SysBankAccount):
            const sysBankAccount= state.sysBankAccounts[action.payload.index];
            const updatedSysBankAccount= {
                ...sysBankAccount,
                ...action.payload.updatedSysBankAccount
            };
            const sysBankAccounts = [...state.sysBankAccounts];
            sysBankAccounts[action.payload.index] = updatedSysBankAccount;
            return {
                ...state,
                sysBankAccounts: sysBankAccounts
            };
        case (SysBankAccountActionConstants.DELETE_SysBankAccount):
            const oldSysBankAccounts = [...state.sysBankAccounts];
            oldSysBankAccounts.splice(action.payload, 1);
            return {
                ...state,
                sysBankAccounts: oldSysBankAccounts
            };
        default:
            return state;
    }
}
