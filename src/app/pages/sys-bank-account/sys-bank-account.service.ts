import {Injectable, OnInit} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {BaseService} from '../../shared-module/bases/base.service';

import {Store} from '@ngrx/store';
import {SysBankAccountFeatureStateInterface} from './store/interfaces/feature-state.interface';
import {FetchSysBankAccountAction} from './store/actions/fetch.action';

@Injectable()
export class SysBankAccountService extends BaseService implements OnInit {

    constructor(public http: HttpClient,
public store: Store<SysBankAccountFeatureStateInterface>) {
        super('sys-bank-account', http);

































    }

    ngOnInit(): void {
    }

fetchAll() {
        this.store.dispatch(new FetchSysBankAccountAction({}));
    }

    getAllFromStore() {
        return this.store.select(o => o.sysBankAccountList);
    }
}