import {Component, OnInit, ViewChild} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {HelperService} from '../../../services/helper.service';
import {ListBaseComponent} from '../../../shared-module/bases/list-base-component/list-base.component';
import {Router} from '@angular/router';
import {Subject} from 'rxjs/Subject';

import {SysBankAccountModel} from '../sys-bank-account.model';
import {SysBankAccountService} from '../sys-bank-account.service';

import {SortColumns} from './sort.columns';
import {Sorts} from '../../../models/bases/sorts.model';
import {DownloadService} from '../../../services/download.service';

@Component({
    selector: 'sa-sys-bank-account-list',
    templateUrl: './sys-bank-account-list.component.html',
})
export class SysBankAccountListComponent extends ListBaseComponent implements OnInit {
public searchForm: FormGroup;
public searchCondition: string;
public loading: boolean;

@ViewChild('lgImportByExcelModal') lgImportByExcelModal: any;
public excelUploadUrl: string;
public uploadAndImportSubject: Subject<string> = new Subject<string>();
public resetDropzoneSubject: Subject<string> = new Subject();
public exportUrl: string;

constructor(private formBuilder: FormBuilder,
public sysBankAccountService: SysBankAccountService,
public router: Router,
public downloadService: DownloadService,
public helperService: HelperService) {
super(router, helperService);
this.sortOprions.sortColumns = SortColumns.Columns;
}

ngOnInit() {
this.excelUploadUrl = this.sysBankAccountService.getExcelUploadUrl();
this.refresh();
this.buildSearchFrom();
this.debounceSearchForm();
}

download() {
this.downloadService.downloadFile(this.exportUrl, 'sys-bank-account-list' + Date.now().toString() + '.xls');
}

/**
* ----- modal functions BEGIN-----
*/
onImportByExcelModalHide() {
    this.lgImportByExcelModal.hide()
}

onFileUploaded(status) {
    this.refresh();
    this.lgImportByExcelModal.hide()
}

importByExceliConfirm() {
    this.uploadAndImportSubject.next('true');
}

importByExcel() {
    this.lgImportByExcelModal.show();
}

downloadExcelTemplate() {
this.downloadService.downloadFile(this.sysBankAccountService.getDownloadTemplateUrl(), 'sys-bank-account-list-template' + Date.now().toString() + '.xls');
}
/**
* ----- modal functions END-----
*/

refresh() {
this.loading = true;
const searchStr = this.helperService.getSearchConditionByRouter(this.router);
this.exportUrl = this.sysBankAccountService.getUrl() + '/excel?' + this.sysBankAccountService.getSearchUrl(searchStr, this.paging, this.sortOprions);
this.exportUrl = this.exportUrl.replace('?&', '?');
this.sysBankAccountService.getAllByPaging(searchStr, this.paging, this.sortOprions).subscribe((resp: any) => {
console.log(resp);
this.listElements = resp.content;
this.paging.totalSize = resp.totalElements;
this.loading = false;
}, err => {
this.loading = false;
});
}

/**
* ----- search form -----
*/
buildSearchFrom() {
this.searchForm = this.formBuilder.group({
        currency: ['', [Validators.required]],
        accountName: ['', [Validators.required]],
        accountNumber: ['', [Validators.required]],
        bankName: ['', [Validators.required]],
        bankAddress: ['', [Validators.required]],
        bankCity: ['', [Validators.required]],
        bankCountry: ['', [Validators.required]],
        bsbNumber: ['', [Validators.required]],
        bicCode: ['', [Validators.required]],
        correspondentBank: ['', [Validators.required]],
});
}
}