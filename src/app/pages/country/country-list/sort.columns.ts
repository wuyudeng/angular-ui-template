import {Sort} from '../../../models/bases/sort.model';

export class SortColumns {
    public static Columns = [
        new Sort({
            isAsc: false,
            columnDisplay: 'Name',
            columnModel: 'name',
            isSortable: true,
            isActive: false
        }),
        new Sort({
            isAsc: false,
            columnDisplay: 'Abbr',
            columnModel: 'abbr',
            isSortable: true,
            isActive: false
        }),
        new Sort({
            isAsc: false,
            columnDisplay: 'Clearing Code Prefix',
            columnModel: 'clearingCodePrefix',
            isSortable: true,
            isActive: false
        }),
        new Sort({
            isAsc: false,
            columnDisplay: 'Currency Country Configs',
            columnModel: 'currencyCountryConfigs',
            isSortable: true,
            isActive: false
        }),
    ];
}
