import {Injectable, OnInit} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {BaseService} from '../../shared-module/bases/base.service';

import {Store} from '@ngrx/store';
import {CountryFeatureStateInterface} from './store/interfaces/feature-state.interface';
import {FetchCountryAction} from './store/actions/fetch.action';

@Injectable()
export class CountryService extends BaseService implements OnInit {

    constructor(public http: HttpClient,
public store: Store<CountryFeatureStateInterface>) {
        super('country', http);















        this.embeddedStr = '&embedded=currency-country-configs,';
    }

    ngOnInit(): void {
    }

fetchAll() {
        this.store.dispatch(new FetchCountryAction({}));
    }

    getAllFromStore() {
        return this.store.select(o => o.countryList);
    }
}