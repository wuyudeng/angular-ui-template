import {CountryModel} from '../../country.model';
import {AppStateInterface} from '../../../../app-store/app-state.interface';

export interface CountryFeatureStateInterface extends AppStateInterface {
    countrys: CountryModel[];
}

