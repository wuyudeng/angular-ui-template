import {Injectable} from '@angular/core';
import {Actions, Effect} from '@ngrx/effects';
import 'rxjs/add/operator/switchMap';
import 'rxjs/add/operator/withLatestFrom';
import {HttpClient} from '@angular/common/http';
import {Store} from '@ngrx/store';
import {CountryActionConstants} from './actions/action.constant';
import {FetchCountryAction} from './actions/fetch.action';
import {CountryService} from '../country.service';
import {CountryFeatureStateInterface} from './interfaces/feature-state.interface';

@Injectable()
export class CountryEffects {
    @Effect()
    countryFetch = this.actions$
        .ofType(CountryActionConstants.FETCH_CountryS)
        .switchMap((action: FetchCountryAction) => {

            console.log('CountryActions.FETCH_CountryS');
            return this.countryService.getAll();
        })
        .map(
            (countrys) => ({
                // type: CountryActionConstants.SET_CountryS,
                type: CountryActionConstants.SET_CountryS,
                payload: countrys.results
            }));
    //
    // @Effect({dispatch: false})
    // countryStore = this.actions$
    //     .ofType(CountryActionConstants.STORE_CountryS)
    //     .withLatestFrom(this.store.select('countrys'))
    //     .switchMap(([action, state]) => {
    //         const req = new HttpRequest('PUT', 'https://ng-country-book-3adbb.firebaseio.com/countrys.json', state.countrys, {reportProgress: true});
    //         return this.httpClient.request(req);
    //     });

    constructor(private actions$: Actions,
                private httpClient: HttpClient,
                private countryService: CountryService,
                private store: Store<CountryFeatureStateInterface>) {
    }

}
