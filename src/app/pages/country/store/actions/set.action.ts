import {Action} from '@ngrx/store';
import {CountryActionConstants} from './admin-user-action.constants';
import {CountryModel} from '../../country.model';

export class SetCountryAction implements Action {
    readonly type = CountryActionConstants.SET_CountryS;

    constructor(public payload: CountryModel[]) {
    }
}
