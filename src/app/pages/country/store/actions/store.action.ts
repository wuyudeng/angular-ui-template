import {Action} from '@ngrx/store';
import {CountryActionConstants} from './country-action.constants';

export class StoreCountryAction implements Action {
    readonly type = CountryActionConstants.STORE_CountryS;
    public payload: any;
}
