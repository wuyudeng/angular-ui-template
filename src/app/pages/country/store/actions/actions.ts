import {AddCountryAction} from './add.action';
import {DeleteCountryAction} from './delete.action';
import {FetchCountryAction} from './fetch.action';
import {SetCountryAction} from './set.action';
import {UpdateCountryAction} from './update.action';
import {StoreCountryAction} from './store.action';

export type CountryActions = SetCountryAction |
    AddCountryAction |
    UpdateCountryAction |
    DeleteCountryAction |
    StoreCountryAction |
    FetchCountryAction;
