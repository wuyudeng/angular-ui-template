export const CountryActionConstants = {
    SET_CountryS: 'SET_CountryS',
    ADD_Country: 'ADD_Country',
    UPDATE_Country: 'UPDATE_Country',
    DELETE_Country: 'DELETE_Country',
    STORE_CountryS: 'STORE_CountryS',
    FETCH_CountryS: 'FETCH_CountryS',
}