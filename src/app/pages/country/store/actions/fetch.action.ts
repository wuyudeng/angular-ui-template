import {Action} from '@ngrx/store';
import {CountryActionConstants} from './action.constant';

export class FetchCountryAction implements Action {
    // readonly type = CountryActionConstants.FETCH_ADMINUSERS;
    readonly type = CountryActionConstants.FETCH_CountryS;

    constructor(public payload: { }) {
    }
}
