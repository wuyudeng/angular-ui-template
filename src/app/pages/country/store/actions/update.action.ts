import {Action} from '@ngrx/store';
import {CountryActionConstants} from './action.constants';
import {CountryModel} from '../../country.model';

export class UpdateCountryAction implements Action {
    readonly type = CountryActionConstants.UPDATE_Country;

    constructor(public payload: { index: number, updatedCountry: CountryModel }) {
    }
}
