import {Action} from '@ngrx/store';
import {CountryActionConstants} from './country-action.constant';

export class DeleteCountryAction implements Action {
    readonly type = CountryActionConstants.DELETE_Country;

    constructor(public payload: number) {
    }
}
