import {Action} from '@ngrx/store';
import {CountryModel} from '../../country.model';
import {CountryActionConstants} from './country-action.constant';

export class AddCountryAction implements Action {
    readonly type = CountryActionConstants.SET_CountryS;

    constructor(public payload: CountryModel[]) {
    }
}
