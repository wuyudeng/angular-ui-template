import {Ingredient} from '../../shared/ingredient.model';
import {CountryActions} from './actions/actions';
import {CountryActionConstants} from './actions/action.constant';
import {CountryInitialStateConstant} from './initial-state.constant';

export function countrysReducer(state = CountryInitialStateConstant, action: CountryActions) {
    switch (action.type) {
        case (CountryActionConstants.SET_CountryS):
            console.log('CountryActions.SET_CountryS');
            return {
                ...state,
                countrys: [...action.payload]
            };
        // case (CountryActionConstants.ADD_Country):
        //     return {
        //         ...state,
        //         countrys: [...state.countrys, action.payload]
        //     };
        case (CountryActionConstants.UPDATE_Country):
            const country= state.countrys[action.payload.index];
            const updatedCountry= {
                ...country,
                ...action.payload.updatedCountry
            };
            const countrys = [...state.countrys];
            countrys[action.payload.index] = updatedCountry;
            return {
                ...state,
                countrys: countrys
            };
        case (CountryActionConstants.DELETE_Country):
            const oldCountrys = [...state.countrys];
            oldCountrys.splice(action.payload, 1);
            return {
                ...state,
                countrys: oldCountrys
            };
        default:
            return state;
    }
}
