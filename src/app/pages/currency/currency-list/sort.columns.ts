import {Sort} from '../../../models/bases/sort.model';

export class SortColumns {
    public static Columns = [
        new Sort({
            isAsc: false,
            columnDisplay: 'Name',
            columnModel: 'name',
            isSortable: true,
            isActive: false
        }),
        new Sort({
            isAsc: false,
            columnDisplay: 'Max Per Trx',
            columnModel: 'maxPerTrx',
            isSortable: true,
            isActive: false
        }),
        new Sort({
            isAsc: false,
            columnDisplay: 'Min Per Trx',
            columnModel: 'minPerTrx',
            isSortable: true,
            isActive: false
        }),
        new Sort({
            isAsc: false,
            columnDisplay: 'Sms Alert Amt',
            columnModel: 'smsAlertAmt',
            isSortable: true,
            isActive: false
        }),
    ];
}
