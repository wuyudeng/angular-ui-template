import {CurrencyModel} from '../../currency.model';
import {AppStateInterface} from '../../../../app-store/app-state.interface';

export interface CurrencyFeatureStateInterface extends AppStateInterface {
    currencys: CurrencyModel[];
}

