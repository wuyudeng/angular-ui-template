import {Ingredient} from '../../shared/ingredient.model';
import {CurrencyActions} from './actions/actions';
import {CurrencyActionConstants} from './actions/action.constant';
import {CurrencyInitialStateConstant} from './initial-state.constant';

export function currencysReducer(state = CurrencyInitialStateConstant, action: CurrencyActions) {
    switch (action.type) {
        case (CurrencyActionConstants.SET_CurrencyS):
            console.log('CurrencyActions.SET_CurrencyS');
            return {
                ...state,
                currencys: [...action.payload]
            };
        // case (CurrencyActionConstants.ADD_Currency):
        //     return {
        //         ...state,
        //         currencys: [...state.currencys, action.payload]
        //     };
        case (CurrencyActionConstants.UPDATE_Currency):
            const currency= state.currencys[action.payload.index];
            const updatedCurrency= {
                ...currency,
                ...action.payload.updatedCurrency
            };
            const currencys = [...state.currencys];
            currencys[action.payload.index] = updatedCurrency;
            return {
                ...state,
                currencys: currencys
            };
        case (CurrencyActionConstants.DELETE_Currency):
            const oldCurrencys = [...state.currencys];
            oldCurrencys.splice(action.payload, 1);
            return {
                ...state,
                currencys: oldCurrencys
            };
        default:
            return state;
    }
}
