import {Action} from '@ngrx/store';
import {CurrencyActionConstants} from './admin-user-action.constants';
import {CurrencyModel} from '../../currency.model';

export class SetCurrencyAction implements Action {
    readonly type = CurrencyActionConstants.SET_CurrencyS;

    constructor(public payload: CurrencyModel[]) {
    }
}
