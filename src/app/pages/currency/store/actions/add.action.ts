import {Action} from '@ngrx/store';
import {CurrencyModel} from '../../currency.model';
import {CurrencyActionConstants} from './currency-action.constant';

export class AddCurrencyAction implements Action {
    readonly type = CurrencyActionConstants.SET_CurrencyS;

    constructor(public payload: CurrencyModel[]) {
    }
}
