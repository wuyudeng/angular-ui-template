export const CurrencyActionConstants = {
    SET_CurrencyS: 'SET_CurrencyS',
    ADD_Currency: 'ADD_Currency',
    UPDATE_Currency: 'UPDATE_Currency',
    DELETE_Currency: 'DELETE_Currency',
    STORE_CurrencyS: 'STORE_CurrencyS',
    FETCH_CurrencyS: 'FETCH_CurrencyS',
}