import {Action} from '@ngrx/store';
import {CurrencyActionConstants} from './currency-action.constants';

export class StoreCurrencyAction implements Action {
    readonly type = CurrencyActionConstants.STORE_CurrencyS;
    public payload: any;
}
