import {AddCurrencyAction} from './add.action';
import {DeleteCurrencyAction} from './delete.action';
import {FetchCurrencyAction} from './fetch.action';
import {SetCurrencyAction} from './set.action';
import {UpdateCurrencyAction} from './update.action';
import {StoreCurrencyAction} from './store.action';

export type CurrencyActions = SetCurrencyAction |
    AddCurrencyAction |
    UpdateCurrencyAction |
    DeleteCurrencyAction |
    StoreCurrencyAction |
    FetchCurrencyAction;
