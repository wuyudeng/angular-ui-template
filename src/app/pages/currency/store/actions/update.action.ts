import {Action} from '@ngrx/store';
import {CurrencyActionConstants} from './action.constants';
import {CurrencyModel} from '../../currency.model';

export class UpdateCurrencyAction implements Action {
    readonly type = CurrencyActionConstants.UPDATE_Currency;

    constructor(public payload: { index: number, updatedCurrency: CurrencyModel }) {
    }
}
