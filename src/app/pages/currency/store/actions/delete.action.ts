import {Action} from '@ngrx/store';
import {CurrencyActionConstants} from './currency-action.constant';

export class DeleteCurrencyAction implements Action {
    readonly type = CurrencyActionConstants.DELETE_Currency;

    constructor(public payload: number) {
    }
}
