import {Action} from '@ngrx/store';
import {CurrencyActionConstants} from './action.constant';

export class FetchCurrencyAction implements Action {
    // readonly type = CurrencyActionConstants.FETCH_ADMINUSERS;
    readonly type = CurrencyActionConstants.FETCH_CurrencyS;

    constructor(public payload: { }) {
    }
}
