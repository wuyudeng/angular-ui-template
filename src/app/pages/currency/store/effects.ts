import {Injectable} from '@angular/core';
import {Actions, Effect} from '@ngrx/effects';
import 'rxjs/add/operator/switchMap';
import 'rxjs/add/operator/withLatestFrom';
import {HttpClient} from '@angular/common/http';
import {Store} from '@ngrx/store';
import {CurrencyActionConstants} from './actions/action.constant';
import {FetchCurrencyAction} from './actions/fetch.action';
import {CurrencyService} from '../currency.service';
import {CurrencyFeatureStateInterface} from './interfaces/feature-state.interface';

@Injectable()
export class CurrencyEffects {
    @Effect()
    currencyFetch = this.actions$
        .ofType(CurrencyActionConstants.FETCH_CurrencyS)
        .switchMap((action: FetchCurrencyAction) => {

            console.log('CurrencyActions.FETCH_CurrencyS');
            return this.currencyService.getAll();
        })
        .map(
            (currencys) => ({
                // type: CurrencyActionConstants.SET_CurrencyS,
                type: CurrencyActionConstants.SET_CurrencyS,
                payload: currencys.results
            }));
    //
    // @Effect({dispatch: false})
    // currencyStore = this.actions$
    //     .ofType(CurrencyActionConstants.STORE_CurrencyS)
    //     .withLatestFrom(this.store.select('currencys'))
    //     .switchMap(([action, state]) => {
    //         const req = new HttpRequest('PUT', 'https://ng-currency-book-3adbb.firebaseio.com/currencys.json', state.currencys, {reportProgress: true});
    //         return this.httpClient.request(req);
    //     });

    constructor(private actions$: Actions,
                private httpClient: HttpClient,
                private currencyService: CurrencyService,
                private store: Store<CurrencyFeatureStateInterface>) {
    }

}
