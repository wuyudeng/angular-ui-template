import {Injectable, OnInit} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {BaseService} from '../../shared-module/bases/base.service';

import {Store} from '@ngrx/store';
import {CurrencyFeatureStateInterface} from './store/interfaces/feature-state.interface';
import {FetchCurrencyAction} from './store/actions/fetch.action';

@Injectable()
export class CurrencyService extends BaseService implements OnInit {

    constructor(public http: HttpClient,
public store: Store<CurrencyFeatureStateInterface>) {
        super('currency', http);


















        this.embeddedStr = '&embedded=currency-country-configs,';
    }

    ngOnInit(): void {
    }

fetchAll() {
        this.store.dispatch(new FetchCurrencyAction({}));
    }

    getAllFromStore() {
        return this.store.select(o => o.currencyList);
    }
}