import {ModuleWithProviders} from '@angular/core'
import {RouterModule, Routes} from '@angular/router';
import {PagesComponent} from './pages.component';
import {AuthGuard} from '../core/guards/auth.guard';

export const routes: Routes = [
    {
        path: '',
        component: PagesComponent,
        children: [
            // {path: '', redirectTo: '', pathMatch: 'full'},
                        {path: 'payer', loadChildren: 'app/pages/payer/payer.module#PayerModule' ,canActivate: [AuthGuard]},

            {path: 'collinson-user', loadChildren: 'app/pages/collinson-user/collinson-user.module#CollinsonUserModule' ,canActivate: [AuthGuard]},

            {path: 'currency', loadChildren: 'app/pages/currency/currency.module#CurrencyModule' ,canActivate: [AuthGuard]},

            {path: 'price-policy', loadChildren: 'app/pages/price-policy/price-policy.module#PricePolicyModule' ,canActivate: [AuthGuard]},

            {path: 'payee-bank-account', loadChildren: 'app/pages/payee-bank-account/payee-bank-account.module#PayeeBankAccountModule' ,canActivate: [AuthGuard]},

            {path: 'deposit', loadChildren: 'app/pages/deposit/deposit.module#DepositModule' ,canActivate: [AuthGuard]},

            {path: 'country', loadChildren: 'app/pages/country/country.module#CountryModule' ,canActivate: [AuthGuard]},

            {path: 'rate', loadChildren: 'app/pages/rate/rate.module#RateModule' ,canActivate: [AuthGuard]},

            {path: 'currency-country-config', loadChildren: 'app/pages/currency-country-config/currency-country-config.module#CurrencyCountryConfigModule' ,canActivate: [AuthGuard]},

            {path: 'transaction', loadChildren: 'app/pages/transaction/transaction.module#TransactionModule' ,canActivate: [AuthGuard]},

            {path: 'payee', loadChildren: 'app/pages/payee/payee.module#PayeeModule' ,canActivate: [AuthGuard]},

            {path: 'sys-bank-account', loadChildren: 'app/pages/sys-bank-account/sys-bank-account.module#SysBankAccountModule' ,canActivate: [AuthGuard]},

            {path: 'price-category', loadChildren: 'app/pages/price-category/price-category.module#PriceCategoryModule' ,canActivate: [AuthGuard]},

        ]
    }
];

export const routing = RouterModule.forChild(routes)
