import {Component, OnInit, ViewEncapsulation} from '@angular/core';
import {CommonService} from '../services/common.service';
import {AppState} from '../app.service';
import {AppReadyEvent} from '../app-ready.component';
import {GatewayService} from '../services/latipay/gateway.service';
import {PermissionService} from '../services/permission.service';
import {CurrencyService} from '../services/latipay/currency.service';
import {CountryService} from '../services/latipay/country.service';
import {AdminUserFeatureStateInterface} from './users/store/interfaces/feature-state.interface';
import {Store} from '@ngrx/store';
import {SystemOptionsFeatureStateInterface} from './system-options/store/interfaces/feature-state.interface';
import {FetchCurrencyAction} from './system-options/store/actions/fetch-currency.action';
import {TranslateService} from '@ngx-translate/core';

@Component({
    selector: 'sa-pages',
    encapsulation: ViewEncapsulation.None,
    templateUrl: './pages.component.html',
    styleUrls: ['./pages.component.scss'],
    providers: [AppState]
})
export class PagesComponent implements OnInit {
    constructor(private appReadyEvent: AppReadyEvent,
                public  translate: TranslateService) {
        appReadyEvent.trigger();

        // translate.addLangs(['en', 'cn']);
        // translate.setDefaultLang('en');
        //
        // const browserLang = translate.getBrowserLang();
        // translate.use(browserLang.match(/en|cn/) ? browserLang : 'en');

    }

    ngOnInit() {

    }


}
