import {Injectable} from '@angular/core';
import {Actions, Effect} from '@ngrx/effects';
import 'rxjs/add/operator/switchMap';
import 'rxjs/add/operator/withLatestFrom';
import {HttpClient} from '@angular/common/http';
import {Store} from '@ngrx/store';
import {RateActionConstants} from './actions/action.constant';
import {FetchRateAction} from './actions/fetch.action';
import {RateService} from '../rate.service';
import {RateFeatureStateInterface} from './interfaces/feature-state.interface';

@Injectable()
export class RateEffects {
    @Effect()
    rateFetch = this.actions$
        .ofType(RateActionConstants.FETCH_RateS)
        .switchMap((action: FetchRateAction) => {

            console.log('RateActions.FETCH_RateS');
            return this.rateService.getAll();
        })
        .map(
            (rates) => ({
                // type: RateActionConstants.SET_RateS,
                type: RateActionConstants.SET_RateS,
                payload: rates.results
            }));
    //
    // @Effect({dispatch: false})
    // rateStore = this.actions$
    //     .ofType(RateActionConstants.STORE_RateS)
    //     .withLatestFrom(this.store.select('rates'))
    //     .switchMap(([action, state]) => {
    //         const req = new HttpRequest('PUT', 'https://ng-rate-book-3adbb.firebaseio.com/rates.json', state.rates, {reportProgress: true});
    //         return this.httpClient.request(req);
    //     });

    constructor(private actions$: Actions,
                private httpClient: HttpClient,
                private rateService: RateService,
                private store: Store<RateFeatureStateInterface>) {
    }

}
