import {RateStateInterface} from './interfaces/state.interface';

export const RateInitialStateConstant: RateStateInterface = {
    rates: []
};