import {Ingredient} from '../../shared/ingredient.model';
import {RateActions} from './actions/actions';
import {RateActionConstants} from './actions/action.constant';
import {RateInitialStateConstant} from './initial-state.constant';

export function ratesReducer(state = RateInitialStateConstant, action: RateActions) {
    switch (action.type) {
        case (RateActionConstants.SET_RateS):
            console.log('RateActions.SET_RateS');
            return {
                ...state,
                rates: [...action.payload]
            };
        // case (RateActionConstants.ADD_Rate):
        //     return {
        //         ...state,
        //         rates: [...state.rates, action.payload]
        //     };
        case (RateActionConstants.UPDATE_Rate):
            const rate= state.rates[action.payload.index];
            const updatedRate= {
                ...rate,
                ...action.payload.updatedRate
            };
            const rates = [...state.rates];
            rates[action.payload.index] = updatedRate;
            return {
                ...state,
                rates: rates
            };
        case (RateActionConstants.DELETE_Rate):
            const oldRates = [...state.rates];
            oldRates.splice(action.payload, 1);
            return {
                ...state,
                rates: oldRates
            };
        default:
            return state;
    }
}
