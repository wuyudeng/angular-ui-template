import {RateModel} from '../../rate.model';

export interface RateStateInterface {
    rates: RateModel[];

}