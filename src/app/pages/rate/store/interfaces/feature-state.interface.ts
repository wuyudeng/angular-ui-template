import {RateModel} from '../../rate.model';
import {AppStateInterface} from '../../../../app-store/app-state.interface';

export interface RateFeatureStateInterface extends AppStateInterface {
    rates: RateModel[];
}

