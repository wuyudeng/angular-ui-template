import {Action} from '@ngrx/store';
import {RateActionConstants} from './rate-action.constant';

export class DeleteRateAction implements Action {
    readonly type = RateActionConstants.DELETE_Rate;

    constructor(public payload: number) {
    }
}
