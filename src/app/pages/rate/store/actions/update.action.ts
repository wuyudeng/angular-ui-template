import {Action} from '@ngrx/store';
import {RateActionConstants} from './action.constants';
import {RateModel} from '../../rate.model';

export class UpdateRateAction implements Action {
    readonly type = RateActionConstants.UPDATE_Rate;

    constructor(public payload: { index: number, updatedRate: RateModel }) {
    }
}
