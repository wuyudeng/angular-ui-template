import {AddRateAction} from './add.action';
import {DeleteRateAction} from './delete.action';
import {FetchRateAction} from './fetch.action';
import {SetRateAction} from './set.action';
import {UpdateRateAction} from './update.action';
import {StoreRateAction} from './store.action';

export type RateActions = SetRateAction |
    AddRateAction |
    UpdateRateAction |
    DeleteRateAction |
    StoreRateAction |
    FetchRateAction;
