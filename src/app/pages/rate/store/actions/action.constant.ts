export const RateActionConstants = {
    SET_RateS: 'SET_RateS',
    ADD_Rate: 'ADD_Rate',
    UPDATE_Rate: 'UPDATE_Rate',
    DELETE_Rate: 'DELETE_Rate',
    STORE_RateS: 'STORE_RateS',
    FETCH_RateS: 'FETCH_RateS',
}