import {Action} from '@ngrx/store';
import {RateActionConstants} from './rate-action.constants';

export class StoreRateAction implements Action {
    readonly type = RateActionConstants.STORE_RateS;
    public payload: any;
}
