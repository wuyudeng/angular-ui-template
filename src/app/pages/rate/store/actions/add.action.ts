import {Action} from '@ngrx/store';
import {RateModel} from '../../rate.model';
import {RateActionConstants} from './rate-action.constant';

export class AddRateAction implements Action {
    readonly type = RateActionConstants.SET_RateS;

    constructor(public payload: RateModel[]) {
    }
}
