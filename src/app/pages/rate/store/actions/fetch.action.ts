import {Action} from '@ngrx/store';
import {RateActionConstants} from './action.constant';

export class FetchRateAction implements Action {
    // readonly type = RateActionConstants.FETCH_ADMINUSERS;
    readonly type = RateActionConstants.FETCH_RateS;

    constructor(public payload: { }) {
    }
}
