import {Action} from '@ngrx/store';
import {RateActionConstants} from './admin-user-action.constants';
import {RateModel} from '../../rate.model';

export class SetRateAction implements Action {
    readonly type = RateActionConstants.SET_RateS;

    constructor(public payload: RateModel[]) {
    }
}
