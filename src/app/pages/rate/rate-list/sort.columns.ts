import {Sort} from '../../../models/bases/sort.model';

export class SortColumns {
    public static Columns = [
        new Sort({
            isAsc: false,
            columnDisplay: 'Buy Currency',
            columnModel: 'buyCurrency',
            isSortable: true,
            isActive: false
        }),
        new Sort({
            isAsc: false,
            columnDisplay: 'Sell Currency',
            columnModel: 'sellCurrency',
            isSortable: true,
            isActive: false
        }),
        new Sort({
            isAsc: false,
            columnDisplay: 'Buy Price',
            columnModel: 'buyPrice',
            isSortable: true,
            isActive: false
        }),
        new Sort({
            isAsc: false,
            columnDisplay: 'Sell Price',
            columnModel: 'sellPrice',
            isSortable: true,
            isActive: false
        }),
        new Sort({
            isAsc: false,
            columnDisplay: 'Sales Buy Price',
            columnModel: 'salesBuyPrice',
            isSortable: true,
            isActive: false
        }),
        new Sort({
            isAsc: false,
            columnDisplay: 'Sales Sell Price',
            columnModel: 'salesSellPrice',
            isSortable: true,
            isActive: false
        }),
        new Sort({
            isAsc: false,
            columnDisplay: 'Client Buy Price',
            columnModel: 'clientBuyPrice',
            isSortable: true,
            isActive: false
        }),
        new Sort({
            isAsc: false,
            columnDisplay: 'Client Sell Price',
            columnModel: 'clientSellPrice',
            isSortable: true,
            isActive: false
        }),
    ];
}
