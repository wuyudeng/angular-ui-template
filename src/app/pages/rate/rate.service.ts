import {Injectable, OnInit} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {BaseService} from '../../shared-module/bases/base.service';

import {Store} from '@ngrx/store';
import {RateFeatureStateInterface} from './store/interfaces/feature-state.interface';
import {FetchRateAction} from './store/actions/fetch.action';

@Injectable()
export class RateService extends BaseService implements OnInit {

    constructor(public http: HttpClient,
public store: Store<RateFeatureStateInterface>) {
        super('rate', http);



























    }

    ngOnInit(): void {
    }

fetchAll() {
        this.store.dispatch(new FetchRateAction({}));
    }

    getAllFromStore() {
        return this.store.select(o => o.rateList);
    }
}