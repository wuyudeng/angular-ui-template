import {Sort} from '../../../models/bases/sort.model';

export class SortColumns {
    public static Columns = [
        new Sort({
            isAsc: false,
            columnDisplay: 'Name',
            columnModel: 'name',
            isSortable: true,
            isActive: false
        }),
        new Sort({
            isAsc: false,
            columnDisplay: 'Price Policies',
            columnModel: 'pricePolicies',
            isSortable: true,
            isActive: false
        }),
    ];
}
