import {Ingredient} from '../../shared/ingredient.model';
import {PriceCategoryActions} from './actions/actions';
import {PriceCategoryActionConstants} from './actions/action.constant';
import {PriceCategoryInitialStateConstant} from './initial-state.constant';

export function priceCategorysReducer(state = PriceCategoryInitialStateConstant, action: PriceCategoryActions) {
    switch (action.type) {
        case (PriceCategoryActionConstants.SET_PriceCategoryS):
            console.log('PriceCategoryActions.SET_PriceCategoryS');
            return {
                ...state,
                priceCategorys: [...action.payload]
            };
        // case (PriceCategoryActionConstants.ADD_PriceCategory):
        //     return {
        //         ...state,
        //         priceCategorys: [...state.priceCategorys, action.payload]
        //     };
        case (PriceCategoryActionConstants.UPDATE_PriceCategory):
            const priceCategory= state.priceCategorys[action.payload.index];
            const updatedPriceCategory= {
                ...priceCategory,
                ...action.payload.updatedPriceCategory
            };
            const priceCategorys = [...state.priceCategorys];
            priceCategorys[action.payload.index] = updatedPriceCategory;
            return {
                ...state,
                priceCategorys: priceCategorys
            };
        case (PriceCategoryActionConstants.DELETE_PriceCategory):
            const oldPriceCategorys = [...state.priceCategorys];
            oldPriceCategorys.splice(action.payload, 1);
            return {
                ...state,
                priceCategorys: oldPriceCategorys
            };
        default:
            return state;
    }
}
