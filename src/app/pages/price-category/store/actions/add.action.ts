import {Action} from '@ngrx/store';
import {PriceCategoryModel} from '../../price-category.model';
import {PriceCategoryActionConstants} from './price-category-action.constant';

export class AddPriceCategoryAction implements Action {
    readonly type = PriceCategoryActionConstants.SET_PriceCategoryS;

    constructor(public payload: PriceCategoryModel[]) {
    }
}
