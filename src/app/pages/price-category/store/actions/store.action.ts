import {Action} from '@ngrx/store';
import {PriceCategoryActionConstants} from './price-category-action.constants';

export class StorePriceCategoryAction implements Action {
    readonly type = PriceCategoryActionConstants.STORE_PriceCategoryS;
    public payload: any;
}
