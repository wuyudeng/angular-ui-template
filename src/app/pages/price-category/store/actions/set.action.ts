import {Action} from '@ngrx/store';
import {PriceCategoryActionConstants} from './admin-user-action.constants';
import {PriceCategoryModel} from '../../price-category.model';

export class SetPriceCategoryAction implements Action {
    readonly type = PriceCategoryActionConstants.SET_PriceCategoryS;

    constructor(public payload: PriceCategoryModel[]) {
    }
}
