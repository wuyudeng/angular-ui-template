import {Action} from '@ngrx/store';
import {PriceCategoryActionConstants} from './price-category-action.constant';

export class DeletePriceCategoryAction implements Action {
    readonly type = PriceCategoryActionConstants.DELETE_PriceCategory;

    constructor(public payload: number) {
    }
}
