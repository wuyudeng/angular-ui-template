import {Action} from '@ngrx/store';
import {PriceCategoryActionConstants} from './action.constant';

export class FetchPriceCategoryAction implements Action {
    // readonly type = PriceCategoryActionConstants.FETCH_ADMINUSERS;
    readonly type = PriceCategoryActionConstants.FETCH_PriceCategoryS;

    constructor(public payload: { }) {
    }
}
