import {AddPriceCategoryAction} from './add.action';
import {DeletePriceCategoryAction} from './delete.action';
import {FetchPriceCategoryAction} from './fetch.action';
import {SetPriceCategoryAction} from './set.action';
import {UpdatePriceCategoryAction} from './update.action';
import {StorePriceCategoryAction} from './store.action';

export type PriceCategoryActions = SetPriceCategoryAction |
    AddPriceCategoryAction |
    UpdatePriceCategoryAction |
    DeletePriceCategoryAction |
    StorePriceCategoryAction |
    FetchPriceCategoryAction;
