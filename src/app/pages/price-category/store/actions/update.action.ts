import {Action} from '@ngrx/store';
import {PriceCategoryActionConstants} from './action.constants';
import {PriceCategoryModel} from '../../price-category.model';

export class UpdatePriceCategoryAction implements Action {
    readonly type = PriceCategoryActionConstants.UPDATE_PriceCategory;

    constructor(public payload: { index: number, updatedPriceCategory: PriceCategoryModel }) {
    }
}
