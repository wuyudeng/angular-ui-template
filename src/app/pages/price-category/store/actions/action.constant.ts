export const PriceCategoryActionConstants = {
    SET_PriceCategoryS: 'SET_PriceCategoryS',
    ADD_PriceCategory: 'ADD_PriceCategory',
    UPDATE_PriceCategory: 'UPDATE_PriceCategory',
    DELETE_PriceCategory: 'DELETE_PriceCategory',
    STORE_PriceCategoryS: 'STORE_PriceCategoryS',
    FETCH_PriceCategoryS: 'FETCH_PriceCategoryS',
}