import {PriceCategoryModel} from '../../price-category.model';
import {AppStateInterface} from '../../../../app-store/app-state.interface';

export interface PriceCategoryFeatureStateInterface extends AppStateInterface {
    priceCategorys: PriceCategoryModel[];
}

