import {Injectable} from '@angular/core';
import {Actions, Effect} from '@ngrx/effects';
import 'rxjs/add/operator/switchMap';
import 'rxjs/add/operator/withLatestFrom';
import {HttpClient} from '@angular/common/http';
import {Store} from '@ngrx/store';
import {PriceCategoryActionConstants} from './actions/action.constant';
import {FetchPriceCategoryAction} from './actions/fetch.action';
import {PriceCategoryService} from '../price-category.service';
import {PriceCategoryFeatureStateInterface} from './interfaces/feature-state.interface';

@Injectable()
export class PriceCategoryEffects {
    @Effect()
    priceCategoryFetch = this.actions$
        .ofType(PriceCategoryActionConstants.FETCH_PriceCategoryS)
        .switchMap((action: FetchPriceCategoryAction) => {

            console.log('PriceCategoryActions.FETCH_PriceCategoryS');
            return this.priceCategoryService.getAll();
        })
        .map(
            (priceCategorys) => ({
                // type: PriceCategoryActionConstants.SET_PriceCategoryS,
                type: PriceCategoryActionConstants.SET_PriceCategoryS,
                payload: priceCategorys.results
            }));
    //
    // @Effect({dispatch: false})
    // priceCategoryStore = this.actions$
    //     .ofType(PriceCategoryActionConstants.STORE_PriceCategoryS)
    //     .withLatestFrom(this.store.select('priceCategorys'))
    //     .switchMap(([action, state]) => {
    //         const req = new HttpRequest('PUT', 'https://ng-priceCategory-book-3adbb.firebaseio.com/priceCategorys.json', state.priceCategorys, {reportProgress: true});
    //         return this.httpClient.request(req);
    //     });

    constructor(private actions$: Actions,
                private httpClient: HttpClient,
                private priceCategoryService: PriceCategoryService,
                private store: Store<PriceCategoryFeatureStateInterface>) {
    }

}
