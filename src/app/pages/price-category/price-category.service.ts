import {Injectable, OnInit} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {BaseService} from '../../shared-module/bases/base.service';

import {Store} from '@ngrx/store';
import {PriceCategoryFeatureStateInterface} from './store/interfaces/feature-state.interface';
import {FetchPriceCategoryAction} from './store/actions/fetch.action';

@Injectable()
export class PriceCategoryService extends BaseService implements OnInit {

    constructor(public http: HttpClient,
public store: Store<PriceCategoryFeatureStateInterface>) {
        super('price-category', http);









        this.embeddedStr = '&embedded=price-policies,';
    }

    ngOnInit(): void {
    }

fetchAll() {
        this.store.dispatch(new FetchPriceCategoryAction({}));
    }

    getAllFromStore() {
        return this.store.select(o => o.priceCategoryList);
    }
}