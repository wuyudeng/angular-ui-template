import {Injectable, OnInit} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {BaseService} from '../../shared-module/bases/base.service';

import {Store} from '@ngrx/store';
import {TransactionFeatureStateInterface} from './store/interfaces/feature-state.interface';
import {FetchTransactionAction} from './store/actions/fetch.action';

@Injectable()
export class TransactionService extends BaseService implements OnInit {

    constructor(public http: HttpClient,
public store: Store<TransactionFeatureStateInterface>) {
        super('transaction', http);
























































































































        this.embeddedStr = '&embedded=parent,children,collinson-user,payer,payee,payee-bank-account,checked-by,processed-by,canceled-by,settled-by,rejected-by,';
    }

    ngOnInit(): void {
    }

fetchAll() {
        this.store.dispatch(new FetchTransactionAction({}));
    }

    getAllFromStore() {
        return this.store.select(o => o.transactionList);
    }
}