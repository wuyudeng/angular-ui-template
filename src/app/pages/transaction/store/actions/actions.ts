import {AddTransactionAction} from './add.action';
import {DeleteTransactionAction} from './delete.action';
import {FetchTransactionAction} from './fetch.action';
import {SetTransactionAction} from './set.action';
import {UpdateTransactionAction} from './update.action';
import {StoreTransactionAction} from './store.action';

export type TransactionActions = SetTransactionAction |
    AddTransactionAction |
    UpdateTransactionAction |
    DeleteTransactionAction |
    StoreTransactionAction |
    FetchTransactionAction;
