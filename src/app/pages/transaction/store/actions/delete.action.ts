import {Action} from '@ngrx/store';
import {TransactionActionConstants} from './transaction-action.constant';

export class DeleteTransactionAction implements Action {
    readonly type = TransactionActionConstants.DELETE_Transaction;

    constructor(public payload: number) {
    }
}
