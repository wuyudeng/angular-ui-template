import {Action} from '@ngrx/store';
import {TransactionActionConstants} from './action.constants';
import {TransactionModel} from '../../transaction.model';

export class UpdateTransactionAction implements Action {
    readonly type = TransactionActionConstants.UPDATE_Transaction;

    constructor(public payload: { index: number, updatedTransaction: TransactionModel }) {
    }
}
