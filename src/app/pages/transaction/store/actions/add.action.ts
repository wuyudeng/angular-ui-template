import {Action} from '@ngrx/store';
import {TransactionModel} from '../../transaction.model';
import {TransactionActionConstants} from './transaction-action.constant';

export class AddTransactionAction implements Action {
    readonly type = TransactionActionConstants.SET_TransactionS;

    constructor(public payload: TransactionModel[]) {
    }
}
