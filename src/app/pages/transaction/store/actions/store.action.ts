import {Action} from '@ngrx/store';
import {TransactionActionConstants} from './transaction-action.constants';

export class StoreTransactionAction implements Action {
    readonly type = TransactionActionConstants.STORE_TransactionS;
    public payload: any;
}
