import {Action} from '@ngrx/store';
import {TransactionActionConstants} from './action.constant';

export class FetchTransactionAction implements Action {
    // readonly type = TransactionActionConstants.FETCH_ADMINUSERS;
    readonly type = TransactionActionConstants.FETCH_TransactionS;

    constructor(public payload: { }) {
    }
}
