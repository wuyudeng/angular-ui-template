export const TransactionActionConstants = {
    SET_TransactionS: 'SET_TransactionS',
    ADD_Transaction: 'ADD_Transaction',
    UPDATE_Transaction: 'UPDATE_Transaction',
    DELETE_Transaction: 'DELETE_Transaction',
    STORE_TransactionS: 'STORE_TransactionS',
    FETCH_TransactionS: 'FETCH_TransactionS',
}