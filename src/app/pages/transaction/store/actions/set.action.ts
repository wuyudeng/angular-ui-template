import {Action} from '@ngrx/store';
import {TransactionActionConstants} from './admin-user-action.constants';
import {TransactionModel} from '../../transaction.model';

export class SetTransactionAction implements Action {
    readonly type = TransactionActionConstants.SET_TransactionS;

    constructor(public payload: TransactionModel[]) {
    }
}
