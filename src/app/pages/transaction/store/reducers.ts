import {Ingredient} from '../../shared/ingredient.model';
import {TransactionActions} from './actions/actions';
import {TransactionActionConstants} from './actions/action.constant';
import {TransactionInitialStateConstant} from './initial-state.constant';

export function transactionsReducer(state = TransactionInitialStateConstant, action: TransactionActions) {
    switch (action.type) {
        case (TransactionActionConstants.SET_TransactionS):
            console.log('TransactionActions.SET_TransactionS');
            return {
                ...state,
                transactions: [...action.payload]
            };
        // case (TransactionActionConstants.ADD_Transaction):
        //     return {
        //         ...state,
        //         transactions: [...state.transactions, action.payload]
        //     };
        case (TransactionActionConstants.UPDATE_Transaction):
            const transaction= state.transactions[action.payload.index];
            const updatedTransaction= {
                ...transaction,
                ...action.payload.updatedTransaction
            };
            const transactions = [...state.transactions];
            transactions[action.payload.index] = updatedTransaction;
            return {
                ...state,
                transactions: transactions
            };
        case (TransactionActionConstants.DELETE_Transaction):
            const oldTransactions = [...state.transactions];
            oldTransactions.splice(action.payload, 1);
            return {
                ...state,
                transactions: oldTransactions
            };
        default:
            return state;
    }
}
