import {TransactionStateInterface} from './interfaces/state.interface';

export const TransactionInitialStateConstant: TransactionStateInterface = {
    transactions: []
};