import {TransactionModel} from '../../transaction.model';

export interface TransactionStateInterface {
    transactions: TransactionModel[];

}