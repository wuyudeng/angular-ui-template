import {TransactionModel} from '../../transaction.model';
import {AppStateInterface} from '../../../../app-store/app-state.interface';

export interface TransactionFeatureStateInterface extends AppStateInterface {
    transactions: TransactionModel[];
}

