import {Injectable} from '@angular/core';
import {Actions, Effect} from '@ngrx/effects';
import 'rxjs/add/operator/switchMap';
import 'rxjs/add/operator/withLatestFrom';
import {HttpClient} from '@angular/common/http';
import {Store} from '@ngrx/store';
import {TransactionActionConstants} from './actions/action.constant';
import {FetchTransactionAction} from './actions/fetch.action';
import {TransactionService} from '../transaction.service';
import {TransactionFeatureStateInterface} from './interfaces/feature-state.interface';

@Injectable()
export class TransactionEffects {
    @Effect()
    transactionFetch = this.actions$
        .ofType(TransactionActionConstants.FETCH_TransactionS)
        .switchMap((action: FetchTransactionAction) => {

            console.log('TransactionActions.FETCH_TransactionS');
            return this.transactionService.getAll();
        })
        .map(
            (transactions) => ({
                // type: TransactionActionConstants.SET_TransactionS,
                type: TransactionActionConstants.SET_TransactionS,
                payload: transactions.results
            }));
    //
    // @Effect({dispatch: false})
    // transactionStore = this.actions$
    //     .ofType(TransactionActionConstants.STORE_TransactionS)
    //     .withLatestFrom(this.store.select('transactions'))
    //     .switchMap(([action, state]) => {
    //         const req = new HttpRequest('PUT', 'https://ng-transaction-book-3adbb.firebaseio.com/transactions.json', state.transactions, {reportProgress: true});
    //         return this.httpClient.request(req);
    //     });

    constructor(private actions$: Actions,
                private httpClient: HttpClient,
                private transactionService: TransactionService,
                private store: Store<TransactionFeatureStateInterface>) {
    }

}
