export class TransactionModel {

        id: any;


        orderId: any;


        buyCurrency: any;


        buyAmount: any;


        sellCurrency: any;


        sellAmount: any;


        payAmount: any;


        baseRate: any;


        salesRate: any;


        rate: any;


        expectedRate: any;


        expireAt: any;


        status: any;


        type: any;


        paymentReason: any;


        parent: any;


        children: any;


        overnightRate: any;


        serviceFee: any;


        collinsonUser: any;


        payer: any;


        payee: any;


        payeeBankAccount: any;


        leaf = false;


        checked = false;


        checkedBy: any;


        processedBy: any;


        canceledBy: any;


        settledBy: any;


        rejectedBy: any;


        checkedAt: any;


        processedAt: any;


        canceledAt: any;


        settledAt: any;


        bookedAt: any;


        rejectedAt: any;


        sameday = false;


        reversed = false;


        sendEmail = false;


        calculateFromBuyToSell: any;

}