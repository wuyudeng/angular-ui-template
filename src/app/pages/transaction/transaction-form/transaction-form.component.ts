import {ChangeDetectorRef, Component, OnInit} from '@angular/core';
import {FormArray, FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {Subscription} from 'rxjs/Subscription';
import {ActivatedRoute, Router} from '@angular/router';
import {Location} from '@angular/common';
import {AutoUnsubscribe} from 'ngx-auto-unsubscribe';
import {Subject} from 'rxjs/Subject';

import {FormBaseComponent} from '../../../shared-module/bases/form-base-component/form-base.component';
import {MyNotifyService} from '../../../services/my-notify.service';
import {ImageModel} from '../../../models/bases/image.model';

import {TransactionModel} from '../transaction.model';
import {TransactionService} from '../transaction.service';
import {ValidationPattern} from '../../../shared-module/bases/validation-error/validation.pattern';
declare var $: any;


@Component({
selector: 'sa-transaction-form',
templateUrl: './transaction-form.component.html',
})
@AutoUnsubscribe()
export class TransactionFormComponent extends FormBaseComponent implements OnInit {
public loading: boolean;
public myForm: FormGroup;
public id: number;
public isEdit = false;
public subscription: Subscription;
public transaction: TransactionModel = new TransactionModel();
public currentDate = new Date();
// =====================================================================
// =============================Dropzone Variable=======================
// ============================Dropzone Variable========================
// =====================================================================

// =================================================================================
// =============================multi select subject Variable=======================
// ============================multi select subject Variable========================
// =================================================================================


// =================================================================================
// ===================================EnumList Variable=============================
    public transactionStatusEnumList: any;
    public transactionTypeEnumList: any;
// ===================================EnumList Variable=============================
// =================================================================================


constructor(public formBuiler: FormBuilder,
public ref: ChangeDetectorRef,
public router: Router,
public location: Location,
public myNotifyService: MyNotifyService,
public transactionService: TransactionService,
public activatedRoute: ActivatedRoute) {
super(activatedRoute, location);
}

ngOnInit() {
this.getEnumList();
this.getRouteParemeter();
this.getQueryParams();
//this.initFormControl();
}

initFormControl() {
this.myForm = this.formBuiler.group({
        id: new FormControl('', {
        
        
            }),
        orderId: new FormControl('', {
        
        asyncValidators:[ValidationPattern.duplicateValidate(this.id, 'orderId', this.transactionService)],updateOn: 'blur'
            }),
        buyCurrency: new FormControl('', {
        
        
            }),
        buyAmount: new FormControl('', {
        
        
            }),
        sellCurrency: new FormControl('', {
        
        
            }),
        sellAmount: new FormControl('', {
        
        
            }),
        payAmount: new FormControl('', {
        
        
            }),
        baseRate: new FormControl('', {
        
        
            }),
        salesRate: new FormControl('', {
        
        
            }),
        rate: new FormControl('', {
        
        
            }),
        expectedRate: new FormControl('', {
        
        
            }),
        expireAt: new FormControl('', {
        
        
            }),
        status: new FormControl('', {
        
        
            }),
        type: new FormControl('', {
        
        
            }),
        paymentReason: new FormControl('', {
        
        
            }),
        parent: new FormControl('', {
        
        
            }),
        children: new FormControl('', {
        
        
            }),
        overnightRate: new FormControl('', {
        
        
            }),
        serviceFee: new FormControl('', {
        
        
            }),
        collinsonUser: new FormControl('', {
        
        
            }),
        payer: new FormControl('', {
        
        
            }),
        payee: new FormControl('', {
        
        
            }),
        payeeBankAccount: new FormControl('', {
        
        
            }),
        leaf: new FormControl('', {
        
        
            }),
        checked: new FormControl('', {
        
        
            }),
        checkedBy: new FormControl('', {
        
        
            }),
        processedBy: new FormControl('', {
        
        
            }),
        canceledBy: new FormControl('', {
        
        
            }),
        settledBy: new FormControl('', {
        
        
            }),
        rejectedBy: new FormControl('', {
        
        
            }),
        checkedAt: new FormControl('', {
        
        
            }),
        processedAt: new FormControl('', {
        
        
            }),
        canceledAt: new FormControl('', {
        
        
            }),
        settledAt: new FormControl('', {
        
        
            }),
        bookedAt: new FormControl('', {
        
        
            }),
        rejectedAt: new FormControl('', {
        
        
            }),
        sameday: new FormControl('', {
        
        
            }),
        reversed: new FormControl('', {
        
        
            }),
        sendEmail: new FormControl('', {
        
        
            }),
        calculateFromBuyToSell: new FormControl('', {
        
        
            }),
});
}

getEnumList(){
this.transactionService.getEnumList('transactionStatusEnum').subscribe((resp: any) => {
console.log(resp);
this.transactionStatusEnumList = resp;
});
this.transactionService.getEnumList('transactionTypeEnum').subscribe((resp: any) => {
console.log(resp);
this.transactionTypeEnumList = resp;
});
    }

getItem() {
this.loading = true;
this.transactionService.get(this.id).subscribe((resp: any) => {
this.loading = false;
console.log(resp);
this.transaction = resp;
this.emitDropzoneFiles();
}, err => {
this.loading = false;
});
}

onSubmit({value, valid}: { value: TransactionModel, valid: boolean }) {
if(valid){
if (!this.isEdit) {
this. transactionService.add(value).subscribe((resp: any) => {
console.log(resp);
this.goBack();
}, err => {
console.log(err);
this.myNotifyService.notifyFail(err.error.error);
})
} else {
this.transactionService.update(this.transaction.id, value).subscribe((resp: any) => {
console.log(resp);
this.myNotifyService.notifySuccess('The transaction is successfully updated.');
this.goBack();
}, err => {
console.log(err);
this.myNotifyService.notifyFail(err.error.error);
})
}
}else {
console.log(this.myForm);
ValidationPattern.validateAllFormFields(this.myForm);
setTimeout(t => {
                $('html, body').animate({
                    scrollTop: $('.text-danger:visible:first').offset().top - 100
                }, 1000);
            });
this.myNotifyService.notifyFail('Some fields are invalid, please check.');
}
}

// =====================================================================
// =======================Multi Select Event============================








































// =======================Multi Select Event============================
// =====================================================================

// =====================================================================
// =============================Dropzone================================
emitDropzoneFiles(){


}


// ============================Dropzone=================================
// =====================================================================

// ===========================================================================
// =============================daymically add================================

// ============================daymically add=================================
// ===========================================================================



}
