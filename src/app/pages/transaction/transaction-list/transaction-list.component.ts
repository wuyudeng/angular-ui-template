import {Component, OnInit, ViewChild} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {HelperService} from '../../../services/helper.service';
import {ListBaseComponent} from '../../../shared-module/bases/list-base-component/list-base.component';
import {Router} from '@angular/router';
import {Subject} from 'rxjs/Subject';

import {TransactionModel} from '../transaction.model';
import {TransactionService} from '../transaction.service';

import {SortColumns} from './sort.columns';
import {Sorts} from '../../../models/bases/sorts.model';
import {DownloadService} from '../../../services/download.service';

@Component({
    selector: 'sa-transaction-list',
    templateUrl: './transaction-list.component.html',
})
export class TransactionListComponent extends ListBaseComponent implements OnInit {
public searchForm: FormGroup;
public searchCondition: string;
public loading: boolean;

@ViewChild('lgImportByExcelModal') lgImportByExcelModal: any;
public excelUploadUrl: string;
public uploadAndImportSubject: Subject<string> = new Subject<string>();
public resetDropzoneSubject: Subject<string> = new Subject();
public exportUrl: string;

constructor(private formBuilder: FormBuilder,
public transactionService: TransactionService,
public router: Router,
public downloadService: DownloadService,
public helperService: HelperService) {
super(router, helperService);
this.sortOprions.sortColumns = SortColumns.Columns;
}

ngOnInit() {
this.excelUploadUrl = this.transactionService.getExcelUploadUrl();
this.refresh();
this.buildSearchFrom();
this.debounceSearchForm();
}

download() {
this.downloadService.downloadFile(this.exportUrl, 'transaction-list' + Date.now().toString() + '.xls');
}

/**
* ----- modal functions BEGIN-----
*/
onImportByExcelModalHide() {
    this.lgImportByExcelModal.hide()
}

onFileUploaded(status) {
    this.refresh();
    this.lgImportByExcelModal.hide()
}

importByExceliConfirm() {
    this.uploadAndImportSubject.next('true');
}

importByExcel() {
    this.lgImportByExcelModal.show();
}

downloadExcelTemplate() {
this.downloadService.downloadFile(this.transactionService.getDownloadTemplateUrl(), 'transaction-list-template' + Date.now().toString() + '.xls');
}
/**
* ----- modal functions END-----
*/

refresh() {
this.loading = true;
const searchStr = this.helperService.getSearchConditionByRouter(this.router);
this.exportUrl = this.transactionService.getUrl() + '/excel?' + this.transactionService.getSearchUrl(searchStr, this.paging, this.sortOprions);
this.exportUrl = this.exportUrl.replace('?&', '?');
this.transactionService.getAllByPaging(searchStr, this.paging, this.sortOprions).subscribe((resp: any) => {
console.log(resp);
this.listElements = resp.content;
this.paging.totalSize = resp.totalElements;
this.loading = false;
}, err => {
this.loading = false;
});
}

/**
* ----- search form -----
*/
buildSearchFrom() {
this.searchForm = this.formBuilder.group({
        orderId: ['', [Validators.required]],
        buyCurrency: ['', [Validators.required]],
        buyAmount: ['', [Validators.required]],
        sellCurrency: ['', [Validators.required]],
        sellAmount: ['', [Validators.required]],
        payAmount: ['', [Validators.required]],
        baseRate: ['', [Validators.required]],
        salesRate: ['', [Validators.required]],
        rate: ['', [Validators.required]],
        expectedRate: ['', [Validators.required]],
        expireAt: ['', [Validators.required]],
        status: ['', [Validators.required]],
        type: ['', [Validators.required]],
        paymentReason: ['', [Validators.required]],
        parent: ['', [Validators.required]],
        children: ['', [Validators.required]],
        overnightRate: ['', [Validators.required]],
        serviceFee: ['', [Validators.required]],
        collinsonUser: ['', [Validators.required]],
        payer: ['', [Validators.required]],
        payee: ['', [Validators.required]],
        payeeBankAccount: ['', [Validators.required]],
        leaf: ['', [Validators.required]],
        checked: ['', [Validators.required]],
        checkedBy: ['', [Validators.required]],
        processedBy: ['', [Validators.required]],
        canceledBy: ['', [Validators.required]],
        settledBy: ['', [Validators.required]],
        rejectedBy: ['', [Validators.required]],
        checkedAt: ['', [Validators.required]],
        processedAt: ['', [Validators.required]],
        canceledAt: ['', [Validators.required]],
        settledAt: ['', [Validators.required]],
        bookedAt: ['', [Validators.required]],
        rejectedAt: ['', [Validators.required]],
        sameday: ['', [Validators.required]],
        reversed: ['', [Validators.required]],
        sendEmail: ['', [Validators.required]],
        calculateFromBuyToSell: ['', [Validators.required]],
});
}
}