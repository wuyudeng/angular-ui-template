import {NgModule} from '@angular/core';
import {SmartadminModule} from '../shared/smartadmin.module'
import {PagesComponent} from './pages.component';
import {routing} from './pages.routing';

@NgModule({
    declarations: [
        PagesComponent
    ],
    imports: [
        SmartadminModule,
        routing,
        // EffectsModule.forFeature([AdminUserEffects, SystemOptionsEffects])
    ],
})

export class PagesModule {
}
