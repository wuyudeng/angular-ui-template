import {Injectable, OnInit} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {BaseService} from '../../shared-module/bases/base.service';

import {Store} from '@ngrx/store';
import {CurrencyCountryConfigFeatureStateInterface} from './store/interfaces/feature-state.interface';
import {FetchCurrencyCountryConfigAction} from './store/actions/fetch.action';

@Injectable()
export class CurrencyCountryConfigService extends BaseService implements OnInit {

    constructor(public http: HttpClient,
public store: Store<CurrencyCountryConfigFeatureStateInterface>) {
        super('currency-country-config', http);


















        this.embeddedStr = '&embedded=currency,country,';
    }

    ngOnInit(): void {
    }

fetchAll() {
        this.store.dispatch(new FetchCurrencyCountryConfigAction({}));
    }

    getAllFromStore() {
        return this.store.select(o => o.currencyCountryConfigList);
    }
}