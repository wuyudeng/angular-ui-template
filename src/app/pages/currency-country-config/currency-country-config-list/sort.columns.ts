import {Sort} from '../../../models/bases/sort.model';

export class SortColumns {
    public static Columns = [
        new Sort({
            isAsc: false,
            columnDisplay: 'Currency',
            columnModel: 'currency',
            isSortable: true,
            isActive: false
        }),
        new Sort({
            isAsc: false,
            columnDisplay: 'Country',
            columnModel: 'country',
            isSortable: true,
            isActive: false
        }),
        new Sort({
            isAsc: false,
            columnDisplay: 'Clearing Code Type',
            columnModel: 'clearingCodeType',
            isSortable: true,
            isActive: false
        }),
        new Sort({
            isAsc: false,
            columnDisplay: 'Need Swift Code',
            columnModel: 'needSwiftCode',
            isSortable: true,
            isActive: false
        }),
        new Sort({
            isAsc: false,
            columnDisplay: 'Need Account Number',
            columnModel: 'needAccountNumber',
            isSortable: true,
            isActive: false
        }),
    ];
}
