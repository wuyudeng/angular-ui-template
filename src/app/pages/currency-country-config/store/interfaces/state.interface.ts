import {CurrencyCountryConfigModel} from '../../currency-country-config.model';

export interface CurrencyCountryConfigStateInterface {
    currencyCountryConfigs: CurrencyCountryConfigModel[];

}