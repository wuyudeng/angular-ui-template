import {CurrencyCountryConfigModel} from '../../currency-country-config.model';
import {AppStateInterface} from '../../../../app-store/app-state.interface';

export interface CurrencyCountryConfigFeatureStateInterface extends AppStateInterface {
    currencyCountryConfigs: CurrencyCountryConfigModel[];
}

