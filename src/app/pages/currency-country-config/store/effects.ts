import {Injectable} from '@angular/core';
import {Actions, Effect} from '@ngrx/effects';
import 'rxjs/add/operator/switchMap';
import 'rxjs/add/operator/withLatestFrom';
import {HttpClient} from '@angular/common/http';
import {Store} from '@ngrx/store';
import {CurrencyCountryConfigActionConstants} from './actions/action.constant';
import {FetchCurrencyCountryConfigAction} from './actions/fetch.action';
import {CurrencyCountryConfigService} from '../currency-country-config.service';
import {CurrencyCountryConfigFeatureStateInterface} from './interfaces/feature-state.interface';

@Injectable()
export class CurrencyCountryConfigEffects {
    @Effect()
    currencyCountryConfigFetch = this.actions$
        .ofType(CurrencyCountryConfigActionConstants.FETCH_CurrencyCountryConfigS)
        .switchMap((action: FetchCurrencyCountryConfigAction) => {

            console.log('CurrencyCountryConfigActions.FETCH_CurrencyCountryConfigS');
            return this.currencyCountryConfigService.getAll();
        })
        .map(
            (currencyCountryConfigs) => ({
                // type: CurrencyCountryConfigActionConstants.SET_CurrencyCountryConfigS,
                type: CurrencyCountryConfigActionConstants.SET_CurrencyCountryConfigS,
                payload: currencyCountryConfigs.results
            }));
    //
    // @Effect({dispatch: false})
    // currencyCountryConfigStore = this.actions$
    //     .ofType(CurrencyCountryConfigActionConstants.STORE_CurrencyCountryConfigS)
    //     .withLatestFrom(this.store.select('currencyCountryConfigs'))
    //     .switchMap(([action, state]) => {
    //         const req = new HttpRequest('PUT', 'https://ng-currencyCountryConfig-book-3adbb.firebaseio.com/currencyCountryConfigs.json', state.currencyCountryConfigs, {reportProgress: true});
    //         return this.httpClient.request(req);
    //     });

    constructor(private actions$: Actions,
                private httpClient: HttpClient,
                private currencyCountryConfigService: CurrencyCountryConfigService,
                private store: Store<CurrencyCountryConfigFeatureStateInterface>) {
    }

}
