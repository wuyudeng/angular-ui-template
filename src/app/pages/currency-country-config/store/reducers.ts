import {Ingredient} from '../../shared/ingredient.model';
import {CurrencyCountryConfigActions} from './actions/actions';
import {CurrencyCountryConfigActionConstants} from './actions/action.constant';
import {CurrencyCountryConfigInitialStateConstant} from './initial-state.constant';

export function currencyCountryConfigsReducer(state = CurrencyCountryConfigInitialStateConstant, action: CurrencyCountryConfigActions) {
    switch (action.type) {
        case (CurrencyCountryConfigActionConstants.SET_CurrencyCountryConfigS):
            console.log('CurrencyCountryConfigActions.SET_CurrencyCountryConfigS');
            return {
                ...state,
                currencyCountryConfigs: [...action.payload]
            };
        // case (CurrencyCountryConfigActionConstants.ADD_CurrencyCountryConfig):
        //     return {
        //         ...state,
        //         currencyCountryConfigs: [...state.currencyCountryConfigs, action.payload]
        //     };
        case (CurrencyCountryConfigActionConstants.UPDATE_CurrencyCountryConfig):
            const currencyCountryConfig= state.currencyCountryConfigs[action.payload.index];
            const updatedCurrencyCountryConfig= {
                ...currencyCountryConfig,
                ...action.payload.updatedCurrencyCountryConfig
            };
            const currencyCountryConfigs = [...state.currencyCountryConfigs];
            currencyCountryConfigs[action.payload.index] = updatedCurrencyCountryConfig;
            return {
                ...state,
                currencyCountryConfigs: currencyCountryConfigs
            };
        case (CurrencyCountryConfigActionConstants.DELETE_CurrencyCountryConfig):
            const oldCurrencyCountryConfigs = [...state.currencyCountryConfigs];
            oldCurrencyCountryConfigs.splice(action.payload, 1);
            return {
                ...state,
                currencyCountryConfigs: oldCurrencyCountryConfigs
            };
        default:
            return state;
    }
}
