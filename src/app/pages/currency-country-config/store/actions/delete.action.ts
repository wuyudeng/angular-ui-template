import {Action} from '@ngrx/store';
import {CurrencyCountryConfigActionConstants} from './currency-country-config-action.constant';

export class DeleteCurrencyCountryConfigAction implements Action {
    readonly type = CurrencyCountryConfigActionConstants.DELETE_CurrencyCountryConfig;

    constructor(public payload: number) {
    }
}
