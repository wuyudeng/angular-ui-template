import {Action} from '@ngrx/store';
import {CurrencyCountryConfigActionConstants} from './action.constant';

export class FetchCurrencyCountryConfigAction implements Action {
    // readonly type = CurrencyCountryConfigActionConstants.FETCH_ADMINUSERS;
    readonly type = CurrencyCountryConfigActionConstants.FETCH_CurrencyCountryConfigS;

    constructor(public payload: { }) {
    }
}
