import {Action} from '@ngrx/store';
import {CurrencyCountryConfigActionConstants} from './currency-country-config-action.constants';

export class StoreCurrencyCountryConfigAction implements Action {
    readonly type = CurrencyCountryConfigActionConstants.STORE_CurrencyCountryConfigS;
    public payload: any;
}
