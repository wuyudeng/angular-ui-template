import {Action} from '@ngrx/store';
import {CurrencyCountryConfigModel} from '../../currency-country-config.model';
import {CurrencyCountryConfigActionConstants} from './currency-country-config-action.constant';

export class AddCurrencyCountryConfigAction implements Action {
    readonly type = CurrencyCountryConfigActionConstants.SET_CurrencyCountryConfigS;

    constructor(public payload: CurrencyCountryConfigModel[]) {
    }
}
