import {Action} from '@ngrx/store';
import {CurrencyCountryConfigActionConstants} from './admin-user-action.constants';
import {CurrencyCountryConfigModel} from '../../currency-country-config.model';

export class SetCurrencyCountryConfigAction implements Action {
    readonly type = CurrencyCountryConfigActionConstants.SET_CurrencyCountryConfigS;

    constructor(public payload: CurrencyCountryConfigModel[]) {
    }
}
