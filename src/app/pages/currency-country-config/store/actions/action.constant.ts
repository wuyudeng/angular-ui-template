export const CurrencyCountryConfigActionConstants = {
    SET_CurrencyCountryConfigS: 'SET_CurrencyCountryConfigS',
    ADD_CurrencyCountryConfig: 'ADD_CurrencyCountryConfig',
    UPDATE_CurrencyCountryConfig: 'UPDATE_CurrencyCountryConfig',
    DELETE_CurrencyCountryConfig: 'DELETE_CurrencyCountryConfig',
    STORE_CurrencyCountryConfigS: 'STORE_CurrencyCountryConfigS',
    FETCH_CurrencyCountryConfigS: 'FETCH_CurrencyCountryConfigS',
}