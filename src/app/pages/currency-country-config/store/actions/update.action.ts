import {Action} from '@ngrx/store';
import {CurrencyCountryConfigActionConstants} from './action.constants';
import {CurrencyCountryConfigModel} from '../../currency-country-config.model';

export class UpdateCurrencyCountryConfigAction implements Action {
    readonly type = CurrencyCountryConfigActionConstants.UPDATE_CurrencyCountryConfig;

    constructor(public payload: { index: number, updatedCurrencyCountryConfig: CurrencyCountryConfigModel }) {
    }
}
