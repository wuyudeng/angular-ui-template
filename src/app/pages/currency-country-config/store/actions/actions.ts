import {AddCurrencyCountryConfigAction} from './add.action';
import {DeleteCurrencyCountryConfigAction} from './delete.action';
import {FetchCurrencyCountryConfigAction} from './fetch.action';
import {SetCurrencyCountryConfigAction} from './set.action';
import {UpdateCurrencyCountryConfigAction} from './update.action';
import {StoreCurrencyCountryConfigAction} from './store.action';

export type CurrencyCountryConfigActions = SetCurrencyCountryConfigAction |
    AddCurrencyCountryConfigAction |
    UpdateCurrencyCountryConfigAction |
    DeleteCurrencyCountryConfigAction |
    StoreCurrencyCountryConfigAction |
    FetchCurrencyCountryConfigAction;
