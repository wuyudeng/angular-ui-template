import {CurrencyCountryConfigStateInterface} from './interfaces/state.interface';

export const CurrencyCountryConfigInitialStateConstant: CurrencyCountryConfigStateInterface = {
    currencyCountryConfigs: []
};