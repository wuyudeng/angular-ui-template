import {NgModule} from '@angular/core';
import {SmartadminDatatableModule} from '../../shared/ui/datatable/smartadmin-datatable.module';
import {SmartadminModule} from '../../shared/smartadmin.module';
import {CommonModule} from '@angular/common';
import {SmartadminInputModule} from '../../shared/forms/input/smartadmin-input.module';
import {RouterModule, Routes} from '@angular/router';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {SharedModule} from '../../shared-module/shared.module';
import {NgxPaginationModule} from 'ngx-pagination';
import {NguiDatetimePickerModule} from '@ngui/datetime-picker';
import {QuillModule} from 'ngx-quill';
import {LoadingModule} from 'ngx-loading';
import {ConfirmationPopoverModule} from 'angular-confirmation-popover';
import {NgSelectSharedModule} from '../../shared-fearure-modules/ng-select-shared-module/ng-select-shared.module';
import {MultiSelectSharedModule} from '../../shared-fearure-modules/multi-select-shared-module/multi-select-shared.module';
import {NouisliderModule} from 'ng2-nouislider';

import {CurrencyCountryConfigListComponent} from './currency-country-config-list/currency-country-config-list.component';
import {CurrencyCountryConfigFormComponent} from './currency-country-config-form/currency-country-config-form.component';

export const routes: Routes = [
    {path: 'currency-country-config-list', component: CurrencyCountryConfigListComponent, pathMatch: 'full'},
    {path: 'currency-country-config-add', component: CurrencyCountryConfigFormComponent, pathMatch: 'full'},
    {path: 'currency-country-config-edit/:id', component: CurrencyCountryConfigFormComponent, pathMatch: 'full'},
];


@NgModule({
    declarations: [
        CurrencyCountryConfigListComponent,
        CurrencyCountryConfigFormComponent
    ],
    imports: [
        SmartadminModule,
        SmartadminDatatableModule,
        RouterModule.forChild(routes),
        CommonModule,
        SmartadminInputModule,
        FormsModule,
        ReactiveFormsModule,
        SharedModule,
        NgxPaginationModule,
        LoadingModule,
        ConfirmationPopoverModule,
        MultiSelectSharedModule,
        NgSelectSharedModule,
        QuillModule,
        NguiDatetimePickerModule,
        NouisliderModule

    ],
})
export class CurrencyCountryConfigModule {
}
