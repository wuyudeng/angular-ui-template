import {Injectable} from '@angular/core';


import { Subject } from 'rxjs/Subject';

@Injectable()
export class UserService {

  public user: Subject<any>;

  public userInfo = {
    username: 'Guest'
  };

  constructor() {
    this.user = new Subject();
  }

}
