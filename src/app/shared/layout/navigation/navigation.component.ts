import {Component, OnInit} from '@angular/core';
import {LoginInfoComponent} from '../../user/login-info/login-info.component';
import {TranslateService} from '@ngx-translate/core';


@Component({

    selector: 'sa-navigation',
    templateUrl: './navigation.component.html'
})
export class NavigationComponent implements OnInit {

    constructor(public translate: TranslateService) {
        // translate.addLangs(['en', 'cn']);
        // translate.setDefaultLang('en');

        // const browserLang = translate.getBrowserLang();
        // translate.use(browserLang.match(/en|cn/) ? browserLang : 'en');
    }

    ngOnInit() {
    }

}
