import {Component, OnInit} from '@angular/core';
import {languages} from '../languages.model'
import {TranslateService} from '@ngx-translate/core';

@Component({
    selector: 'sa-language-selector',
    templateUrl: './language-selector.component.html',
})
export class LanguageSelectorComponent implements OnInit {

    public languages: Array<any>;
    public currentLanguage: any;

    constructor(public translate: TranslateService) {
        this.languages = languages;
        this.currentLanguage = this.languages.find(l => l.key === 'us');

        const langs = [];
        this.languages.map(l => {
            langs.push(l.key)
        });
        translate.addLangs(langs);
        translate.setDefaultLang('us');
        const browserLang = translate.getBrowserLang();
        translate.use(browserLang.match(/en|cn/) ? browserLang : 'us');
    }

    ngOnInit() {

    }

    setLanguage(language) {
        this.currentLanguage = language;
        this.translate.use(language.key);
    }
}
