import { Injectable } from '@angular/core';
import { Subject } from 'rxjs/Subject';

@Injectable()
export class ChatService {

  url: string;

  public messageToSubject;
  public newMessage;

  constructor() {
    this.url = '/chat/chat.json';
    this.messageToSubject = new Subject();
    this.newMessage = new Subject();
  }


  getChatState()  {
  }

  messageTo(user){
    this.messageToSubject.next(user)
  }

  sendMessage(message){
    this.newMessage.next(message)

  }



}
